<?php (!defined('BASEPATH')) and exit('No direct script access allowed');

class L
{
    /**
     * ci instance object
     *
     */
    public function __construct()
        {
            $this->CI = & get_instance();
        }

    public function l($key){
        // return HOTEL_BANACO_LANGUAGE;
        // $language = ($this->session->userdata('language') === 'ITALIAN')?'ITALIAN':'ENGLISH';
        // return $language;
        $lang = $this->CI->Admin_model->get_language($key, HOTEL_BANACO_LANGUAGE);
        if (HOTEL_BANACO_LANGUAGE === 'ITALIAN') {
            $langName = $lang->lang_italian;
        }else{
            $langName = $lang->lang_english;
        }

        if (empty($langName)) {
            $langName = $key;
        }
        return $langName;
    }


    // public function Lan($key){
    //     $ipaddress = '';
    //         if (getenv('HTTP_CLIENT_IP'))
    //             $ipaddress = getenv('HTTP_CLIENT_IP');
    //         else if(getenv('HTTP_X_FORWARDED_FOR'))
    //             $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    //         else if(getenv('HTTP_X_FORWARDED'))
    //             $ipaddress = getenv('HTTP_X_FORWARDED');
    //         else if(getenv('HTTP_FORWARDED_FOR'))
    //             $ipaddress = getenv('HTTP_FORWARDED_FOR');
    //         else if(getenv('HTTP_FORWARDED'))
    //            $ipaddress = getenv('HTTP_FORWARDED');
    //         else if(getenv('REMOTE_ADDR'))
    //             $ipaddress = getenv('REMOTE_ADDR');
    //         else
    //             $ipaddress = 'UNKNOWN';

    //         if (empty($ipaddress)) {
    //             if (isset($_SERVER['HTTP_CLIENT_IP']))
    //                 $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    //             else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    //                 $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    //             else if(isset($_SERVER['HTTP_X_FORWARDED']))
    //                 $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    //             else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
    //                 $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    //             else if(isset($_SERVER['HTTP_FORWARDED']))
    //                 $ipaddress = $_SERVER['HTTP_FORWARDED'];
    //             else if(isset($_SERVER['REMOTE_ADDR']))
    //                 $ipaddress = $_SERVER['REMOTE_ADDR'];
    //             else
    //                 $ipaddress = 'UNKNOWN';
    //         }

    //     $user_id = $this->CI->session->userdata('id');
    //     if (isset($user_id)) {
    //         $user_deatils = $this->CI->language_model->getUserDeails($user_id);
    //         $language = $user_deatils->lang;
    //     }else{
    //         $default = $this->CI->language_model->getLanguageDeailsByIp($ipaddress);
    //         if (empty($default)) {
    //             $language = 'fr';
    //         }else{
    //             $language = $default->dlang_lang;
    //         }
    //     }

    //     $lang = $this->CI->language_model->get_language($key, $language);
    //     if ($language === 'fr') {
    //         $langName = $lang->lang_french;
    //     }else{
    //         $langName = $lang->lang_english;
    //     }

    //     if (empty($langName)) {
    //         $langName = $key;
    //     }
    //     return $langName;
    // }


    // public function get_appointment_check_subscription($slug = '')
    // {
    //     $result = $this->CI->language_model->get_appointment_check_subscription($slug);
    //     return $result;
    // }

    // public function get_appointment_check_subscription_assign_days_present($slug = '')
    // {
    //     $status = false;
    //     $result = $this->CI->language_model->get_appointment_check_subscription_assign_days_present($slug);
    //     if (!empty($result)) {
    //         foreach ($result as $key => $value) {
    //             if (!empty($value->start) && !empty($value->end)) {
    //                 $status = true;
    //             }
    //         }
    //     }
        
    //     return $status;
    // }



}
