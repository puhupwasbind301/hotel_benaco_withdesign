<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
|  URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


//Front
// P Starts
$route['default_controller'] = 'Home/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['index'] = 'Home/index';
$route['error-404'] = 'Home/error_404';

//NEED INFORMATION
$route['home/insert-need-information'] = 'Home/insertNeedInformation';
//Camera Singola
$route['home/camera-singola'] = 'Home/cameraSingola';


//HOTEL BOOKING
$route['home/login-check'] 				= 'Home/checkLogin';
$route['home/sign-up'] 					= 'Home/signUp';
$route['home/insert-hotel-booking']	    = 'Home/insertUserSquareFeetCalculator';
$route['home/contact-form-submission']  = 'Home/contactFormSubmission';
// P Ends

//Admin
$route['web-admin'] 	 		= 'Admin_login/auth_admin';
$route['admin']     	 		= 'Admin';
$route['admin/backend']     	= 'Admin/backend';
$route['admin/logout']   		= 'Admin_login/logout';
$route['admin/set-lockscreen']  = 'Admin/set_lockscreen';
$route['admin/lockscreen']      = 'Admin_login/auth_lockscreen';
$route['admin/blank'] 			= 'Admin/blank';	
$route['admin/setting'] 		= 'Admin/setting';
$route['admin/reset-password'] 	= 'Admin/resetPassword';

//Home Page Def ADD

$route['admin/home-page-def'] 			 = 'Admin/homePageDef';
$route['admin/experience'] 				 = 'Admin/experience';
$route['admin/living-hotel-banaco'] 	 = 'Admin/livingHotelBanaco';
$route['admin/pool-and-garden'] 		 = 'Admin/poolAndGarden';
$route['admin/contact-detail'] 			 = 'Admin/contactDetail';

//Home Page Def VIEW

$route['admin/user-need-information-detail']= 'Admin/userNeedInformationDetail';
$route['admin/delete-user-need-information']= 'Delete/deleteUserNeedInformation';
$route['admin/contact-detail-view'] 		= 'Admin/contactDetailView';




//Hotel Booking
$route['admin/booking'] 					= 'Admin/booking';
$route['admin/user-detail'] 	    		= 'Admin/userDetail';

//Hotel Booking VIEW
$route['admin/delete-user'] 				= 'Delete/deleteUser';
// $route['admin/feature-and-service-view'] 	= 'Admin/featureAndServiceView';


$route['admin/insert-add-slider-image'] 	   = 'Admin/insertAddSliderImage';
$route['admin/update-add-slider-image'] 	   = 'Admin/updateAddSliderImage';
$route['admin/delete-add-slider-image'] 	   = 'Delete/deleteAddSliderImage';


$route['admin/room-page']					 ='Admin/roomPage';
$route['admin/home-page']					 ='Admin/homePage';
$route['admin/delete-home-page-image/(:num)/(:any)']='Delete/deleteHomePageImage/$1/$2';
$route['admin/insert-room-category'] 		 = 'Admin/insertRoomCategory';
$route['admin/update-room-category'] 		 = 'Admin/updateRoomCategory';
$route['admin/delete-room-category'] 		 = 'Delete/deleteRoomCategory';
$route['admin/get-room-detail/(:num)'] 		 = 'Admin/getRoomDetail/$1';
$route['admin/get-room-image-video/(:num)']  = 'Admin/getRoomImageVideo/$1';
$route['admin/get-room-feature-service/(:num)']  = 'Admin/getRoomFeatureService/$1';



$route['admin/insert-hotel-benaco-video']='Admin/insertHotelBenacoVideo';
$route['admin/hotel-banaco-video-view']  ='Admin/hotelBanacoVideoView';
$route['admin/update-hotel-benaco-video']='Admin/updateHotelBenacoVideo';
$route['admin/delete-hotel-benaco']  	 ='Delete/deleteHotelBenacoVideo';


$route['admin/insert-contact-detail'] 	   = 'Admin/insertContactDetail';
$route['admin/update-contact-detail'] 	   = 'Admin/updateContactDetail';
$route['admin/delete-contact-detail'] 	   = 'Delete/deleteContactDetail';
























$route['admin/datatable_json'] = 'Admin/datatable_json';





// puhupwas ends

// $route['default_controller'] = 'Admin_login/auth_admin';
// $route['404_override'] = '';
// $route['translate_uri_dashes'] = FALSE;

// $route['index'] = 'Home/index';
// $route['error-404'] = 'Home/error_404';

// P Ends
