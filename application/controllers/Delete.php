<?php 

	class Delete extends CI_Controller{

		
		//Delete deleteLivingHotelBanaco
		public function deleteLivingHotelBanaco(){

			$res = $this->Admin_model->deleteTable('living_hotel_banaco_room','lhbr_id',$this->input->post('lhbrid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}

		//Delete deleteLivingHotelBanacoFacilites
		public function deleteLivingHotelBanacoFacilites(){

			$res = $this->Admin_model->deleteTable('living_hotel_banaco_facilities','lhb_id',$this->input->post('lhbid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}

			//Delete deleteUserNeedInformation
		public function deleteUserNeedInformation(){

			$res = $this->Admin_model->deleteTable('user_need_information','uni_id',$this->input->post('uniid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}


			//Delete deleteContactDetail
		public function deleteContactDetail(){

			$res = $this->Admin_model->deleteTable('contact_detail','cd_id',$this->input->post('cdid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}


	//Delete deleteHomePageDefs
		public function deleteHomePageDef(){
			$res = $this->Admin_model->deleteTable('home_page_def','hpd_id',$this->input->post('hpdid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}

	//Delete deleteHotelBenacoVideo
		public function deleteHotelBenacoVideo(){

			$res = $this->Admin_model->deleteTable('hotel_benaco_video','hbv_id',$this->input->post('hbvid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}

			//Delete deleteContactDetail
		public function deleteExperience(){

			$res = $this->Admin_model->deleteTable('experience','e_id',$this->input->post('eid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}

	//Delete deleteAddSliderImage
		public function deleteAddSliderImage(){

			$res = $this->Admin_model->deleteTable('slider_images','si_id',$this->input->post('siid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}


	// Rooms

	//Delete deleteDoubleRoom
		public function deleteDoubleRoom(){

			$res = $this->Admin_model->deleteTable('double_room','dr_id',$this->input->post('drid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}

		//Delete deleteFeatureAndService
		public function deleteFeatureAndService(){

			$res = $this->Admin_model->deleteTable('feature_and_service','fas_id',$this->input->post('fasid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}

	//Delete deleteRoomFeatureService
		public function deleteRoomFeatureService(){

			$res = $this->Admin_model->deleteTable('feature_and_service_detail','fasd_id',$this->input->post('fasdid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}




		//Delete deleteDiscoverOtherRoom
		public function deleteDiscoverOtherRoom(){

			$res = $this->Admin_model->deleteTable('discover_other_room','dor_id',$this->input->post('dorid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}

		//Delete deleteOtherRoomFacilites
		public function deleteOtherRoomFacilites(){

			$res = $this->Admin_model->deleteTable('discover_other_room_facilities','dorf_id',$this->input->post('dorfid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}

	//Delete users
		public function deleteUser(){

			$res = $this->Admin_model->deleteTable('user','user_id',$this->input->post('userid'));
			
			if($res){

				$this->session->set_flashdata('success_msg',"Deleted Successfully!");
				redirect($this->agent->referrer());
			}
			else{

				$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
				redirect($this->agent->referrer());
			}
		}


	//new admin panel delete room category

	public function deleteRoomCategory(){

		$res = $this->Admin_model->deleteTable('room_categories','rc_id',$this->input->post('rcid'));
		
		if($res){

			$this->session->set_flashdata('success_msg',"Deleted Successfully!");
			redirect($this->agent->referrer());
		}
		else{

			$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
			redirect($this->agent->referrer());
		}
	}
	

	//new admin panel Delete deleteHomePageImage
		public function deleteHomePageImage($imageid,$imagepage){
			if($imagepage = 'home'){
			$res = $this->Admin_model->deleteTable('home_images_videos','hiv_id',$imageid);
			}
			if($imagepage = 'room'){
			$res = $this->Admin_model->deleteTable('room_images_videos','riv_id',$imageid);	
			}

			// if($res){
			// 	$output= [


			// 	]
			// 	$this->session->set_flashdata('success_msg',"Deleted Successfully!");
			// 	redirect($this->agent->referrer());
			// } else {
			// 	$this->session->set_flashdata('error_msg',"Failed to Delete, please try again!");
			// 	redirect($this->agent->referrer());
			// }

			if($res){
				$datas['result'] = true;
				$datas['msg'] = 'Successfully Deleted File';
				$this
					->session
					->set_flashdata('success_msg', "Successfully Deleted File");
			} else {

				$datas['result'] = false;
				$datas['msg'] = 'Error Deleted File';
				$this
					->session
					->set_flashdata('warn_msg', "Error Deleted File, Please try again.");
			}
			echo json_encode($datas);
		}

		









	}
	
 ?>
