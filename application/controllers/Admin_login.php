<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_login extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	    $language = ($this->session->userdata('language') === 'ITALIAN')?'ITALIAN':'ENGLISH';
	    defined('HOTEL_BANACO_LANGUAGE') OR define('HOTEL_BANACO_LANGUAGE', $language);
    }  
    
	/*-------------------------------------------------------------------------
	| Method : Authenticate admin	
	|-------------------------------------------------------------------------*/
	public function auth_admin() {
			// print_r($_POST);
			// echo $this->session->userdata('authAdminLogin');
			// exit();

		$checkEmail = $this->Admin_model->checkAdmin($this->input->post('email'));
		if($checkEmail){
			$this->load->view('admin/reset_password');
		}


		$condition1 = ['riv_section	'=> 3];
		$condition2 = [
			// 'fasd_language' => HOTEL_BANACO_LANGUAGE,
			'fasd_page_name'=> 'FEATURE'
		];
		$condition3 = [
			// 'fasd_language' => HOTEL_BANACO_LANGUAGE,
			'fasd_page_name'=> 'SERVICE'
		];
		$condition4 = ['riv_section	'=> 2];
		$condition5 = ['hiv_section	'=> 4];
		$condition6 = ['hiv_section	'=> 5];
		$condition7 = ['hiv_section	'=> 6];
		$condition9 = ['hiv_section	'=> 8];
		$condition10 = ['hiv_section	'=> 9];
		// $condition10 = [
		// 	// 'fasd_language' => HOTEL_BANACO_LANGUAGE,
		// 	'rp_room_id'=> $this->session->userdata('room_id')
		// ];

		$data['room_page'] 	   = $this->Admin_model->fetchTable('room_page', 'rp_id')[0];
		// $data['room_page'] = $this->Home_model->fetchTableWithLanguageWhere('room_page',$condition10);

		$data['double_room_slider'] = $this->Admin_model->fetchTableWithLanguageWhere('room_images_videos',$condition1);

		$room_header = $this->Admin_model->fetchTableWithLanguageWhere('room_images_videos',$condition4);
		
		$data['room_header']   = end($room_header);
		$data['room_features'] = $this->Home_model->fetchTableWithLanguageWhere('room_feature_and_service',$condition2);
		$data['room_services'] = $this->Home_model->fetchTableWithLanguageWhere('room_feature_and_service',$condition3);

		$data['home_page'] 	= $this->Admin_model->fetchTable('home_page', 'hp_id')[0];
		$homepagevideo 		= $this->Admin_model->fetchTableWithLanguageWhere('home_images_videos',$condition5);
		$data['homepagevideo'] = end($homepagevideo);
		$data['home_intro_images_section'] = $this->Admin_model->fetchTableWithLanguageWhere('home_images_videos',$condition6);
		$data['p_g_slider_images_section'] = $this->Admin_model->fetchTableWithLanguageWhere('home_images_videos',$condition7);
		$data['p_g_footer_slider_images_section'] = $this->Admin_model->fetchTableWithLanguageWhere('home_images_videos',$condition9);

		$data['experience_images_section'] = $this->Admin_model->fetchTableWithLanguageWhere('home_images_videos',$condition10);

		$data['room_categories'] = $this->Admin_model->getCatofRooms();
		
		// echo '<pre>';
		// print_r($data['home_intro_images_section']);
		// exit();

		if(!$this->session->userdata('authAdminLogin')) {	
			$this->helper->authCookieLogin();
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			// $this->form_validation->set_rules('em', 'Password', 'trim|required');

			if($this->form_validation->run() === FALSE) {
				$this->load->view('admin/login');
			} else {
				// print_r($_POST);
				// $checkEmail = $this->Admin_model->checkAdmin($this->input->post('email'));
				// if($checkEmail){
				// 	echo 'sdfjla';	
				// }
				// exit();
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				// $email	  = $this->input->post('email');
				// echo $email;
				// exit();
				$remember_me = $this->input->post('remember_me');
				$result   = $this->Admin_model->authAdmin($username, $password);

				if($result) {
					$condition1 = [];
					if($remember_me === '1') {
						$this->auth_remember_me($username);
					}
					// echo '<pre>';
					// print_r($data['room_header']);
					// exit();

					$this->load->view('admin/backend',$data);
					// redirect(base_url('admin/backend'));
					// redirect(base_url('admin/backend'));
				} else {
					$this->session->set_flashdata('loginMsg', '<div class="text-center alert alert-danger">Username or Password is incorrect.</div>');
					// redirect(base_url('admin'));
					$this->load->view('admin/backend',$data);
				}
			}
		} else {
			// redirect(base_url('admin/backend',$data));
			// redirect(base_url('admin/backend'));
			// $this->load->view('web-admin');
			// redirect(base_url('web-admin'));
			// redirect(base_url('admin'));
			$this->load->view('admin/backend',$data);
		}

		
	}

	/*-------------------------------------------------------------------------
	| Method : Remember me
	|-------------------------------------------------------------------------*/
	private function auth_remember_me($username) {
		
		$remember_me_token = bin2hex(random_bytes(20));

		$ipaddress = $this->helper->getIpAddress();

		$result = $this->Admin_model->setRememberMeToken($username, $remember_me_token, $ipaddress, $lockscreen = 0);

		if(!empty($result)) {
			$cookie = array(
				'name'     => 'remember_me_token',
				'value'    => $remember_me_token.'|'.$username,
				'expire'   => '2629746',  // one month
				'domain'   => null,
				'path'     => '/',
				'prefix'   => null,
				'secure'   => false,
				'httponly' => true
			);
			set_cookie($cookie);
			return true;
		}

	}

	/*-------------------------------------------------------------------------
	| Method : authenticate lock screen
	|-------------------------------------------------------------------------*/
	public function auth_lockscreen() {

		if($this->session->userdata('authAdminLockscreen') === '1' && $this->session->userdata('authAdminLogin')) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');

			if($this->form_validation->run() === FALSE) {

				$this->load->view('admin/lockscreen');

			} else {

				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$result   = $this->Admin_model->authAdmin($username, $password);

				if($result) {
					// redirect(base_url('web-admin'));
					redirect(base_url('admin'));
				} else {
					$this->session->set_flashdata('loginMsg', '<div class="text-center alert alert-danger">Password is incorrect.</div>');
					redirect(base_url('admin/lockscreen'));
				}

			}

		} else {
			// redirect(base_url('web-admin'));
			redirect(base_url('admin'));
		}
		
	}


  /*-------------------------------------------------------------------------
  | Method : Logout admin
  |-------------------------------------------------------------------------*/
	public function logout() {
		session_destroy();
		$this->Admin_model->destroyLockscreen();
		delete_cookie('remember_me_token');
		// redirect(base_url('admin/backend'));	
		redirect(base_url('web-admin'));
	}


}

?>


