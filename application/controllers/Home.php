<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->helper('url');
  		$this->load->library("pagination");

		// if($this->session->userdata('authAdminLockscreen') === '1' && $this->session->userdata('authAdminLogin')) {
		// 	redirect(base_url('admin/lockscreen'));
		// }	

		// if(!$this->session->userdata('authAdminLogin')) {
		// 	$this->helper->authCookieLogin();
		// 	$this->session->set_flashdata('loginMsg', '<div class="alert alert-warning text-center">Sorry, You are not logged in </div>');
		// 	redirect(base_url('web-admin'));
		// }
		// $this->load->library('email');            
		// var_dump($this->load->is_loaded('email'));
		// exit();
		// if ($this->load->is_loaded('system/Email') === false) {
		// if ($this->load->is_loaded('Email') === false) {
		    // $this->load->library('Email');            
		// }
		$language = ($this->session->userdata('language') === 'ITALIAN')?'ITALIAN':'ENGLISH';
		defined('HOTEL_BANACO_LANGUAGE') OR define('HOTEL_BANACO_LANGUAGE', $language);
		 
	} 
	public function changeLang(){
		
		$this->session->set_userdata('language',$this->input->post('lang'));
		echo json_encode('true');
	}	

	public function index(){
		
		$condition1 = [];
		$condition2 = [];
		$condition3 = [];
		$condition4 = [];

		$this->Home_model->fetchFirstRowTable('contact_detail','cd_id','1')[0];

		// $data['contact_detail']  = $this->Home_model->fetchFirstRowTable('contact_detail','cd_id','1')[0];
		// $data['living_hotel_banaco_room']  = $this->Home_model->fetchTableWithLanguage('living_hotel_banaco_room','lhbr_language',HOTEL_BANACO_LANGUAGE)[0];
		

		// $data['living_hotel_banaco_facilities']  = $this->Home_model->fetchTable('living_hotel_banaco_facilities','lhb_id');

		// $data['home_page_def']  = $this->Home_model->fetchTableWithLanguage('home_page_def','hpd_language',HOTEL_BANACO_LANGUAGE)[0];

		// $data['experience']  = $this->Home_model->fetchTableWithLanguage('experience','e_language',HOTEL_BANACO_LANGUAGE);

		// $data['experience']  = $this->Home_model->fetchTableWithLanguageLimit('experience','e_id','e_language',HOTEL_BANACO_LANGUAGE,2);

		// $condition1 = ['si_image_page	'=> 3];
		// $condition2 = ['si_image_page	'=> 4];
		// $condition3 = ['si_image_page	'=> 5];
		// $condition4 = ['hbv_language' => HOTEL_BANACO_LANGUAGE];

		// $data['hotel_benaco_video'] = $this->Home_model->fetchTableWithDescWhere('hotel_benaco_video','hbv_id',$condition4)[0];
		// $data['p_g_slider_1'] = $this->Home_model->fetchTableWithLanguageWhere('slider_images',$condition1);
		// $data['p_g_video'] = $this->Home_model->fetchTableWithDescWhere('slider_images','si_id',$condition2)[0];
		// $data['p_g_slider_3'] = $this->Home_model->fetchTableWithLanguageWhere('slider_images',$condition3);

		

		// $data['welcome_section_slider'] = $this->Home_model->fetchTable('slider_images','si_id');

		// echo '<pre>';
		// print_r($data['hotel_benaco_video']);
		// exit();
			
		$data['front_page_name'] = 'home-page-def';
		$this->load->view('front/index',$data);
	}

	//camela singola page
	public function cameraSingola(){
		// $condition1 = [];
		// $condition2 = [];
		// $condition3 = [];
		// $condition4 = [];
		
		// $data['contact_detail']  = $this->Home_model->fetchFirstRowTable('contact_detail','cd_id','1')[0];
		// $data['double_room'] = $this->Home_model->fetchTableWithLanguage('double_room','dr_language',HOTEL_BANACO_LANGUAGE)[0];
		// $data['feature_and_service'] = $this->Home_model->fetchTableWithLanguage('feature_and_service','fas_language',HOTEL_BANACO_LANGUAGE)[0];

		// $data['discover_other_room'] = $this->Home_model->fetchTableWithLanguage('discover_other_room','dor_language',HOTEL_BANACO_LANGUAGE)[0];

		// $data['discover_other_room_facilities'] = $this->Home_model->fetchTableWithLanguage('discover_other_room_facilities','dorf_language',HOTEL_BANACO_LANGUAGE);

		// $condition1 = [
		// 	'fasd_language' => HOTEL_BANACO_LANGUAGE,
		// 	'fasd_page_name'=> 'FEATURE'
		// ];
		// $condition2 = [
		// 	'fasd_language' => HOTEL_BANACO_LANGUAGE,
		// 	'fasd_page_name'=> 'SERVICE'
		// ];

		// $data['room_features'] = $this->Home_model->fetchTableWithLanguageWhere('feature_and_service_detail',$condition1);

		// $data['room_services'] = $this->Home_model->fetchTableWithLanguageWhere('feature_and_service_detail',$condition2);

		// $condition3 = ['si_image_page	'=> 6];
		// $data['double_room_slider'] = $this->Home_model->fetchTableWithLanguageWhere('slider_images',$condition3)[0];

		// $condition4 = ['si_image_page	'=> 10];
		// $data['room_images'] = $this->Home_model->fetchTableWithDescWhere('slider_images','si_id',$condition4)[0];

		// echo '<pre>';
		// print_r($data['double_room_slider']);
		// exit();

		$data['front_page_name'] = 'camera-singola';
		$this->load->view('front/index',$data);
	}	



	/*-------------------------------------------------------------------------
	| Method : Remember me
	|-------------------------------------------------------------------------*/
	private function auth_remember_me($username) {
		
		$remember_me_token = bin2hex(random_bytes(20));

		$ipaddress = $this->helper->getIpAddress();

		$result = $this->Home_model->setRememberMeToken($username, $remember_me_token, $ipaddress);

		if(!empty($result)) {
			$cookie = array(
				'name'     => 'remember_me_token',
				'value'    => $remember_me_token.'|'.$username,
				'expire'   => '2629746',  // one month
				'domain'   => null,
				'path'     => '/',
				'prefix'   => null,
				'secure'   => false,
				'httponly' => true
			);
			set_cookie($cookie);
			return true;
		}

	}

	public function signUp()
	{
		$this->form_validation->set_rules('user','User Name','trim|required');
		$this->form_validation->set_rules('email','Email','trim|required|is_unique[user.user_email]');
		$this->form_validation->set_rules('psw','Password','trim|required');
		$this->form_validation->set_rules('psw-repeat','Repeat Password','trim|required|matches[psw]');
		
		if($this->form_validation->run() == false){
			$datas['result'] = false;
			$datas['msg']	 = validation_errors();
		} else {
			$user_insert_id = $this->Home_model->createNewUser();
			/*$dd = $this->upload_image('company_profile','company_profile','filename','uni_id', 	'uni_image', $uni_insert_id);*/

			// echo '<pre>';
			// echo json_encode(strip_tags(strip_tags($dd['data']['error'])));
			// exit();
			
			if($user_insert_id ){
				// $login = array(
				// 	'logged_in' => true,
				// 	'email'     => $this->input->post('email'),
				// 	'username'  => ucfirst($this->input->post('user'))
				// );
				// $this->session->set_userdata($login);
				$datas['result']  = true;
				$datas['msg']	=  'Successfully registered account.';
				$this->session->set_flashdata('success_msg',"Successfully Added");
			} else {
				$datas['result']  = false;
				$datas['msg']	=  'Failed to registered, Please try again.';
				$this->session->set_flashdata('warn_msg',"Failed to Add, Please try again.");
			}
		}

		echo json_encode($datas);

	}
	
	public function checkLogin()
	{	

		// echo json_encode($_POST);
		// exit();	
			// $this->helper->authCookieLogin();
			$this->form_validation->set_rules('user','User Name','trim|required');
			$this->form_validation->set_rules('psw','Password','trim|required');

			if($this->form_validation->run() == false){
				$datas['result'] = false;
				$datas['msg']	 = validation_errors();
			} else {
				
				$username = $this->input->post('user');
				$password = $this->input->post('psw');
				// $email	  = $this->input->post('email');
				// echo $email;
				// exit();
				$remember_me = $this->input->post('remember_me');
				$result   = $this->Home_model->authUser($username, $password);
				// echo json_encode($result);
				// exit();

				if($result) {
					if($remember_me === '1') {
						$this->auth_remember_me($username);
					}

						$datas['result']  = true;
						$datas['msg']	=  'Successfully login.';
						$this->session->set_flashdata('success_msg',"Successfully Added");
					} else {
						$datas['result']  = false;
						$datas['msg']	=  'Failed to login, Please try again.';
						$this->session->set_flashdata('warn_msg',"Failed to Add, Please try again.");
					}
				
				echo json_encode($datas);
			}
		



		
		
	
	
	}


	/*-------------------------------------------------------------------------
	| Method : insertNeedInformation
	|-------------------------------------------------------------------------*/
	public function insertNeedInformation(){
		
		$this->form_validation->set_rules('username','Name','trim|required');
		$this->form_validation->set_rules('email','Email','trim|required');
		$this->form_validation->set_rules('object','Object','trim|required');
		$this->form_validation->set_rules('message','Message','trim|required');
		
		if($this->form_validation->run() == false){
			$datas['result'] = false;
			$datas['msg']	 = validation_errors();
		}

		else {
			
			$uni_insert_id = $this->Home_model->insertNeedInformation();
			
			/*$dd = $this->upload_image('company_profile','company_profile','filename','uni_id', 	'uni_image', $uni_insert_id);*/

			// echo '<pre>';
			// echo json_encode(strip_tags(strip_tags($dd['data']['error'])));
			// exit();
			
			if($uni_insert_id ){
				$datas['result']  = true;
				$datas['msg']	=  'Successfully Sent';
				$this->session->set_flashdata('success_msg',"Successfully Sent");
			}
			else{
				$datas['result']  = false;
				$datas['msg']	=  'Failed to Add, Please try again.';
				$this->session->set_flashdata('warn_msg',"Failed to Add, Please try again.");
			}
		}

		echo json_encode($datas);

	}





























}?>