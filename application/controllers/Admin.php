<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this
			->load
			->helper('url');
		$this
			->load
			->library("pagination");

		// $this->load->library('CKEditor');
		// $this->load->helper('ckeditor_helper');
		// $this->ckeditor->basePath = base_url().'asset/ckeditor/';
		// $this->ckeditor->config['toolbar'] = array(
		//                 array( 'Source', '-', 'Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList' )
		//                                                     );
		// $this->ckeditor->config['language'] = 'it';
		// $this->ckeditor->config['width'] = '730px';
		// $this->ckeditor->config['height'] = '300px';


		if ($this
				->session
				->userdata('authAdminLockscreen') === '1' && $this
				->session
				->userdata('authAdminLogin')) {
			redirect(base_url('admin/lockscreen'));
		}
		if (!$this
			->session
			->userdata('authAdminLogin')) {
			$this
				->helper
				->authCookieLogin();
			// $this->session->set_flashdata('loginMsg', '<div class="alert alert-warning text-center">Sorry, You are not logged in </div>');
			redirect(base_url('web-admin'));
			
		}

		$language = ($this->session->userdata('language') === 'ITALIAN') ? 'ITALIAN' : 'ENGLISH';
		defined('HOTEL_BANACO_LANGUAGE') or define('HOTEL_BANACO_LANGUAGE', $language);
	}

	public function index()
	{
		$user = $this
			->Admin_model
			->countUser();
		$user_messages = $this
			->Admin_model
			->countUserMessages();
		$this
			->load
			->view('admin/index', compact('user', 'user_messages'));
		// $this
		//     ->load
		//     ->view('admin/index');
	}

	public function backend()
	{

		$this
			->load
			->view('admin/backend');

	}

	public function resetPassword()
	{
		echo 'hii';
		print_r($_POST);
		exit();
		// $this->load->view('admin/reset_password');

	}

	

	public function userNeedInformationDetail()
	{
		$user_need_information = $this
			->Admin_model
			->fetchTable('user_need_information', 'uni_id');
		$this
			->load
			->view('admin/user_need_information_detail', compact('user_need_information'));
	}

	public function contactDetail()
	{
		$this
			->load
			->view('admin/contact_detail');
	}

	public function contactDetailView()
	{
		$data = $this
			->Admin_model
			->fetchFirstRowTable('contact_detail', 'cd_id', 1);
		$this
			->load
			->view('admin/contact_detail_view', compact('data'));
	}


	public function userDetail()
	{
		$users = $this
			->Admin_model
			->fetchTable('user', 'user_id');
		$this
			->load
			->view('admin/users', compact('users'));
	}


	public function upload_image($table = '', $path_folder = '', $file_input_name = '', $table_id_name = '', $table_image_name = '', $insert_id = '')
	{
		// print_r($_FILES);
		// exit();
		$new_name = "file_" . strtolower(uniqid() . time() . '.' . pathinfo($_FILES[$file_input_name]['name'], PATHINFO_EXTENSION));

		if (!file_exists('./uploads/' . $path_folder)) {
			if (!is_dir('./uploads/' . $path_folder)) {
				// mkdir('./uploads/'.$path_folder, 0777, true);
				try {
					if (!mkdir('./uploads/' . $path_folder, 0777, true)) {
						throw new Exception('No Folder Created');
						clearstatcache();
					}
				} catch (Exception $e) {
					$output = ['status' => false, 'data' => '', 'msg' => $e->getMessage()];
				}
			}
		}

		$output = array();
		$config['upload_path'] = './uploads/' . $path_folder;
		$config['allowed_types'] = 'gif|jpg|png|mp4|pdf';
		$config['max_size'] = 100500;
		$config['max_width'] = 10048;
		$config['max_height'] = 10048;
		$config['file_name'] = $new_name;

		// $this
		//     ->load
		//     ->library('upload', $config);
		$this->upload->initialize($config);


		// echo base_url().'uploads/'.$path_folder.'/'.$new_name;
		// exit();
		if (!$this
			->upload
			->do_upload($file_input_name)) {
			$error = array(
				'error' => $this
					->upload
					->display_errors()
			);
			$output = ['status' => false, 'data' => $error,
				// 'data' => '',
				'msg' => 'Upload file Error'];
		} else {
			// if($error == 'Choose Image')
			// $data2 = array('upload_data' => $this->upload->data());
			$data = array(
				$table_image_name => 'uploads/' . $path_folder . '/' . $new_name
			);

			$data = $this
				->security
				->xss_clean($data);
			if (empty($insert_id)) {
				$this
					->db
					->insert($table, $data);
			} else {
				$this
					->db
					->where($table_id_name, $insert_id);
				$this
					->db
					->update($table, $data);
			}
			// $this->session->set_flashdata('msg', 'File Uploaded Successfully');
			// echo "File uploaded Successfully!";
			$output = ['status' => true, 'data' => '', 'msg' => 'File Uploaded Successfully'];
		}

		return $output;
	}

	public function upload_image_single($table = '', $path_folder = '',$file_input_name = '', $table_id_name = '', $table_image_name = '', $table_imagepage_name = '', $insert_id = '')
	{
		// print_r($_FILES);
		// exit();
		$new_name = "file_" . strtolower(uniqid() . time() . '.' . pathinfo($_FILES[$file_input_name]['name'], PATHINFO_EXTENSION));

		if (!file_exists('./uploads/' . $path_folder)) {
			if (!is_dir('./uploads/' . $path_folder)) {
				// mkdir('./uploads/'.$path_folder, 0777, true);
				try {
					if (!mkdir('./uploads/' . $path_folder, 0777, true)) {
						throw new Exception('No Folder Created');
						clearstatcache();
					}
				} catch (Exception $e) {
					$output = ['status' => false, 'data' => '', 'msg' => $e->getMessage()];
				}
			}
		}

		$output = array();
		$config['upload_path'] = './uploads/' . $path_folder;
		$config['allowed_types'] = 'gif|jpg|png|mp4|pdf';
		$config['max_size'] = 100500;
		$config['max_width'] = 10048;
		$config['max_height'] = 10048;
		$config['file_name'] = $new_name;

		// $this
		//     ->load
		//     ->library('upload', $config);
		$this->upload->initialize($config);


		// echo base_url().'uploads/'.$path_folder.'/'.$new_name;
		// exit();
		if (!$this
			->upload
			->do_upload($file_input_name)) {
			$error = array(
				'error' => $this
					->upload
					->display_errors()
			);
			$output = ['status' => false, 'data' => $error,
				// 'data' => '',
				'msg' => 'Upload file Error'];
		} else {
			// if($error == 'Choose Image')
			// $data2 = array('upload_data' => $this->upload->data());
			$data = array(
				$table_image_name => 'uploads/' . $path_folder . '/' . $new_name,
				$table_imagepage_name => $insert_id
			);

			$data = $this
				->security
				->xss_clean($data);
			
				$this
					->db
					->insert($table, $data);
			
			// $this->session->set_flashdata('msg', 'File Uploaded Successfully');
			// echo "File uploaded Successfully!";
			$output = ['status' => true, 'data' => '', 'msg' => 'File Uploaded Successfully'];
		}

		return $output;
	}

	public function upload_image_multiple($table = '', $path_folder = '',$file_input_name = '', $table_id_name = '', $table_image_name = '', $table_imagepage_name = '', $insert_id = '')

	{
		if (!file_exists('./uploads/' . $path_folder)) {
			if (!is_dir('./uploads/' . $path_folder)) {
				try {
					if (!mkdir('./uploads/' . $path_folder, 0777, true)) {
						throw new Exception('No Folder Created');
						clearstatcache();
					}
				} catch (Exception $e) {
					$output = ['status' => false, 'data' => '', 'msg' => $e->getMessage()];
				}
			}
		}

		$countfiles = count($_FILES[$file_input_name]['name']);

		for ($i = 0; $i < $countfiles; $i++) {

			if (!empty($_FILES[$file_input_name]['name'][$i])) {
				$new_name = "file_" . strtolower(uniqid($i) . '.' . pathinfo($_FILES[$file_input_name]['name'][$i], PATHINFO_EXTENSION));

				$_FILES['file']['name'] = $new_name;
				$_FILES['file']['type'] = $_FILES[$file_input_name]['type'][$i];
				$_FILES['file']['tmp_name'] = $_FILES[$file_input_name]['tmp_name'][$i];
				$_FILES['file']['error'] = $_FILES[$file_input_name]['error'][$i];
				$_FILES['file']['size'] = $_FILES[$file_input_name]['size'][$i];

				$output = array();
				$config['upload_path'] = './uploads/' . $path_folder;
				$config['allowed_types'] = 'gif|jpg|png|mp4|pdf';
				$config['max_size'] = 1005000;
				$config['max_width'] = 10048;
				$config['max_height'] = 10048;
				$config['file_name'] = $new_name;

				$this->upload->initialize($config);

				// $this
				//     ->load
				//     ->library('upload', $config);

				if (!$this
					->upload
					->do_upload('file')) {
					$error = array(
						'error' => $this
							->upload
							->display_errors()
					);
					$output = ['status' => false, 'data' => $error,
						// 'data' => '',
						'msg' => 'Upload file Error'];
				} else {

					$data = array(
						$table_image_name => 'uploads/' . $path_folder . '/' . $new_name,
						$table_imagepage_name => $insert_id
					);

					$data = $this
						->security
						->xss_clean($data);


					// print_r($data);

					$this
						->db
						->insert($table, $data);

				}

			} //end loop for

		} //end count if

		$output = ['status' => true, 'data' => '', 'msg' => 'File Uploaded Successfully'];
		return $output;
	}


	public function upload_image_single_room($table = '', $path_folder = '',$file_input_name = '', $table_id_name = '', $table_image_name = '', $table_room_column_name = '', $room_id = '', $table_imagepage_name = '', $insert_id = '')
	{
		// print_r($_FILES);
		// exit();
		$new_name = "file_" . strtolower(uniqid() . time() . '.' . pathinfo($_FILES[$file_input_name]['name'], PATHINFO_EXTENSION));

		if (!file_exists('./uploads/' . $path_folder)) {
			if (!is_dir('./uploads/' . $path_folder)) {
				// mkdir('./uploads/'.$path_folder, 0777, true);
				try {
					if (!mkdir('./uploads/' . $path_folder, 0777, true)) {
						throw new Exception('No Folder Created');
						clearstatcache();
					}
				} catch (Exception $e) {
					$output = ['status' => false, 'data' => '', 'msg' => $e->getMessage()];
				}
			}
		}

		$output = array();
		$config['upload_path'] = './uploads/' . $path_folder;
		$config['allowed_types'] = 'gif|jpg|png|mp4|pdf';
		$config['max_size'] = 100500;
		$config['max_width'] = 10048;
		$config['max_height'] = 10048;
		$config['file_name'] = $new_name;

		// $this
		//     ->load
		//     ->library('upload', $config);
		$this->upload->initialize($config);


		// echo base_url().'uploads/'.$path_folder.'/'.$new_name;
		// exit();
		if (!$this
			->upload
			->do_upload($file_input_name)) {
			$error = array(
				'error' => $this
					->upload
					->display_errors()
			);
			$output = ['status' => false, 'data' => $error,
				// 'data' => '',
				'msg' => 'Upload file Error'];
		} else {
			// if($error == 'Choose Image')
			// $data2 = array('upload_data' => $this->upload->data());
			$data = array(
				$table_image_name => 'uploads/' . $path_folder . '/' . $new_name,
				$table_room_column_name => $room_id,
				$table_imagepage_name => $insert_id
			);

			$data = $this
				->security
				->xss_clean($data);
			
				$this
					->db
					->insert($table, $data);
			
			// $this->session->set_flashdata('msg', 'File Uploaded Successfully');
			// echo "File uploaded Successfully!";
			$output = ['status' => true, 'data' => '', 'msg' => 'File Uploaded Successfully'];
		}

		return $output;
	}

	public function upload_image_multiple_room($table = '', $path_folder = '',$file_input_name = '', $table_id_name = '', $table_image_name = '', $table_imagepage_name = '', $insert_id = '')

	{
		if (!file_exists('./uploads/' . $path_folder)) {
			if (!is_dir('./uploads/' . $path_folder)) {
				try {
					if (!mkdir('./uploads/' . $path_folder, 0777, true)) {
						throw new Exception('No Folder Created');
						clearstatcache();
					}
				} catch (Exception $e) {
					$output = ['status' => false, 'data' => '', 'msg' => $e->getMessage()];
				}
			}
		}

		$countfiles = count($_FILES[$file_input_name]['name']);

		for ($i = 0; $i < $countfiles; $i++) {

			if (!empty($_FILES[$file_input_name]['name'][$i])) {
				$new_name = "file_" . strtolower(uniqid($i) . '.' . pathinfo($_FILES[$file_input_name]['name'][$i], PATHINFO_EXTENSION));

				$_FILES['file']['name'] = $new_name;
				$_FILES['file']['type'] = $_FILES[$file_input_name]['type'][$i];
				$_FILES['file']['tmp_name'] = $_FILES[$file_input_name]['tmp_name'][$i];
				$_FILES['file']['error'] = $_FILES[$file_input_name]['error'][$i];
				$_FILES['file']['size'] = $_FILES[$file_input_name]['size'][$i];

				$output = array();
				$config['upload_path'] = './uploads/' . $path_folder;
				$config['allowed_types'] = 'gif|jpg|png|mp4|pdf';
				$config['max_size'] = 1005000;
				$config['max_width'] = 10048;
				$config['max_height'] = 10048;
				$config['file_name'] = $new_name;

				$this->upload->initialize($config);

				// $this
				//     ->load
				//     ->library('upload', $config);

				if (!$this
					->upload
					->do_upload('file')) {
					$error = array(
						'error' => $this
							->upload
							->display_errors()
					);
					$output = ['status' => false, 'data' => $error,
						// 'data' => '',
						'msg' => 'Upload file Error'];
				} else {

					$data = array(
						$table_image_name => 'uploads/' . $path_folder . '/' . $new_name,
						$table_imagepage_name => $insert_id
					);

					$data = $this
						->security
						->xss_clean($data);


					// print_r($data);

					$this
						->db
						->insert($table, $data);

				}

			} //end loop for

		} //end count if

		$output = ['status' => true, 'data' => '', 'msg' => 'File Uploaded Successfully'];
		return $output;
	}


		/*-------------------------------------------------------------------------
		| Method : Insert homePage
		|--------------------------------------------------------------------------*/

		public function homePage()
		{

			echo '<pre>';
			print_r($_POST);
			print_r($_FILES);
			// echo json_encode($_POST);
			// echo json_encode($_FILES);
			// ini_set('display_errors', 1);
			// exit();
			$this
				->form_validation
				->set_rules('hotel_benaco_heading', 'Hotel Benaco Heading', 'trim|required');
			$this
				->form_validation
				->set_rules('intro_header_title', 'in', 'trim|required');

			if ($this
					->form_validation
					->run() == false) {
				$datas['result'] = false;
				$datas['msg'] = validation_errors();
			} else {
				$hp_text_insert_id = $this
					->Admin_model
					->insertHomeDetail();
					
				// public function upload_image_multiple($table = '', $path_folder = '', $file_input_name = '', $table_id_name = '', $table_image_name = '', $table_imagepage_name = '', $insert_id = '')
				// public function upload_image($table = '', $path_folder = '', $file_input_name = '', $table_id_name = '', $table_image_name = '', $insert_id = '');
				
	            $hotel_benaco_video = $this->upload_image_single('home_images_videos', 'home_images_videos', 'upload_home_video', 'hiv_id', 'hiv_path','hiv_section', $this->input->post('hotel_benaco_video_section'));

	            $home_intro_images = $this->upload_image_multiple('home_images_videos', 'home_images_videos', 'upload_home_intro_photo', 'hiv_id', 'hiv_path','hiv_section', $this->input->post('home_intro_images_section'));

	            $p_g_slider_images = $this->upload_image_multiple('home_images_videos', 'home_images_videos', 'p_g_slider_images', 'hiv_id', 'hiv_path','hiv_section', $this->input->post('p_g_slider_images_section'));

	            $experience_images = $this->upload_image_multiple('home_images_videos', 'home_images_videos', 'experience_images', 'hiv_id', 'hiv_path','hiv_section', $this->input->post('experience_images_section'));

	             // $pool_and_garden_video = $this->upload_image_single('home_images_videos', 'home_images_videos', 'pool_and_garden_video', 'hiv_id', 'hiv_path','hiv_section', $this->input->post('pool_and_garden_video_section'));

	             $p_g_footer_slider_images = $this->upload_image_multiple('home_images_videos', 'home_images_videos', 'p_g_footer_slider_images', 'hiv_id', 'hiv_path','hiv_section', $this->input->post('p_g_footer_slider_images_section'));



				
	   //          echo json_encode($hp_text_insert_id);
				// echo json_encode($hotel_benaco_video);
				// echo json_encode($home_intro_images);
				// echo json_encode($p_g_slider_images);
				// // echo json_encode($pool_and_garden_video);
				// echo json_encode($p_g_footer_slider_images);
				// exit();

				if ($hp_text_insert_id || $hotel_benaco_video || $home_intro_images || $p_g_slider_images || $p_g_footer_slider_images) {

					$datas['result'] = true;
					if($hp_text_insert_id == 'updated'):
					$datas['msg'] = 'Successfully Updated';
					else:
					$datas['msg'] = 'Successfully Added';
					endif;
					$this
						->session
						->set_flashdata('success_msg', "Successfully Added");
				} else {

					$datas['result'] = false;
					$datas['msg'] = strip_tags(strip_tags($hotel_benaco_video['data']['error']));
					$this
						->session
						->set_flashdata('warn_msg', "Failed to Add, Please try again.");
				}
			}

			echo json_encode($datas);
		}

	/*-------------------------------------------------------------------------
	| Method : Insert roomPage
	|--------------------------------------------------------------------------*/

	public function roomPage()
	{
		echo '<pre>';
		print_r($_POST);
		print_r($_FILES);
		// echo json_encode($_POST);
		// echo json_encode($_FILES);
		// exit();
		$this
			->form_validation
			->set_rules('room_title', 'Room Title', 'trim|required');
		$this
			->form_validation
			->set_rules('intro_text', 'Intro Text', 'trim|required');

		if ($this
				->form_validation
				->run() == false) {
			$datas['result'] = false;
			$datas['msg'] = validation_errors();
		} else {

			// $rp_delete_tables_data = $this
			// 	->Admin_model
			// 	->emptyTables(['room_feature_and_service','room_images_videos']);

			$rp_text_insert_id = $this
				->Admin_model
				->insertRoomDetail();

		

			// public function upload_image_multiple($table = '', $path_folder = '', $file_input_name = '', $table_id_name = '', $table_image_name = '', $table_imagepage_name = '', $insert_id = '')
            // $room_video = $this->upload_image_multiple('room_images_videos', 'room_images_videos', 'upload_room_video', 'riv_id', 'riv_path','riv_section', $this->input->post('room_video_section'));

			$header_image = $this->upload_image_single_room('room_images_videos', 'room_images_videos', 'upload_room_photo', 'riv_id', 'riv_path','riv_room_id',$this->input->post('room_category_id'),'riv_section', $this->input->post('header_image_section'));

            $double_room_slider = $this->upload_image_multiple_room('room_images_videos', 'room_images_videos', 'double_room_slider', 'riv_id', 'riv_path','riv_room_id',$this->input->post('room_category_id'),'riv_section', $this->input->post('double_room_slider_section'));

            $living_hotel_banaco_images = $this->upload_image_single_room('room_images_videos', 'room_images_videos', 'living_hotel_banaco_images', 'riv_id', 'riv_path','riv_room_id',$this->input->post('room_category_id'),'riv_section', $this->input->post('living_hotel_banaco_images_section'));

            $fasd_insert_id = $this
                ->Admin_model
                ->insertRoomFeature();
            $sasd_insert_id = $this
                ->Admin_model
                ->insertRoomService();

			// $rp_file_insert_id = $this
			//     ->Admin_model
			//     ->insertRoomFiles();

			// echo json_encode($header_image);
			// echo json_encode($fasd_insert_id);
   //          echo json_encode($rp_text_insert_id);
			// echo json_encode($double_room_slider);
			// echo json_encode($fasd_insert_id);
			// echo json_encode($sasd_insert_id);
			// exit();

			if ($rp_text_insert_id || $header_image || $double_room_slider || $fasd_insert_id || $sasd_insert_id) {
				$datas['result'] = true;
				if($rp_text_insert_id == 'updated'):
				$datas['msg'] = 'Successfully Updated';
				else:
				$datas['msg'] = 'Successfully Added';
				endif;
				$this
					->session
					->set_flashdata('success_msg', "Successfully Added");
			} else {
				$datas['result'] = false;
				$datas['msg'] = 'Something went wrong.';
				$this
					->session
					->set_flashdata('warn_msg', "Failed to Add, Please try again.");
			}
		}
		echo json_encode($datas);
	}


	function getRoomDetail($key){
		$data = [];
		$condition10 = [
			'rp_language' => HOTEL_BANACO_LANGUAGE,
			'rp_room_id'=> $key
		];

		$condition11 = [
			'riv_room_id'=> $key,
			'riv_section'=> 2
		];
		$condition12 = [
			'riv_room_id'=> $key,
			'riv_section'=> 3
		];

		$condition13 = [
			'fasd_language' => HOTEL_BANACO_LANGUAGE,
			'fasd_room_id'=> $key,
			'fasd_page_name'=> 'FEATURE'
		];
		$condition14 = [
			'fasd_language' => HOTEL_BANACO_LANGUAGE,
			'fasd_room_id'=> $key,
			'fasd_page_name'=> 'SERVICE'
		];


		$condition15 = [
			'riv_room_id'=> $key,
			'riv_section'=> 10
		];
		

		$data['room_page'] =  $this->Home_model->fetchTableWithLanguageWhere('room_page',$condition10)[0];


		$data['room_header_image'] = $this->Home_model->fetchTableWithDescWhere('room_images_videos','riv_id',$condition11);
		$data['double_room_gallery_image'] = $this->Home_model->fetchTableWithLanguageWhere('room_images_videos',$condition12);

		
		$data['room_features'] = $this->Home_model->fetchTableWithLanguageWhere('room_feature_and_service',$condition13);
		$data['room_services'] = $this->Home_model->fetchTableWithLanguageWhere('room_feature_and_service',$condition14);


		$data['living_hotel_banaco_images'] = $this->Home_model->fetchTableWithDescWhere('room_images_videos','riv_id',$condition15);
		
		
		echo json_encode($data);
	}



	public function insertRoomCategory(){
		// echo json_encode($_POST);
		// exit();
		$output = array();
		$catAlready = $this->Admin_model->checkAlreadyRoomCategories(strtoupper($this->input->post('room_cat_name')));
		if($catAlready){
		$data = [
			'rc_name' => ucfirst($this->input->post('room_cat_name')),
			'rc_italian_name' => $this->input->post('room_cat_italian_name'),
			'rc_name_key' => str_replace(' ','-',strtolower($this->input->post('room_cat_name')))
		];
		$catRes = $this->Admin_model->insertRoomCategories($data);
		$output = [
			'status' => true,
			'massage' => 'Successfully inserted category.'
			];
			$this->session->set_flashdata('success_msg',"Successfully Added");

		} else {
		$output = [
			'status' => false,
			'massage' => 'Duplicate Entry.'
			];
			$this->session->set_flashdata('warn_msg',"Failed to Add, Please try again.");
		}
		// if($catRes){
		// 	$this->session->flashdata('msg','Successfully Added Category');
		// } else {
		// 	$this->session->flashdata('msg','Something Went Wrong');
		// }
		echo json_encode($output);
	}


	public function updateRoomCategory(){
		// echo json_encode($_POST);
		// exit();
		$output = array();
		$catAlready = $this->Admin_model->checkAlreadyRoomCategories(ucfirst($this->input->post('room_cat_name')));
		if($catAlready){
		$data = [
			'rc_name' => ucfirst($this->input->post('room_cat_name')),
			'rc_italian_name' => $this->input->post('room_cat_italian_name'),
			'rc_name_key' => str_replace(' ','-',strtolower($this->input->post('room_cat_name')))
		];
		$catRes = $this->Admin_model->updateRoomCategories($data,$this->input->post('rcid'));
		// echo json_encode($data);
		// echo json_encode($catRes);
		// exit();
		if($catRes){
		$output = [
			'status' => true,
			'massage' => 'Successfully updated category.'
			];
			$this->session->set_flashdata('success_msg',"Successfully Added");
		} else {
		$output = [
			'status' => false,
			'massage' => 'No changes made.'
			];
			$this->session->set_flashdata('warn_msg',"Failed to update, Please try again.");
		  }
		} else{
			$output = [
				'status' => false,
				'massage' => 'Dublicate Entry.'
				];
				$this->session->set_flashdata('warn_msg',"Failed to update same entry, Please try again.");
		}
		echo json_encode($output);
	}










	/*-------------------------------------------------------------------------
	   | Method : Insert insertContactDetail
	   |--------------------------------------------------------------------------*/

	public function insertContactDetail()
	{
		// echo json_encode(str_replace(array("\r","\n",'\r','\n',"<\/p>"),' ', $this->input->post('visit')));
		// exit();
		$this
			->form_validation
			->set_rules('write_email', 'Write Email', 'trim|required');
		$this
			->form_validation
			->set_rules('contact_no', 'Contact No.', 'trim|required');
		// // $this->form_validation->set_rules('desc','Description','trim|required');
		if ($this
				->form_validation
				->run() == false) {
			$datas['result'] = false;
			$datas['msg'] = validation_errors();
		} else {
			$cd_insert_id = $this
				->Admin_model
				->insertContactDetail();
			// upload_image($table='',$path_folder='',$file_input_name='',$table_id_name='', $table_image_name='', $insert_id='')
			$dd = $this->upload_image('contact_detail', 'contact_detail', 'filename', 'cd_id', 'cd_image', $cd_insert_id);

			if ($cd_insert_id && $dd['status']) {

				$datas['result'] = true;
				$datas['msg'] = 'Success';
				$this
					->session
					->set_flashdata('success_msg', "Successfully Added");
			} else {

				$datas['result'] = false;
				$datas['msg'] = strip_tags(strip_tags($dd['data']['error']));
				$this
					->session
					->set_flashdata('warn_msg', "Failed to Add, Please try again.");
			}
		}

		echo json_encode($datas);
	}

	/*-------------------------------------------------------------------------
	| Method : Update updateContactDetail
	|-------------------------------------------------------------------------*/

	public function updateContactDetail()
	{

		$upd_id = $this
			->Admin_model
			->insertContactDetail();
		// upload_image($table='',$path_folder='',$file_input_name='',$table_id_name='', $table_image_name='', $insert_id='')
		$dd = $this->upload_image('contact_detail', 'contact_detail', 'filename', 'cd_id', 'cd_image', $upd_id);
		if ($upd_id || $dd['status']) {
			$datas['result'] = true;
			$datas['msg'] = "Success";
			$this
				->session
				->set_flashdata('success_msg', 'Successfully Updated');
		} else {
			$datas['result'] = false;
			$datas['msg'] = strip_tags(strip_tags($dd['data']['error']));
			$this
				->session
				->set_flashdata('warn_msg', 'You have not made any changes.');
		}

		echo json_encode($datas);
	}



	/*-------------------------------------------------------------------------
	| Method : insert insertContactUs
	|--------------------------------------------------------------------------*/

	public function insertContactUs()
	{

		// $this->form_validation->set_rules('address','Address','trim|required');
		$this
			->form_validation
			->set_rules('phone', 'Phone', 'trim|required');
		$this
			->form_validation
			->set_rules('email', 'Email', 'trim|required');
		$this
			->form_validation
			->set_rules('working_hours', 'Working Hours', 'trim|required');
		// $this->form_validation->set_rules('download_brochure','Download Brochure','trim|required');
		if ($this
				->form_validation
				->run() == false) {
			$datas['result'] = false;
			$datas['msg'] = validation_errors();
		} else {
			$cu_insert_id = $this
				->Admin_model
				->insertContactUs();
			// upload_image($table='',$path_folder='',$file_input_name='',$table_id_name='', $table_image_name='', $insert_id='')
			$dd = $this->upload_image('contact_us', 'contact_us', 'filename', 'cu_id', 'cu_brochure', $cu_insert_id);

			if ($cu_insert_id) {
				$datas['result'] = true;
				$datas['msg'] = 'Success';
				$this
					->session
					->set_flashdata('success_msg', "Successfully Added");
			} else {

				$datas['result'] = false;
				$datas['msg'] = strip_tags(strip_tags($dd['data']['error']));
				$this
					->session
					->set_flashdata('warn_msg', "Failed to Add, Please try again.");
			}
		}

		echo json_encode($datas);
	}

	/*-------------------------------------------------------------------------
	| Method : Update updateContactUs
	|-------------------------------------------------------------------------*/

	public function updateContactUs()
	{

		$upd_id = $this
			->Admin_model
			->insertContactUs();
		// upload_image($table='',$path_folder='',$file_input_name='',$table_id_name='', $table_image_name='', $insert_id='')
		// $dd = $this->upload_image('contact_us','contact_us','filename','cu_id', 'cu_image', $upd_id);
		if ($upd_id) {
			$datas['result'] = true;
			$datas['msg'] = "Success";
			$this
				->session
				->set_flashdata('success_msg', 'Successfully Updated');
		} else {
			$datas['result'] = false;
			$datas['msg'] = strip_tags(strip_tags($dd['data']['error']));
			$this
				->session
				->set_flashdata('warn_msg', 'You have not made any changes.');
		}

		echo json_encode($datas);
	}

	

	// 	/*-------------------------------------------------------------------------
	// 	| Method : Blank
	// 	|-------------------------------------------------------------------------*/
	// 	public function blank(){
	// 		$this->load->view('admin/blank');
	// 	}
	// 	/*-------------------------------------------------------------------------
	// 	| Method : Setting
	// 	|-------------------------------------------------------------------------*/

	public function setting()
	{
		$this
			->form_validation
			->set_rules('opassword', 'Old Password', 'trim|required');
		$this
			->form_validation
			->set_rules('npassword', 'New Password', 'trim|required|min_length[8]');
		$this
			->form_validation
			->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[npassword]');

		if ($this
				->form_validation
				->run() == false) {
			$data['error'] = validation_errors();
			$this
				->load
				->view('admin/setting', $data);
		} else {
			$uid = $this
				->session
				->userdata('authAdminLogin');
			$result = $this
				->Admin_model
				->oldPasswordCheck($uid);
			if ($result) {

				$result = $this
					->Admin_model
					->changePassword($uid);

				if ($result) {

					$this
						->session
						->set_flashdata('success_msg', "Password Updated!");
					redirect($this
						->agent
						->referrer());

				} else {

					$this
						->session
						->set_flashdata('error_msg', "Wrong Password");
					redirect($this
						->agent
						->referrer());

				}
			} else {

				$this
					->session
					->set_flashdata('warn_msg', "Old Password is incorrect!");
				redirect($this
					->agent
					->referrer());
			}
		}
	}

	/*-------------------------------------------------------------------------
	| Method : Admin set lock screen
	|-------------------------------------------------------------------------*/
	public function set_lockscreen()
	{
		$result = $this
			->Admin_model
			->setLockscreen();
		redirect(base_url('admin/lockscreen'));
	}

}
