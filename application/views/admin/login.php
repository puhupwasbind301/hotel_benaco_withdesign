<!DOCTYPE html>
<html lang="en">
<head>
  <?php
    $title = "Login"; ?>
    <meta charset="utf-8"/>
    <meta name="url" content="<?= base_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?php include 'admin_assets/include/header.php'; ?>
    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/ds.css"/>
    <title><?= $this->l->l('login') ?></title>
</head>
<body>


<!-- section login-->
<section class="ds-bg-gr login-100vh py-2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="row align-items-center">
                    <div class="col-3 col-md-2 col-lg-1 col-xl-1 in-pr-0">
                        <a href="#"><img class="in-hotel-logo" src="<?= base_url() ?>assets/img/icon/logo-hotel-benaco-b.png" alt="hotel"></a>
                    </div>
                    <div class="col-4 col-md-3 col-lg-2 in-pl-10 col-xl-2 in-pl-0">
                        <div class="">
                            <p class="mb-1 ds-in-t"><?= $this->l->l('backend_web') ?></p>
                            <p class="mb-1 ds-in-t"><?= $this->l->l('hotel_benaco') ?></p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <form  method="post">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-xl-6 py-2 py-md-5">

                <div class="row px-2 px-md-5">

                    <h1 class="login-title py-5 fw-lighter ds-z"><?= $this->l->l('login') ?></h1>
                    <?php if($this->session->flashdata('loginMsg')) : ?>
                    <?php echo $this->session->flashdata('loginMsg'); ?>
                    <?php endif ?>
                    <h4 class="login-box-msg"><?= $this->l->l('sign_in') ?></h4>
                    <div class="col-md-6 mb-4">
                        <input class="form-control small ds-form-con login-form-con" type="text" placeholder="<?= $this->l->l('user') ?>"  value="<?php echo set_value('username'); ?>" name="username" required
                               aria-label="default input example">
                    </div>

                    <div class="col-md-6 mb-4">
                        <input class="form-control small ds-form-con login-form-con" type="password" placeholder="<?= $this->l->l('password') ?>" value="<?php echo set_value('password'); ?>" name="password" required
                               aria-label="default input example">
                    </div>
                    <button type="submit"  class="log-btn pb-5"><?= $this->l->l('come_in') ?></button>
                 
                   

                </div>
            </div>
        </div>
        </form>
        <form  method="post" >
        <div class="row justify-content-center" style="margin-top:-100px;">
            <div class="col-lg-8 col-xl-6 py-2 py-md-5">
                    
                <div class="row px-2 px-md-5">
                    <div class="col-md-6">
                        <a href="#" class="pt-5 text-decoration-none small text-secondary"><?= $this->l->l('password') ?> <?= $this->l->l('forgot') ?></a>
<input type="email" name="email" class="emailname form-control small ds-form-con login-form-con  pt-2 ext-decoration-none d-block ds-small ds-border-bottom text-secondary" id="emailname"   placeholder="<?= $this->l->l('enter_your_password_reset_email') ?>"  aria-label="default input example">
                        <!-- <a href="javascript:void(0)" onclick="$('#forgetPassword').submit()" class="text-dark d-block pt-3 text-end reset-password"><?= $this->l->l('reset') ?></a> -->
                        <input type="submit" id="upload10" name="submit" class="btn btn-secondary my-1 " style="float:right;" value="<?= $this->l->l('reset') ?>" hidden/>
                        <label for="upload10" class="js-label" style="float:right;"><?= $this->l->l('come_in') ?></label>
                    
                    </div> 
                </div> 

            </div> 
        </div>
        </form>
        
        <div class="row justify-content-end">
            <div class="col-md-7 col-lg-6 col-xl-4 pt-5
                 login-pl-4">
                <p class="mb-0"><?= $this->l->l('hotel_benaco_backend_administration_v') ?> 01/2021</p>
                <a href="#" class="text-dark"><?= $this->l->l('support_magma_studio') ?></a>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- & section login-->

<?php include 'admin_assets/include/footer.php'; ?>
<script src="<?= base_url() ?>assets/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    // var reset = document.querySelector('.reset-password');
    // reset.addEventListener('click',()=>{
    //     // window.location.href = "<?= base_url() ?>admin/reset_password";
    //    var email = document.querySelector('.emailname');
    //     console.log(email);
    // });
</script>

<script type="text/javascript">
  
  
  $('#forgetPassword').submit(function(event) {
    event.preventDefault()
    
    var data = $(this).serializeArray();
    // console.log(data)
    // return false;
    $.ajax({
      url: '<?php echo base_url(); ?>admin/reset-password',
      type: 'POST',
      dataType: 'json',
      data: data,
    })
    .done(function(result) {
      console.log(result);
      return false;
      if (result.status === 'success') {
        $('#allocateMsg').html('<div class="alert alert-success  mt-3">'+result.msg+'</div>');
      }else{
        $('#allocateMsg').html('<div class="alert alert-danger  mt-3">'+result.msg+'</div>');
      }
      setTimeout(()=>{
        window.location.reload()
      },2500)
    })
    .fail(function(jqXHR,exception) {
      console.log(jqXHR.responseText);
    })
  });



</script>
</body>
</html>

