<!DOCTYPE html>
<html>
<?php $title = "Contact Detail View";
  $nav_page = 6;
  include 'admin_assets/include/header.php';
 ?>
<style type="text/css">
   @media (min-width: 992px) {
  .modaledit-lg, .modalview-lg{
    max-width: 800px;
  }
}
 </style>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <?php include 'admin_assets/include/navbar.php';?>

  <?php include 'admin_assets/include/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?= $this->l->l('contact_detail') ?> <?= $this->l->l('view') ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url()?>admin"><?= $this->l->l('home') ?></a></li>
              <li class="breadcrumb-item active"><a href="<?= base_url()?>admin/contact-detail"><?= $this->l->l('contact_detail') ?></a></li>
              <li class="breadcrumb-item active"><a href="<?= base_url('admin/contact-detail-view')?>"><?= $this->l->l('view') ?></a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <?php if($this->session->flashdata('msg')): ?>
              <?php echo $this->session->flashdata('msg'); ?>
            <?php endif; ?>
          <div class="card">
            <!-- <div class="card-header">
              
              <button type="button" class="btn btn-info float-right" data-toggle="modal" data-target="#modal-lg">Add Staff</button>
            </div> -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="tbl_id" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th><?= $this->l->l('image') ?></th>
                  <th><?= $this->l->l('visit') ?></th>
                  <th><?= $this->l->l('email') ?></th>
                  <th><?= $this->l->l('contact') ?></th>
                  <th><?= $this->l->l('created_date') ?></th>
                  <th><?= $this->l->l('manage') ?></th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach($data as $key => $val): ?>
                  <tr>
                    <td><?= ++$key?></td>
                    <td><center>
                       <?php if(!empty($val->cd_image)): ?>
                        <img src="<?= base_url().$val->cd_image ?>" width=150 height=150>
                      <?php else: ?>
                        <img src="https://via.placeholder.com/150" alt="works-image" style="height:150px;width:150">
                      <?php endif; ?>
                    </center></td>
                    <td class="text-justify"><?= $val->cd_visit ?></td>
                    <td ><?= $val->cd_write_email ?></td>
                    <td ><?= $val->cd_contact_no ?></td>
                    <td ><?= $val->cd_created_date ?></td>
                    
                    <td><a href="#" data-toggle="modal" data-target="#modalview-lg<?= $val->cd_id?>"><i class="fa fa-eye text-info" title="<?= $this->l->l('view') ?>"></i></a>&emsp;|&emsp;<a href="#" data-toggle="modal" data-target="#modaledit-lg<?= $val->cd_id?>"><i class="fa fa-edit text-success" title="<?= $this->l->l('edit') ?>"></i></a>&emsp;|&emsp;<a href="#" data-toggle="modal" data-target="#modal-sm<?= $val->cd_id?>"><i class="fa fa-trash text-danger" title="<?= $this->l->l('delete') ?>"></i></a></td>

                  </tr>

                  <!-- Edit Staff -->
          <div class="modal fade" id="modaledit-lg<?= $val->cd_id?>">
        <div class="modal-dialog modaledit-lg">
          <div class="modal-content">
            <div class="modal-header"><?= $this->l->l('edit') ?> <?= $this->l->l('contact_detail') ?></h4>
              <div id="msg"></div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              <form action="" class="contact_detail_edit" method="post">
                <div class="card-body">
                  <input type="hidden" name="cdid" value="<?= $val->cd_id ?>">
                  
                      <div class="row">
                          <div class="col-md-12">
                            <div class="col-md-12">
                              <div class="form-group">
                                <label for="name"><?= $this->l->l('image') ?></label>
                                <p>
                                   
                                    <?php if(!empty($val->cd_image)): ?>
                                     <img src="<?= base_url().$val->cd_image ?>" width=150 height=150>
                                   <?php else: ?>
                                     <img src="https://via.placeholder.com/150" alt="works-image" style="height:150px;width:150">
                                   <?php endif; ?>
                                </p>
                                <div class="custom-file mb-3">
                                  
                                    <input type="file" class="custom-file-input" id="customFile" name="filename" value="<?= $val->cd_image ?>">
                                    <?php $v =explode('/',$val->cd_image); ?>
                            <label class="custom-file-label" for="customFile" style="overflow:hidden;"><?= end($v); ?></label>
                                </div>
                              </div>
                              <!-- <div id="imagePreview">
                                <img src="<?= base_url() ?>photo-1526947425960-945c6e72858f.jpeg">
                              </div> -->

                            </div>
                      </div>    

                  </div>
                 
                  <div class="row">
                  <div class="col-md-12">
                      <div class="form-group">
                        <label for="sdesc"><?= $this->l->l('visit') ?></label>
                        <textarea class="form-control" name="visit"  id="desc" rows="10"><?= $val->cd_visit ?></textarea>
                      </div>
                   </div>    
                   </div>  
                   <div class="row">
                     <div class="col-md-12">
                         <div class="form-group">
                       <label for="sname"><?= $this->l->l('email') ?></label>
                       <input type="email" class="form-control" name="write_email" placeholder="<?= $this->l->l('email') ?>" required="" value="<?= $val->cd_write_email ?>">
                         </div>
                     </div>
                   </div>  
                   <div class="row">
                     <div class="col-md-12">
                         <div class="form-group">
                       <label for="sname"><?= $this->l->l('contact') ?></label>
                       <input type="text" class="form-control" name="contact_no" placeholder="<?= $this->l->l('contact') ?>" required="" value="<?= $val->cd_contact_no ?>">
                         </div>
                     </div>
                   </div>  
                  
                </div>
                <div id="messageForm1"></div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary float-right"><?= $this->l->l('update') ?></button>
                </div>
              </form>
            </div>
            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

      <!-- View Staff -->
       <div class="modal fade" id="modalview-lg<?= $val->cd_id?>">
        <div class="modal-dialog modalview-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><?= $this->l->l('contact_detail') ?></h4>
              <div id="msg"></div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <!-- form start -->
              
               <div class="row p-2">
                <div class="col-md-3">
                  <b><?= $this->l->l('contact_detail') ?> <?= $this->l->l('image') ?></b>
                </div>
                <div class="col-md-9">
                  <p>
                     <img src="<?= base_url().$val->cd_image ?>" width=150 height=150>
                  </p>
                </div>
              </div>

              <div class="row p-2">
                <div class="col-md-3">
                  <b><?= $this->l->l('visit') ?></b>
                </div>
                <div class="col-md-9">
                  <?= $val->cd_visit  ?>
                </div>
              </div>
             
              <div class="row p-2">
                <div class="col-md-3">
                  <b><?= $this->l->l('email') ?></b>
                </div>
                <div class="col-md-9">
                  <?= $val->cd_write_email  ?>
                </div>
              </div>
              <div class="row p-2">
                <div class="col-md-3">
                  <b><?= $this->l->l('contact') ?> No.</b>
                </div>
                <div class="col-md-9">
                  <?= $val->cd_contact_no  ?>
                </div>
              </div>
              <div class="row p-2">
                <div class="col-md-3">
                  <b><?= $this->l->l('created_date') ?></b>
                </div>
                <div class="col-md-9">
                  <?= $val->cd_created_date  ?>
                </div>
              </div>
            </div>
            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      
      <!-- Delete Staff -->
      <div class="modal fade" id="modal-sm<?= $val->cd_id?>">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><?= $this->l->l('delete') ?> <?= $this->l->l('contact_detail') ?></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo base_url();?>admin/delete-contact-detail" method="post">
            <div class="modal-body">
              <input type="hidden" name="cdid" value="<?= $val->cd_id?>">
              <p><?= $this->l->l('are_you_sure_you_want_to_delete') ?></p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->l->l('close') ?></button>
              <button type="submit" class="btn btn-primary"><?= $this->l->l('yes') ?></button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
                <?php endforeach; ?>
                </tbody>
                
               
              </table>
              </div>
            </div>
          </div>
          </div>
     
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

    
  </div>
  <!-- /.content-wrapper -->

  <?php include 'admin_assets/include/footer.php'; ?>
  <script>
$(function() {
  $('input[name="doj"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });
});
</script>


<script type="text/javascript">
  $(function() {

    $('.contact_detail_edit').on('submit' , function (e) {
    e.preventDefault();
    for(instance in CKEDITOR.instances) {
          CKEDITOR.instances[instance].updateElement();
     }
    let url = $('meta[name=url]').attr("content");
    let data = new FormData($(this).get(0));

    console.log(data);
    

    ajax(url+"admin/update-contact-detail", data).then(function(result) {
      if(result.result){
        
        window.location.reload()
      }
      else{
        
        window.location.reload()
      
      }
      

    }).catch(function(e) {

      
      console.log(e)

    })

   })
  })

</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#tbl_id').dataTable({
        "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
        "iDisplayLength": 75
    });
} );
</script>
<script>

// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  // alert(fileName);
  // return false;
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  // var src = $(this).val();
  // $("#imagePreview").html(filename ? "<img src='" + filename + "'>" : "");
});

</script>
</body>
</html>
