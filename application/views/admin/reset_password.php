<!DOCTYPE html>
<html lang="en">
<head>
  <?php
    $title = "Reset Password"; ?>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/ds.css"/>
    <title><?= $this->l->l('reset_password') ?></title>
</head>
<body>


<!-- section login-->
<section class="ds-bg-gr login-100vh py-2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="row align-items-center">
                    <div class="col-3 col-md-2 col-lg-1 col-xl-1 in-pr-0">
                        <a href="#"><img class="in-hotel-logo" src="<?= base_url() ?>assets/img/icon/logo-hotel-benaco-b.png" alt="hotel"></a>
                    </div>
                    <div class="col-4 col-md-3 col-lg-2 in-pl-10 col-xl-2 in-pl-0">
                        <div class="">
                            <p class="mb-1 ds-in-t"><?= $this->l->l('backend_web') ?></p>
                            <p class="mb-1 ds-in-t"><?= $this->l->l('hotel_benaco') ?></p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
       <h5></h5>
        
        <div class="row justify-content-center">
            <div class="col-lg-8 col-xl-6 py-2 py-md-5">
                <form action="<?= base_url() ?>admin/reset-password" method="post">
                <div class="row px-2 px-md-5">
                    <h1 class="login-title py-5 fw-lighter ds-z"><?= $this->l->l('reset_password') ?></h1>
                    <?php if($this->session->flashdata('loginMsg')) : ?>
                    <?php echo $this->session->flashdata('loginMsg'); ?>
                    <?php endif ?>
                    <!-- <h4 class="login-box-msg">Change Password</h4>
                    <div class="col-md-6 mb-4">
                        <input class="form-control small ds-form-con login-form-con" type="password" placeholder="Old Password" value="<?php echo set_value('password'); ?>" name="opassword" required
                               aria-label="default input example">
                       <small class="text-danger"><?php echo form_error('opassword'); ?></small>
                    </div>  -->
                    <div class="col-md-6 mb-4">
                        <input class="form-control small ds-form-con login-form-con" type="password" placeholder="<?= $this->l->l('new_password') ?>" value="<?php echo set_value('password'); ?>" name="npassword" required
                               aria-label="default input example">
                       <small class="text-danger"><?php echo form_error('npassword'); ?></small>
                    </div> 
                    <div class="col-md-6 mb-4">
                        <input class="form-control small ds-form-con login-form-con" type="password" placeholder="<?= $this->l->l('confirm_password') ?>" value="<?php echo set_value('password'); ?>" name="cpassword" required
                               aria-label="default input example">
                         <small class="text-danger"><?php echo form_error('cpassword'); ?></small>      
                    </div>
                    <button type="submit" name="submit" class="log-btn pb-5"><?= $this->l->l('update') ?></button>
                </div>
                </form> 
            </div>
        </div>
        
        <div class="row justify-content-end">
            <div class="col-md-7 col-lg-6 col-xl-4 pt-5
                 login-pl-4">
                <p class="mb-0"><?= $this->l->l('hotel_benaco_backend_administration_v') ?> 01/2021</p>
                <a href="#" class="text-dark">Support Magma Studio</a>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- & section login-->


<script src="<?= base_url() ?>assets/js/bootstrap.bundle.min.js"></script>
</body>
</html>

