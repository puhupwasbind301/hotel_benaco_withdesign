<!DOCTYPE html>
<html>
<?php $title = "Contact Detail";
  $nav_page = 6;
  include 'admin_assets/include/header.php';
 ?>
 <style type="text/css">
   @media (min-width: 992px) {
  .modaledit-lg, .modalview-lg{
    max-width: 800px;
  }
}
 </style>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <?php include 'admin_assets/include/navbar.php';?>

  <?php include 'admin_assets/include/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-4">
            <h1><?= $this->l->l('contact_detail') ?></h1>
          </div>
          <div class="col-sm-8">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url()?>admin"><?= $this->l->l('home') ?></a></li>
              <li class="breadcrumb-item active"><a href="<?= base_url()?>admin/contact-detail"><?= $this->l->l('contact_detail') ?></a></li>
              <li class="breadcrumb-item active"><a href="<?= base_url('admin/contact-detail-view')?>"><?= $this->l->l('view') ?></a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <?php if($this->session->flashdata('msg')): ?>
              <?php echo $this->session->flashdata('msg'); ?>
            <?php endif; ?>
          <div class="card">
            <div class="card-header">
            </div>
            <div class="card-body">
                  <!-- form start -->
                  <form  id="contact_detail" method="post">
                    <div class="card-body">
                      <div id="messageForm"></div>
                    
                       <div class="row">
                        <div class="col-md-12"> 
                          <div class="form-group">
                            <label for="name"><?= $this->l->l('contact_detail') ?> <?= $this->l->l('image') ?></label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input" id="customFile" name="filename">
                                <label class="custom-file-label" for="customFile" style="overflow:hidden;"><?= $this->l->l('choose_image') ?></label>
                            </div>
                          </div>
                          <!-- <div id="imagePreview">
                            <img src="<?= base_url() ?>photo-1526947425960-945c6e72858f.jpeg">
                          </div> -->

                        </div>
                   
                        
                          <div class="col-md-12">
                             <div class="form-group">
                              <label for="address"><?= $this->l->l('visit') ?></label>
                              <textarea class="form-control" name="visit" placeholder="<?= $this->l->l('visit') ?>" required=""  id="desc"></textarea>
                            </div>
                          </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="name"><?= $this->l->l('write_email') ?></label>
                            <input type="text" class="form-control" id="write_email" name="write_email" placeholder="<?= $this->l->l('write_email') ?>" required="" >
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="name"><?= $this->l->l('contact') ?></label>
                            <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="<?= $this->l->l('contact') ?>" required="" >
                          
                          </div>
                        </div>

                      
                      </div>




                    
                    <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary float-right"><?= $this->l->l('submit') ?></button>
                    </div>
                  </form>
            </div>
          </div>
          </div>
       
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
         <!-- Add Staff -->
    <div class="modal fade" id="modal-lg">
       <div class="modal-dialog modal-lg">
         <div class="modal-content">
           <div class="modal-header">
             <h4 class="modal-title">Add Banner <?= $this->l->l('image') ?></h4>
             <div id="msg"></div>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
           </div>
           <div class="modal-body">
             <!-- form start -->
             <form  class="add_banner_image" method="post">
               <div class="card-body">
                 <div id="messageForm"></div>
                <div class="row">
               

                 <div class="col-md-12">
                   <div class="form-group">
                     <label for="name"><?= $this->l->l('image') ?></label>
                     <div class="custom-file mb-3">
                         <input type="hidden" class="custom-file-input" id="image_page" name="image_page" value="contact-detail">
                         <input type="file" class="custom-file-input" id="customFile" name="filename">
                         <label class="custom-file-label" for="customFile" style="overflow:hidden;"><?= $this->l->l('choose_image') ?></label>
                     </div>
                   </div>
                   <!-- <div id="imagePreview">
                     <img src="<?= base_url() ?>photo-1526947425960-945c6e72858f.jpeg">
                   </div> -->

                 </div>


               
               </div>
             
               
               <!-- /.card-body -->

               <div class="card-footer">
                 <button type="submit" class="btn btn-primary float-right"><?= $this->l->l('submit') ?></button>
               </div>
             </form>
           </div>
           
         </div>
         <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
     </div> 
    
  </div>
  <!-- /.content-wrapper -->

  <?php include 'admin_assets/include/footer.php'; ?>
  <script>
$(function() {
  $('input[name="doj"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'),10)
  });
});
</script>
<script type="text/javascript">
  $(function() {
    //  $('.add_banner_image').on('submit' , function (e) {
       
    //  e.preventDefault();

    //  let url = $('meta[name=url]').attr("content");
    //  let data = new FormData($(this).get(0))
    //  ajax(url+"admin/add-banner-image", data).then(function(result) {
    //    console.log(result);
    //    // return false; 
    //    if(result.result){
    //      window.location.reload()
    //    }
    //    else{
    //      $('#messageForm').html('<div class="alert alert-danger">'+result.msg+'</div>');
    //      // $('#mobileror').html(result);
    //    }
    //    // // window.location.reload()
    //  }).catch(function(e){
    //    console.log(e)
    //  })

    // })
    
    $('#contact_detail').on('submit' , function (e) {
    e.preventDefault();
    for(instance in CKEDITOR.instances) {
          CKEDITOR.instances[instance].updateElement();
      }
    let url = $('meta[name=url]').attr("content");
    let data = new FormData($(this).get(0))
    ajax(url+"admin/insert-contact-detail", data).then(function(result) {
      // console.log(result);
      // return false;
      
      if(result.result){
        window.location.reload()
      }
      else{
        $('#messageForm').html('<div class="alert alert-danger">'+result.msg+'</div>');
        // $('#mobileror').html(result);
      }
      // window.location.reload()
    }).catch(function(e){
      console.log(e)
    })

   })
  })

</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tbl_id').dataTable({
        "aLengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
        "iDisplayLength": 75
    });
} );
</script>
<script>

// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  // alert(fileName);
  // return false;
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  // var src = $(this).val();
  // $("#imagePreview").html(filename ? "<img src='" + filename + "'>" : "");
});



</script>

</body>
</html>
