<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="url" content="<?= base_url() ?>"/>
    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/ds.css"/>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script src="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.min.js"></script>

    <!-- P Starts -->
    <!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet"> 
    <link href="<?= base_url() ?>admin_assets/plugins/fontawesome_iconpicker/dist/css/fontawesome-iconpicker.min.css" rel="stylesheet"> 

    <!-- icon picker 3/4 bootstrap -->
    <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>admin_assets/plugins/bootstrap-iconpicker-1.10.0/dist/css/bootstrap-iconpicker.min.css"/> -->
    <!-- icon picker 3/4 bootstrap ends -->
    <!-- P Ends -->
    <title>Backend</title>
    <style>
        .cs-border-trn {
            border-bottom: 2px solid transparent;
        }

        .cs-border {
            border-bottom: 2px solid #404040;
        }

        @media (min-width: 1200px) {
            .ds {
                position: fixed;
            }
        }

        a{
            cursor: pointer;
        }
    </style>
</head>
<body>


<!-- section login-->
<section class="ds-bg-gr py-4">
    <div class="row mx-0">
        <div class="col-lg-3 col-md-4 order-1" data-spy="affix" data-offset-top="160">
            <div class="ds">
                <div class="row mx-0 align-items-center">
                    <div class="col-3 col-md-3 in-pr-0">
                        <a href="#"><img class="in-hotel-logo" src="<?= base_url() ?>assets/img/icon/logo-hotel-benaco-b.png" alt="hotel"></a>
                    </div>
                    <div class="col-6 col-md-9 in-pl-0">
                        <div class="">
                            <p class="mb-0 ds-in-t">Backend Web</p>
                            <p class="mb-0 ds-in-t">Hotel Benaco</p>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <select class="cs-op small border-0 bg-transparent ds-ml-auto js-piccc hotel-bacano-lang"
                                aria-label="Default select example ">
                            <option value="" disabled="disabled" selected><?= $this->l->l('language') ?></option>
                            <option value="ITALIAN" <?php if(HOTEL_BANACO_LANGUAGE == 'ITALIAN'){ECHO 'SELECTED';} ?> ><?= $this->l->l('italian') ?></option>
                            <option value="ENGLISH" <?php if(HOTEL_BANACO_LANGUAGE == 'ENGLISH'){ECHO 'SELECTED';} ?> ><?= $this->l->l('english') ?></option>
                        </select>
                    </div>
                    <br>
                    <div class="col-md-12 mt-3">
                        <button type="button" class="small js-vi  text-dark" data-toggle="modal" data-target="#modal-lg">Add Room Categories</button>
                    </div>

                    <div class="col-md-12 mt-2">
                        <button type="button" class="small js-vi  text-dark" data-toggle="modal" data-target="#modal-lg-update">Update Room Categories</button>
                    </div>

                    <div class="col-md-12 mt-2">
                         <a href="<?= base_url() ?>admin/logout"><button class="small js-vi  text-dark"><?= $this->l->l('logout') ?></button></a>
                    </div>

                   <!--  <div class="col-md-12 mt-2">
                    <button class="btn btn-secondary" role="iconpicker"></button>
                                         <div role="iconpicker"></div>
                    </div> -->


                </div>
                <div class="row">
                    <div class="col-md-12 px-0">
                        <div class="bc-grid">
                            <div class="">
                                <a href="#" class="bc-border-bottom-1 pt-4"></a>
                                <a href="#" class="bc-border-bottom-2 pt-4"></a>
                            </div>
                            <div class="">

                                <!-- parent tab-->
                                <div class="d-flex align-items-start">
                                    <div class="nav bc-nav flex-column nav-pills js-pills me-3" id="u-pills-tab" role="tablist"
                                         aria-orientation="vertical">
                                        <button class="nav-link has-treeview text-dark active my-4" id="u-pills-home-tab"
                                                data-bs-toggle="pill"
                                                data-bs-target="#u-pills-home" type="button" role="tab"
                                                aria-controls="u-pills-home" aria-selected="true">Homepage
                                        </button>
                                        <button class="nav-link has-treeview text-dark" id="
                                        u-pills-profile-tab"
                                                data-bs-toggle="pill"
                                                data-bs-target="#u-pills-profile" type="button" role="tab"
                                                aria-controls="u-pills-profile" aria-selected="false"><?= $this->l->l('rooms') ?>
                                        </button>
                                    </div>
                                </div>
                                <!-- & parent tab-->

                                
                                <div class="d-flex align-items-start">
                                    <div class="nav bc-nav bc-pl-2 flex-column nav-pills me-3" id="v-pills-tab"
                                         role="tablist"
                                         aria-orientation="vertical">
                                    <?php foreach ($room_categories as $key => $value): ?>
                                        <button class="nav-link small js-vi <?php //if($key==0){echo 'active';} ?>" data-key="<?= ++$key ?>"  id="v-pills-home-tab"
                                            data-bs-toggle="pill" data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true">
                                    <?= (HOTEL_BANACO_LANGUAGE == 'ITALIAN')?$value->rc_italian_name:$value->rc_name ?>
                                        </button>
                                        <?php endforeach ?>

                                        <!-- <button class="nav-link text-dark small" id="v-pills-profile-tab"
                                                data-bs-toggle="pill"
                                                data-bs-target="#v-pills-profile" type="button" role="tab"
                                                aria-controls="v-pills-profile" aria-selected="false"><?= $this->l->l('comfort_with_balcony') ?>
                                        </button>
                                        <button class="nav-link text-dark small" id="v-pills-messages-tab"
                                                data-bs-toggle="pill"
                                                data-bs-target="#v-pills-messages" type="button" role="tab"
                                                aria-controls="v-pills-messages" aria-selected="false"><?= $this->l->l('family_with_balcony') ?>
                                        </button>
                                        <button class="nav-link text-dark small" id="v-pills-settings-tab"
                                                data-bs-toggle="pill"
                                                data-bs-target="#v-pills-settings" type="button" role="tab"
                                                aria-controls="v-pills-settings" aria-selected="false">Single Economy
                                        </button> -->
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
       

        <div class="col-lg-7 col-md-8 order-3 order-md-2">
            <?php 
            if($this->session->flashdata('success_msg')){ ?>
                <!--  <div class="alert alert-warning" id="ss">
                   <strong>Warning!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                 </div> -->
                 <?php 
             } else { ?>
                <!-- <div class="alert alert-danger" id="dg">
                   <strong>Danger!</strong> <?php echo $this->session->flashdata('warn_msg'); ?>
                 </div> -->
                 <?php 
                }
             ?>
             <div id="allocateMsg"></div>
             <div class="d-lg-none d-md-block d-none order-2 order-md-3 align-items-center">
            <div class="dss">
                <div class="text-end">
                    <span class="small bc-in-sm"><?= $this->l->l('good_morning_gregory') ?></span>
                    <a href="#" class="mx-3"><img src="<?= base_url() ?>assets/img/icon/user.png" alt="user" width="40px"></a>
                </div>
            </div>
        </div>
            <div class="tab-content" id="u-pills-tabContent">
                <div class="tab-pane fade show active" id="u-pills-home" role="tabpanel"
                     aria-labelledby="u-pills-home-tab">

                     <form class="home-section" method="post" enctype="multipart/form-data">
                        <?php $path = explode('/',$homepagevideo->hiv_path); ?>
                    <div class="row ds-mt-9 pt-4">
                        <div class="col-xl-7 col-md-9">
                            <h3 class="pb-3 js-vi"><?= $this->l->l('video_header') ?></h3>
                            <input class="form-control bc-font mb-4 pb-1 ds-form-con px-0" type="text" name="video_header_link" value="<?= $home_page->hp_video_header_link ?>"
                                   placeholder="<?= end($path) ?>"
                                   aria-label="default input example">
                            <input type="hidden" name="hpid" value="<?= $home_page->hp_id ?>">       
                            <input class="form-control bc-font ds-form-con small px-0" type="text" name="hotel_benaco_heading"
                              value="<?= $home_page->hp_hotel_benaco_heading ?>"
                                   placeholder="Lorem ipsum dolor sit amet, consectetur adipisicing elit."
                                   aria-label="default input example">
                         <div class="d-block  pt-5">
                         <!-- <div class="d-block d-md-none pt-5"> -->
                           <input type="hidden" name="hotel_benaco_video_section" value="4">
                           <input type="file" name="upload_home_video" id="upload1" class="inputfile" hidden/>
                           <label for="upload1" class="js-label"><?= $this->l->l('upload') ?> Video</label>
                              <p id="file-filename"></p>
                         </div> 
                         <!--   <div class="col-xl-5 col-md-3 text-center pt-4 d-none d-lg-block">
                              <div class="pt-5">
                           <input type="hidden" name="hotel_benaco_video_section" value="4">
                           <input type="file" name="upload_home_video" id="upload8" class="inputfile" hidden/>
                           <label for="upload8" class="js-label"><?= $this->l->l('upload') ?> Video</label>
                              <p id="file-filename"></p>  
                              </div>
                           </div> -->

                            <h3 class="mt-5 pt-md-5 pb-3 js-vi"><?= $this->l->l('intro_text') ?></h3>
                            <input class="form-control bc-font mb-3 pb-1 ds-form-con px-0" type="text" name="intro_header_title"
                              value="<?= $home_page->hp_intro_header_title ?>"
                                   placeholder="tutte le forme dell accoglienza"
                                   aria-label="default  input example">
                            <input class="form-control bc-font mb-4 ds-form-con px-0" type="text" name="intro_header"
                               value="<?= $home_page->hp_intro_header ?>"
                                   placeholder="Benvenuti al Benaco"
                                   aria-label="default  input example">
                            <input class="form-control bc-font ds-form-con px-0" type="text"
                                name="intro_sub_header" value="<?= $home_page->hp_intro_sub_header ?>"
                                   placeholder="NEL MAGNIFICO CONTESTO DELLAGO D1 GARDA"
                                   aria-label="default input example">
                           <div class="pt-4">
                                <textarea name="intro_description" class="form-control summernote" cols="30" rows="10"><?= $home_page->hp_intro_description ?></textarea>
                           </div>
                        </div>
                      <!--   <div class="col-xl-5 col-md-3 text-center pt-4 d-none d-lg-block">
                           <div class="pt-5">
                        <input type="hidden" name="hotel_benaco_video_section" value="4">
                        <input type="file" name="upload_home_video" id="upload8" class="inputfile" hidden/>
                        <label for="upload8" class="js-label"><?= $this->l->l('upload') ?> Video</label>
                           <p id="file-filename"></p>  
                           </div>
                        </div> -->
                       <!--  <div class="pt-4 text-end">
                                 <input type="hidden"  name="home_intro_images_section" value="5">
                                 <input type="file" name="upload_home_intro_photo[]" multiple="multiple" id="upload" hidden>
                                     
                                 <label for="upload" class="js-label text-muted"><?= $this->l->l('add_photos') ?></label>
                        </div> -->
                    </div>


                    <!-- gallery-->
                    <h2 class="bc-mt-5 pb-4 js-vi">Gallery</h2>

                                        <div class="bc-gallery">
                                            <?php foreach ($home_intro_images_section as $key => $value): ?>
                                             <div class="bc-relative">
                                                <img class="img-fluid " src="<?= base_url().$value->hiv_path ?>" alt="<?= $this->l->l('rooms') ?>" style="height:100px !important;width:200px !important">
                                                <img class="img-fluid bc-abs remove-image" data-name="home" id="<?php echo $value->hiv_id; ?>" src="<?= base_url() ?>assets/img/icon/remove.png" alt="remove" title="Delete Image" style="cursor: pointer;">
                                            </div>
                                            <?php endforeach ?>
                                           
                                        </div>
                                        <div class="pt-4 text-end">
                                                <input type="hidden"  name="home_intro_images_section" value="5">
                                                <input type="file" name="upload_home_intro_photo[]" multiple="multiple" id="upload" hidden>
                                                    <!-- <input type="file" id="upload" hidden/> -->
                                                <label for="upload" class="js-label text-muted"><?= $this->l->l('add_photos') ?></label>
                                       </div>
                                        <!-- & gallery-->

                                        <div class="row">
                                            <div class="col-md-7">
                                                <h2 class="bc-mt-5 pb-4 js-vi"><?= $this->l->l('experience') ?></h2>

                                                <textarea name="home_experience" class="form-control summernote" cols="30" rows="10"><?= $home_page->hp_home_experience ?></textarea>

                                            </div>
                                        </div>


                                        <!-- againgallery -->

                                        
                                         <h2 class="bc-mt-5 pb-4 js-vi"><?= $this->l->l('experience').' '.$this->l->l('image') ?></h2>
                                            
                                          <div class="bc-gallery pt-5">
                                            <?php foreach ($experience_images_section as $key => $value): ?>
                                            <div class="bc-relative">
                                                <img class="img-fluid" src="<?= base_url().$value->hiv_path ?>" alt="<?= $this->l->l('rooms') ?>">
                                                <a href="#"><img class="img-fluid bc-abs remove-image" data-name="home" id="<?php echo $value->hiv_id; ?>" src="<?= base_url() ?>assets/img/icon/remove.png" alt="remove"></a>
                                            </div>
                                            <?php endforeach ?>
                                         </div>
                                        <div class="pt-4 text-end">
                                            <input type="hidden"  name="experience_images_section" value="9">
                                            <input type="file" id="upload9" name="experience_images[]"  multiple="multiple" hidden/>
                                            <label for="upload9" class="js-label text-muted"><?= $this->l->l('add_photos') ?></label>
                                        </div>
                                        <!-- & gallery-->

                                     
                                        <!-- end again gallery -->

                                        <!-- Piscina & Giardino-->
                                        <h2 class="bc-mt-5 pb-4 js-vi"><?= $this->l->l('pool_and_garden') ?></h2>

                                        <div class="bc-gallery">
                                            <?php foreach ($p_g_slider_images_section as $key => $value): ?>
                                            <div class="bc-relative">
                                                <img class="img-fluid" src="<?= base_url().$value->hiv_path ?>" alt="<?= $this->l->l('rooms') ?>" style="height:100px !important;width:200px !important">
                                                <img class="img-fluid bc-abs remove-image" data-name="home" id="<?= $value->hiv_id ?>" src="<?= base_url() ?>assets/img/icon/remove.png" alt="remove" >
                                            </div>
                                            <div class="bc-relative">
                                                <p><?= ($key%2==0)?++$key.'-Piscina':++$key.'-Giardino' ?></p>
                                            </div>
                                            <?php endforeach ?>
                                        </div>
                                       <div class="pt-4 text-end">
                                                <input type="hidden"  name="p_g_slider_images_section" value="6">
                                                <input type="file" name="p_g_slider_images[]" multiple="multiple" id="upload3" hidden>
                                                    <!-- <input type="file" id="upload" hidden/> -->
                                                <label for="upload3" class="js-label text-muted"><?= $this->l->l('add_photos') ?></label>
                                               </div>
                                        <!-- & Piscina & Giardino-->

                                        <div class="row bc-mt-5 py-5">
                                            <div class="col-md-7">
                                                <h3 class="js-vi">Video emotional</h3>
                                                <input class="form-control bc-font mb-3 pb-1 ds-form-con px-0" type="text"
                                                       placeholder="https://www.hotelbenaco.com/video/videohome.mp4"
                                                       aria-label="default input example">
                                            </div>
                                            <div class="col-md-5 text-center pt-5">
                                               <div class=" pt-xl-3 pt-md-2 text-end">
                                                <input type="hidden"  name="pool_and_garden_video_section" value="7" >
                                                <input type="file" name="pool_and_garden_video" id="upload4" hidden>
                                                    <!-- <input type="file" id="upload" hidden/> -->
                                                <label for="upload4" class="js-label text-muted"><?= $this->l->l('upload') ?> Video</label>
                                               </div>
                                            </div>
                                        </div>

                                        <!-- footer gallery-->
                                        <h2 class="bc-mt-5 pb-4 js-vi">Gallery footer</h2>

                                        <div class="bc-gallery">
                                            <?php foreach ($p_g_footer_slider_images_section as $key => $value): ?>
                                            <div class="bc-relative">
                                                <img class="img-fluid " src="<?= base_url().$value->hiv_path ?>" alt="<?= $this->l->l('rooms') ?>" style="height:100px !important;width:200px !important">
                                                <img class="img-fluid bc-abs remove-image" data-name="home" id="<?= $value->hiv_id ?>" src="<?= base_url() ?>assets/img/icon/remove.png" alt="remove">
                                            </div>
                                            <?php endforeach ?>
                                        </div>
                                       
                                         <div class="pt-4 text-end">
                                                <input type="hidden"  name="p_g_footer_slider_images_section" value="8">
                                                <input type="file" name="p_g_footer_slider_images[]" multiple="multiple" id="upload5" hidden>
                                                    <!-- <input type="file" id="upload" hidden/> -->
                                                <label for="upload5" class="js-label text-muted"><?= $this->l->l('add_photos') ?></label>
                                        </div>

                                        <!-- & footer gallery-->

                    

                            <div class="pt-4 text-center h3 bc-mt-5 fw-lighter">
                                <input type="submit" name="submit" id="upload" value="<?= $this->l->l('save_all_changes') ?>"  class="js-labelll text-dark" style="border:none;background: none;" />
                           </div>
                           </form>


                 

                </div>
                <div id="allocateMsgRoom"></div>
                <div class="tab-pane fade" id="u-pills-profile" role="tabpanel"
                     aria-labelledby="u-pills-profile-tab">
                 <form class="room-section" method="post" enctype="multipart/form-data">
                    <div id="room_category_id"></div>
                    <div class="row ds-mt-9 pb-4">
                        <div class="col-md-7 pt-4">
                            <h3 class="js-vi"><?= $this->l->l('title_rooms') ?></h3>
                        <input type="hidden" name="rpid" id="rp_id" value="">
                            <input class="form-control bc-font mb-3 pb-1 ds-form-con px-0" type="text" id="room_title" name="room_title" type="text" value="<?= $room_page->rp_room_title ?>"  placeholder="Doppia con o senza balcano" aria-label="default bc-font input example" >
                        </div>
                    </div>
                    <h3 class="py-4 js-vi"><?= $this->l->l('header_image') ?></h3>
                    <div class="row">
                        <div class="col-md-10">
                            <input type="hidden" name="header_image_section" value="2">
                                <?php if(empty($room_header->riv_path)): ?>
                                    <img class="img-fluid" src="<?= base_url() ?>assets/img/hotel-benaco/camere.jpg" alt="<?= $this->l->l('rooms') ?>">
                                <?php else: ?>
                                    <img class="img-fluid" id="header_image" src="<?= base_url().$room_header->riv_path ?>" alt="<?= $this->l->l('rooms') ?>">
                                <?php endif; ?>
                        </div>


                        <div class="col-md-2 bc-fe">
                            <div class="">
                               <input type="file" name="upload_room_photo"  id="upload6" hidden> 
                                <!-- <input type="file" id="upload" hidden/> -->
                            <label for="upload6" class="js-label text-muted"><?= $this->l->l('change_photo') ?></label>
                           </div>
                        </div>
                    </div>

                    <div class="row py-5">
                        <div class="col-md-7">
                            <h3 class="mt-5 js-vi"><?= $this->l->l('intro_text') ?></h3>
                            <input class="form-control bc-font mb-3 pb-1 ds-form-con px-0 pt-4" type="text" value="<?= $room_page->rp_intro_text ?>"
                                   placeholder="Vi Presentiamo"
                                   aria-label="default bc-font input example" id="intro_text" name="intro_text">
                            <input class="form-control bc-font mb-3 ds-form-con px-0" type="text"
                            value="<?= $room_page->rp_double_room ?>"
                                   placeholder="Camera Doppia"
                                   aria-label="default bc-font input example" id="double_room" name="double_room">
                                   
                            <input class="form-control bc-font ds-form-con px-0" type="text"
                            name="with_or_without_balcony" id="with_or_without_balcony"
                            value="<?= $room_page->rp_with_or_without_balcony ?>"
                                   placeholder="Con o senza balcone"
                                   aria-label="default input example">
                            <div class="pt-4">
                                <textarea   name="double_room_desc"  class="form-control summernote" cols="30" rows="10"><span id="double_room_desc"></span></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- gallery-->
                    <h2 class="bc-mt-5 pb-4 js-vi">Gallery</h2>

                    <div class="bc-gallery double-room">
                        <?php //foreach ($double_room_slider as $key => $value): ?>
                       
                        <?php //endforeach ?>
                        
                    </div>

                   <div class="pt-5 text-end">
                        <input type="hidden" name="double_room_slider_section" multiple="multiple" value="3">
                        <input  type="file"  name="double_room_slider[]" multiple="multiple" value="<?= $this->l->l('add_photos') ?>" id="upload7" hidden />
                         <!-- <input type="file" id="upload" hidden/> -->
                        <label for="upload7" class="js-label text-muted"><?= $this->l->l('add_photos') ?></label>
                   </div>
                   
                    <!-- & gallery-->
                    
                     <h2 class="bc-mt-5 pb-4 js-vi"><?= $this->l->l('living_hotel_benaco').' '.$this->l->l('rooms').' '.$this->l->l('image') ?></h2>

                    <div class="bc-gallery pt-5">
                        <div class="bc-relative" id="living_hotel_banaco_images">
                          
                        </div>
                    </div>

                    <div class="pt-4 text-end">
                        <input type="hidden"  name="living_hotel_banaco_images_section" value="10">
                            <input type="file" id="upload10" name="living_hotel_banaco_images" hidden/>
                        <label for="upload10" class="js-label text-muted"><?= $this->l->l('add_photos') ?></label>
                    </div>

                    <h2 class="bc-mt-5 pb-4 js-vi"><?= $this->l->l('features') ?></h2>
                    <div class="row room_features" id="displayMoreFeatureSection">
                       <!--  <?php //foreach ($room_features as $key => $value): ?>
                          <div class="col-md-6">
                            <div class="row">
                                <div class="col-12">
                                       <input class="form-control icp icp-auto" data-input-search="true" name="iconnameFeature[]" value="fas fa-wifi" type="text" placeholder="Search Icon" />  
                                       <input type="hidden" class="form-control" name="rf_single_id[]" value="<?= $value->fasd_id ?>" required=""> 
                                       <input type="hidden" class="form-control" name="feature" value="FEATURE" placeholder="Room feature" required="">  
                                      <div class="input-group">
                                      <input type="text" class="form-control bc-font mb-3 pb-1 ds-form-con px-0" name="roomFeature[]" placeholder="Room Facilities" aria-label="default bc-font input example " value="<?= $value->fasd_feature ?>">
                                          <span class="input-group-addon text-dark chooseicon" style="cursor:pointer">Choose icon</span>
                                      </div>     
                                </div>
                            </div>
                        </div>
                        <?php // endforeach ?>                       -->
                    </div>
                    
                    
                  
                    <div class="row">
                        <div class="col-md-12" > 
                            <a href="#" class="text-dark d-block py-3 addMoreFeature" style="cursor: pointer;"><?= $this->l->l('add_feature') ?></a>
                        </div>
                    </div>

                    <h2 class="bc-mt-5 pb-4 js-vi"><?= $this->l->l('services') ?></h2>
                    <div class="row room_services" id="displayMoreServiceSection">
                        <!-- <?php //foreach ($room_services as $key => $value): ?>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-9">
                                    <input class="form-control icp icp-auto" data-input-search="true" name="iconnameService[]" value="fas fa-wifi" type="text" placeholder="Search Icon" /> 
                                    <input type="hidden" class="form-control" name="rs_single_id[]" value="<?= $value->fasd_id ?>" required="">    
                                    <input type="hidden" class="form-control" name="service" value="SERVICE" placeholder="Room service" required="">    
                                    <div class="input-group">
                                        <input type="text"  class="form-control bc-font mb-3 pb-1 ds-form-con px-0" name="roomService[]" value="<?= $value->fasd_feature ?>" placeholder="Room Service" aria-label="default bc-font input example">
                                        <span class="input-group-addon text-dark chooseicon" style="cursor:pointer">Choose icon</span>
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <?php //endforeach ?> -->
                    </div>
                   
                    <div class="row">
                        <div class="col-md-12">
                            <a href="#" class="text-dark d-block py-3 addMoreService"><?= $this->l->l('add_service') ?></a>
                        </div>
                    </div>

                        <div class="pt-4 text-center h3 bc-mt-5 fw-lighter">
                                <input type="submit" name="submit" id="upload" value="<?= $this->l->l('save_all_changes') ?>"  class="js-labelll text-dark" style="border:none;background: none;" />
                       </div>
                    </form>   
                </div>

            </div>

            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                     aria-labelledby="v-pills-home-tab">
                    <!-- Place data here-->
                </div>
                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel"
                     aria-labelledby="v-pills-profile-tab">
                    <!-- Place data here-->
                </div>
                <div class="tab-pane fade" id="v-pills-messages" role="tabpanel"
                     aria-labelledby="v-pills-messages-tab">
                    <!-- Place data here-->
                </div>
                <div class="tab-pane fade" id="v-pills-settings" role="tabpanel"
                     aria-labelledby="v-pills-settings-tab">
                    <!-- Place data here-->
                </div>
            </div>
        </div>
        <div class="col-md-2 d-block d-md-none d-lg-block order-2 order-md-3 align-items-center">
            <div class="ds">
                <div class="bc-flex">
                    <span class="small bc-in-sm d-none d-md-block">Buongiorno Gregory</span>
                    <a href="#" class="mx-3"><img src="<?= base_url() ?>assets/img/icon/user.png" alt="user" class="img-fluid w-md-75 jk-s"></a>
                    <p class="small bc-in-sm d-block d-md-none">Buongiorno <br>Gregory</p>
                </div>
            </div>
        </div>
        <div class="col-xl-3 order-4 pt-5 offset-xl-9 col-md-6 offset-md-6 col-lg-5 offset-lg-7">
            <div class="pt-5 small js-piccc">
                <?= $this->l->l('hotel_benaco_backend_administration_v') ?>.01/2021</div>
            <a href="#" class="text-dark small js-piccc">Support Magma Studio</a>
        </div>
        <div class="panel-footer d-none">
            <button class="btn btn-danger action-destroy">Destroy instances</button>
            <button class="btn btn-default action-create">Create instances</button>
        </div>

    </div>


<!-- Add Room Categories -->
  <div class="modal fade" id="modal-lg">
     <div class="modal-dialog modal-lg">
       <div class="modal-content">
         <div class="modal-header">
           <h4 class="modal-title">Add Room Category</h4>
           <div id="msg"></div>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body">
           <!-- form start -->
           <form  id="add_room_category" method="post">
             <div class="card-body">
               <div id="messageForm"></div>
               <div class="row">
                 <div class="col-md-12">
                   <div class="form-group">
                     <label for="name">Room Category Name</label>
                     <input type="text" class="form-control" id="room_cat_name" name="room_cat_name" placeholder="Room Category Name" required="" >
                   </div>
                 </div>
                 <div class="col-md-12">
                   <div class="form-group">
                     <label for="name">Room Category Italian Name</label>
                     <input type="text" class="form-control" id="room_cat_italian_name" name="room_cat_italian_name" placeholder="Room Category Italian Name" required="" >
                   </div>
                 </div>
               </div>
           
             
             <!-- /.card-body -->

             <div class="card-footer">
               <button type="submit" class="btn btn-primary float-right">Submit</button>
             </div>
             </div>
           </form>
         
         
       </div>
       <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
   </div> 
</div>

<!-- Update Room Categories -->
  <div class="modal fade" id="modal-lg-update">
     <div class="modal-dialog modal-lg">
       <div class="modal-content">
         <div class="modal-header">
           <h4 class="modal-title">Update Room Category</h4>
           <div id="msg"></div>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body">
           <!-- form start -->
           
             <div class="card-body">
                                 <div class="table-responsive">
                                              <table id="tbl_id" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                  <th>#</th>
                                                  <th>Room Category</th>
                                                  <th>Manage</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                  <?php foreach($room_categories as $key => $val): ?>
                                                  <tr>
                                                    <td><?= ++$key?></td>
                                                    <?php if(HOTEL_BANACO_LANGUAGE == 'ITALIAN'): ?>
                                                        <td><?= $val->rc_italian_name ?></td>
                                                    <?php else: ?>   
                                                    <td><?= $val->rc_name ?></td>
                                                    <?php endif; ?>
                                                    
                                                    <td><a href="#" data-toggle="modal" data-target="#modaledit-lg<?= $val->rc_id?>"><i class="fa fa-edit text-success" title="Edit"></i></a>&emsp;|&emsp;<a href="#" data-toggle="modal" data-target="#modal-sm<?= $val->rc_id?>"><i class="fa fa-trash text-danger" title="Delete"></i></a></td>

                                                  </tr>

                                                  <!-- Edit Staff -->
                                                  
                                          <div class="modal fade" id="modaledit-lg<?= $val->rc_id?>">
                                        <div class="modal-dialog modaledit-lg">
                                          <div class="modal-content">
                                            <div class="modal-header">Edit Room Category</h4>
                                              <div id="msg"></div>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                              <!-- form start -->
                                              <form action="" class="room_category_edit" method="post">
                                                <div id="updateMsg"></div>
                                                <div class="card-body">
                                                  <input type="hidden" name="rcid" value="<?= $val->rc_id ?>">
                                                  <div class="row">
                                                    <div class="col-md-12">
                                                      <div class="form-group">
                                                        <label for="sname">Room Category Name</label>
                                                        <input type="text" class="form-control" name="room_cat_name" placeholder="Room Category Name" required="" value="<?= $val->rc_name ?>">
                                                      </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                      <div class="form-group">
                                                        <label for="sname">Room Category Italian Name</label>
                                                        <input type="text" class="form-control" name="room_cat_italian_name" placeholder="Room Category Name" required="" value="<?= $val->rc_italian_name ?>">
                                                      </div>
                                                    </div>

                                                    
                                                  </div>
                                            


                                            
                                                </div>
                                                <div id="messageForm1"></div>
                                                <!-- /.card-body -->

                                                <div class="card-footer">
                                                  <button type="submit" class="btn btn-primary float-right">Update</button>
                                                </div>
                                                
                                              </form>
                                            </div>
                                            
                                          </div>
                                          <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                      </div>
                                      
                                      <!-- Delete Staff -->
                                      <div class="modal fade" id="modal-sm<?= $val->rc_id?>">
                                        <div class="modal-dialog modal-sm">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h4 class="modal-title">Delete Room Category</h4>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                         <form action="<?php echo base_url();?>admin/delete-room-category" method="post"> 
                                            <div class="modal-body">
                                              <input type="hidden" name="rcid" value="<?= $val->rc_id?>">
                                              <!-- <input type="hidden" name="sccid" value="<?= $val->rc_id ?>"> -->
                                              <p>Are you sure, you want to delete this?</p>
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                              <button type="submit" class="btn btn-primary" name="submit">Yes</button>
                                            </div>
                                            </form>
                                          </div>
                                          <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                      </div>
                                      <!-- /.modal -->
                                                <?php endforeach; ?>
                                                </tbody>
                                                
                                               
                                              </table>
                                              </div>
               
            </div>
           
         </div>
         
       </div>
       <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
   </div> 
  <!-- & section login-->

</section>











<!-- for modal from footer.php -->
<!-- jQuery -->
<script src="<?= base_url(); ?>admin_assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap 4 -->
<script src="<?= base_url(); ?>admin_assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<!-- for modal from footer.php ends -->

<!-- Summernote -->
<script src="<?= base_url(); ?>admin_assets/plugins/summernote/summernote-bs4.min.js"></script>




 <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
</script>
<script src="<?= base_url() ?>admin_assets/plugins/fontawesome_iconpicker/dist/js/fontawesome-iconpicker.js"></script> 

<!-- icon picker 3/4 bootstrap -->
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>admin_assets/plugins/bootstrap-iconpicker-1.10.0/dist/js/bootstrap-iconpicker.bundle.min.js"></script> -->
<!-- icon picker 3/4 bootstrap ends -->

<script src="<?= base_url() ?>assets/js/bootstrap.bundle.min.js"></script>

<script>
  $(function () {
    $('.action-destroy').on('click', function () {
      $.iconpicker.batch('.icp.iconpicker-element', 'destroy');
    });
    // Live binding of buttons
    $(document).on('click', '.action-placement', function (e) {
      $('.action-placement').removeClass('active');
      $(this).addClass('active');
      $('.icp-opts').data('iconpicker').updatePlacement($(this).text());
      e.preventDefault();
    });
    // $('.icp-auto').iconpicker();
    $('.icp').on('click', function () {
      $('.icp-auto').iconpicker();

      $('.icp-dd').iconpicker({
        //title: 'Dropdown with picker',
        //component:'.btn > i'
      });
      $('.icp-opts').iconpicker({
        title: 'With custom options',
        icons: [
          {
            title: "fab fa-github",
            searchTerms: ['repository', 'code']
          },
          {
            title: "fas fa-heart",
            searchTerms: ['love']
          },
          {
            title: "fab fa-html5",
            searchTerms: ['web']
          },
          {
            title: "fab fa-css3",
            searchTerms: ['style']
          }
        ],
        selectedCustomClass: 'label label-success',
        mustAccept: true,
        placement: 'bottomRight',
        showFooter: true,
        // note that this is ignored cause we have an accept button:
        hideOnSelect: true,
        // fontAwesome5: true,
        templates: {
          footer: '<div class="popover-footer">' +
          '<div style="text-align:left; font-size:12px;">Placements: \n\
<a href="#" class=" action-placement">inline</a>\n\
<a href="#" class=" action-placement">topLeftCorner</a>\n\
<a href="#" class=" action-placement">topLeft</a>\n\
<a href="#" class=" action-placement">top</a>\n\
<a href="#" class=" action-placement">topRight</a>\n\
<a href="#" class=" action-placement">topRightCorner</a>\n\
<a href="#" class=" action-placement">rightTop</a>\n\
<a href="#" class=" action-placement">right</a>\n\
<a href="#" class=" action-placement">rightBottom</a>\n\
<a href="#" class=" action-placement">bottomRightCorner</a>\n\
<a href="#" class=" active action-placement">bottomRight</a>\n\
<a href="#" class=" action-placement">bottom</a>\n\
<a href="#" class=" action-placement">bottomLeft</a>\n\
<a href="#" class=" action-placement">bottomLeftCorner</a>\n\
<a href="#" class=" action-placement">leftBottom</a>\n\
<a href="#" class=" action-placement">left</a>\n\
<a href="#" class=" action-placement">leftTop</a>\n\
</div><hr></div>'
        }
      }).data('iconpicker').show();
    }).trigger('click');


  });
</script>
<script>
  
    $('.addMoreFeature').click(function(event) {
       event.preventDefault();
       roomFeature();
     });

   function roomFeature(){
     $('#displayMoreFeatureSection').append(`<div class="col-md-6">
                            <div class="row">
                                <div class="col-12">
                                       <input class="form-control icp icp-auto" data-input-search="true" name="iconnameFeature[]" value="fas fa-wifi" type="text" placeholder="Search Icon" />    
                                       <input type="hidden" class="form-control" name="feature" value="FEATURE" placeholder="Room service" required=""> 
                                      <div class="input-group">
                                          <input type="text" class="form-control bc-font mb-3 pb-1 ds-form-con px-0" name="roomFeature[]" placeholder="Room Facilities" required="" aria-label="default bc-font input example">
                                          <span class="input-group-addon text-dark chooseicon" style="cursor:pointer">Choose icon</span>
                                      </div>     
                                </div>
                            </div>
                        </div>`)
     $('.addMoreFeature').show();
     $('.icp-auto').iconpicker();
   }

</script>
<script type="text/javascript">
   $('.addMoreService').click(function(event) {
      event.preventDefault();
      roomService();
    });


   function roomService(){
     $('#displayMoreServiceSection').append(`<div class="col-md-4">
                            <div class="row">
                                <div class="col-9">
                                     <input class="form-control icp icp-auto" data-input-search="true" name="iconnameService[]" value="fas fa-wifi" type="text" placeholder="Search Icon" />
                                     <input type="hidden" class="form-control" name="service" value="SERVICE" placeholder="Room service" required="">    
                                    <div class="input-group">
                                        <input type="text" class="form-control bc-font mb-3 pb-1 ds-form-con px-0" name="roomService[]" placeholder="Room Facilities" required="" aria-label="default bc-font input example">
                                        <span class="input-group-addon text-dark chooseicon" style="cursor:pointer">Choose icon</span>
                                    </div>   
                                </div>
                             </div>
                        </div>`)
     $('.addMoreService').show();
     $('.icp-auto').iconpicker();
   }

</script>

<script>
    // $(function () {
        $('.summernote').summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']],
            ],
            tabsize: 1,
            height: 150
        });

        $('.tab-button').unbind().click(function (event) {
            var border = $(this).attr('data-border')
            $('.custom-border').addClass('cs-border-trn').removeClass('cs-border');
            $('.' + border + '').removeClass('cs-border-trn').addClass('cs-border')
        });
    // })
</script>


<script>
$(document).ready(function(){
    $('#upload1').change(function() {
        let val = $(this).val()
        if (val) {
            let arr = val.split('\\')
            let name = arr[arr.length - 1]
            $('#file-filename').text(name)
        } else {
             $('#file-filename').text("")
        }
    }) 
    
});

</script>


<script>
  $('.hotel-bacano-lang').change(function(e) {
      e.preventDefault();
      let lanselect = $(this).find(":selected").val();
      console.log(lanselect)
      // setTimeout(()=>{
      //   $(this).find(":selected").attr('selected');
      // },3000);
      localStorage.setItem("language", lanselect);
      $.ajax({
          url: '<?php echo base_url();?>Home/changeLang',
          type: 'POST',
          dataType: 'json',
          data: {lang: lanselect},
      })
      .done(function(result) {
          if (result=='true') {
            // return false;
              window.location.reload()
            }
      })
      .fail(function(jqXHR,exception) {
      console.log(jqXHR.responseText);
    })
  });
</script>


<script>
    
    $('.room-section').submit(function(event) {
      event.preventDefault()
      var data = new FormData(this)
      $.ajax({
        url: '<?php echo base_url();?>admin/room-page',
        type: 'POST',
        dataType: 'json',
        contentType: false,
        processData: false,
        data: data,
      })
      .done(function(result) {
        console.log(result);
        return false;
       // setTimeout(()=>{
       //            $('#dg').css('display','none');
       //            $('#ss').css('display','none');
       //          window.location.reload()
       //        },2500);
              window.scrollTo(0, 0);
              if (result.result) {
                $('#allocateMsgRoom').html('<div class="alert alert-success  mt-3">'+result.msg+'</div>');
              }else{
                $('#allocateMsgRoom').html('<div class="alert alert-danger  mt-3">'+result.msg+'</div>');
              }
      })
      .fail(function(jqXHR,exception) {
        console.log(jqXHR.responseText);
      })
    });

    

    $('.remove-image').click(function(event) {
      event.preventDefault()
      // alert();
      const imageid = $(event.target).attr('id');
      const imagepage = $(event.target).attr('data-name');
      // console.log(imageid)
      // alert(imagepage)
      $.ajax({
        url: '<?php echo base_url(); ?>admin/delete-home-page-image/'+imageid+'/'+imagepage,
        type: 'GET',
        dataType: 'json',
        contentType: false,
        processData:false,
        beforeSend:function(){
            $(event.target).hide('slow/2');
        }
      })
      .done(function(result) {
        // console.log(result);
        // return false;
        // if (result.result) {
        //   $('#allocateMsg').html('<div class="alert alert-success  mt-3">'+result.msg+'</div>');
        // } else {
        //   $('#allocateMsg').html('<div class="alert alert-danger  mt-3">'+result.msg+'</div>');
        // }
        setTimeout(()=>{
          window.location.reload()
        },2000)
      })
      .fail(function(jqXHR,exception) {
        console.log(jqXHR.responseText);
      })
    });

    $('.home-section').submit(function(event) {
      event.preventDefault()
      var data = new FormData(this)
      // console.log(data)
      // return false;
      $.ajax({
        url: '<?php echo base_url();?>admin/home-page',
        type: 'POST',
        dataType: 'json',
        contentType: false,
        processData:false,
        data: data,
      })
      .done(function(result) {
        console.log(result);
        return false;
        setTimeout(()=>{
            $('#dg').css('display','none');
            $('#ss').css('display','none');
          window.location.reload()
        },2500)
        window.scrollTo(0, 0);
        if (result.result) {
          $('#allocateMsg').html('<div class="alert alert-success  mt-3">'+result.msg+'</div>');
        }else{
          $('#allocateMsg').html('<div class="alert alert-danger  mt-3">'+result.msg+'</div>');
        }
        
      })
      .fail(function(jqXHR,exception) {
        console.log(jqXHR.responseText);
      })
    });
</script>

<script>
      $("#v-pills-tab button").click(function(event) {
       $(this).parent().children().removeClass("active");
       $(this).addClass("active");
       var key = $(this).attr("data-key");
       localStorage.setItem("roomcatid", key);
       let url = $('meta[name=url]').attr("content");
       // console.log(key)
       // $('name["room_category_id"]').val(key);
       $('#room_category_id').html('<input type="hidden" name="room_category_id" value="'+key+'">');
      
       $.ajax({
         url: '<?php echo base_url();?>admin/get-room-detail/'+key,
         type: 'GET',
         dataType: 'json',
         contentType: false,
         processData: false,
         beforeSend:function(){
            $('.double-room').html('');
            $('.room_features').html('');
            $('.room_services').html('');
         }
       })
       .done(function(result) {
         // console.log(result.room_features.fasd_feature);
         // console.log(result.room_features);
         $('#rp_id').val(result.room_page.rp_id);
         $('#room_title').val(result.room_page.rp_room_title);
         $('#intro_text').val(result.room_page.rp_intro_text);
         $('#double_room').val(result.room_page.rp_double_room);
         $('#with_or_without_balcony').val(result.room_page.rp_with_or_without_balcony);
         $('#double_room_desc').html(result.room_page.rp_double_room_desc);



         
         $('#header_image').attr('src',url+result.room_header_image.riv_path);
         $.each(result.double_room_gallery_image, function (key,val) {
             // $.each(result.double_room_gallery_image[i], function (key, val) {
                 // console.log(val.riv_path);
                        $('.double-room').append(`<div class="bc-relative">
                             <img class="img-fluid" src="${url}${val.riv_path}" alt="<?= $this->l->l('rooms') ?>" style="height:100px !important;width:200px !important">
                             <img class="img-fluid bc-abs remove-image" data-name="room" id="${val.riv_id}" src="<?= base_url() ?>assets/img/icon/remove.png" alt="remove">
                         </div>`);
             // });
         });



         $('#living_hotel_banaco_images').html(`<img class="img-fluid"  src="${url+result.living_hotel_banaco_images.riv_path}" alt="<?= $this->l->l('rooms') ?>">
          <a href="#"><img class="img-fluid bc-abs" src="<?= base_url() ?>assets/img/icon/remove.png" alt="remove"></a>`);




         $.each(result.room_features, function (key,val) {
             // $.each(result.double_room_gallery_image[i], function (key, val) {
                 console.log(val);
                        $('.room_features').append(`<div class="col-md-6"><div class="row">
                                <div class="col-12"><input class="form-control icp icp-auto" data-input-search="true" name="iconnameFeature[]" value="${val.fasd_iconname}" type="text" placeholder="Search Icon" /><input type="hidden" class="form-control" name="rf_single_id[]" value="${val.fasd_id}" required=""><input type="hidden" class="form-control" name="feature" value="FEATURE" placeholder="Room feature" required=""><div class="input-group">
                                       <input type="text" class="form-control bc-font mb-3 pb-1 ds-form-con px-0" name="roomFeature[]" placeholder="Room Facilities" aria-label="default bc-font input example " value="${val.fasd_feature}">
                                          <span class="input-group-addon text-dark chooseicon" style="cursor:pointer">Choose icon</span>
                                      </div>     
                                </div>
                            </div>
                        </div>`);
             // });
             $('.icp-auto').iconpicker();
         });


         $.each(result.room_services, function (key,val) {
             // $.each(result.double_room_gallery_image[i], function (key, val) {
                 console.log(val);
                        $('.room_services').append(`<div class="col-md-4">
                            <div class="row">
                                <div class="col-9">
                                    <input class="form-control icp icp-auto" data-input-search="true" name="iconnameService[]" value="${val.fasd_iconname}" type="text" placeholder="Search Icon" /> 
                                    <input type="hidden" class="form-control" name="rs_single_id[]" value="${val.fasd_id}" required="">    
                                    <input type="hidden" class="form-control" name="service" value="SERVICE" placeholder="Room service" required="">    
                                    <div class="input-group">
                                        <input type="text"  class="form-control bc-font mb-3 pb-1 ds-form-con px-0" name="roomService[]" value="${val.fasd_feature}" placeholder="Room Service" aria-label="default bc-font input example">
                                        <span class="input-group-addon text-dark chooseicon" style="cursor:pointer">Choose icon</span>
                                    </div>  
                                </div>
                            </div>
                        </div>`);


             // });
             $('.icp-auto').iconpicker();
         });
        return false;

         // window.location.reload();
       })
       .fail(function(jqXHR,exception) {
         console.log(jqXHR.responseText);
       })

       
    });
</script>
<script>
    $("#u-pills-tab").click(function(event){
        let tabid = $(event.target).attr('id');
        localStorage.setItem("tabid", tabid);
        // alert(tabid)
    })
    window.onload = function() {
    let tadlocalid = localStorage.getItem("tabid");
    let roomcatid = localStorage.getItem("roomcatid");
    
    if(tadlocalid == 'u-pills-home-tab'){
        // alert('hi')
        $('#u-pills-tab').children().removeClass('active');
        $('#u-pills-home-tab').addClass('active');
        $('#u-pills-home-tab').attr('aria-selected',true);
        $('#u-pills-profile').removeClass('active show');
        $('#u-pills-home').addClass('active show');
        // $('#v-pills-tab button:first-child').removeClass('active');
    } else {
        // alert('bye')
        $('#u-pills-tab').children().removeClass('active');
        $('#u-pills-profile-tab').addClass('active');
        $('#u-pills-profile-tab').attr('aria-selected',true);
        $('#u-pills-home').removeClass('active show');
        $('#u-pills-profile').addClass('active show');
        // $('#v-pills-tab button:first-child').addClass('active');
    }

    $('#v-pills-tab button').removeClass('active');
    $('#v-pills-tab button:nth-child('+roomcatid+')').addClass('active');
        }
</script>
<script type="text/javascript">
    $('#add_room_category').on('submit' , function (e) {
    e.preventDefault();
    // alert('hiii');
    // return false;
    let url = $('meta[name=url]').attr("content");
    let data = new FormData($(this).get(0))
   $.ajax({
      url: '<?php echo base_url();?>admin/insert-room-category',
      type: 'POST',
      dataType: 'json',
      contentType: false,
      processData: false,
      data:data
    })
    .done(function(result) {
      // console.log(result.massage);
      
      if (result.status) {
        $('#updateMsg').html('<div class="alert alert-success  mt-3">'+result.massage+'</div>');
      }else{
        $('#updateMsg').html('<div class="alert alert-danger  mt-3">'+result.massage+'</div>');
      }
      setTimeout(()=>{
        window.location.reload()
      },2500)
      window.scrollTo(0, 0);
    })
    .fail(function(jqXHR,exception) {
      console.log(jqXHR.responseText);
    })

   })
</script>
<script type="text/javascript">
   $('.room_category_edit').on('submit' , function (e) {
    e.preventDefault();
    let url = $('meta[name=url]').attr("content");
    let data = new FormData($(this).get(0));
    // console.log(data);
 
    $.ajax({
      url: '<?php echo base_url();?>admin/update-room-category',
      type: 'POST',
      dataType: 'json',
      contentType: false,
      processData: false,
      data:data
    })
    .done(function(result) {
      // console.log(result.massage);
      
      if (result.status) {
        $('#updateMsg').html('<div class="alert alert-success  mt-3">'+result.massage+'</div>');
      }else{
        $('#updateMsg').html('<div class="alert alert-danger  mt-3">'+result.massage+'</div>');
      }
      setTimeout(()=>{
        window.location.reload()
      },2500)
      window.scrollTo(0, 0);
    })
    .fail(function(jqXHR,exception) {
      console.log(jqXHR.responseText);
    })
   })
</script>


<!-- <script>
    // var current_index = $("#u-pills-tab").tabs("button","active");
    // $("#u-pills-tab").tabs('load',current_index);
    $('#myTab a').click(function(e) {
      e.preventDefault();
      $(this).tab('show');
    });

    // store the currently selected tab in the hash value
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
      var id = $(e.target).attr("href").substr(1);
      window.location.hash = id;
    });

    // on load of the page: switch to the currently selected tab
    var hash = window.location.hash;
    $('#myTab a[href="' + hash + '"]').tab('show');
</script> -->
<!-- <script>
       var index = 'key';
        var dataStore = window.sessionStorage;
        try {
            var oldIndex = dataStore.getItem(index);
        } catch(e) {
            var oldIndex = 0;
        }
        $('#u-pills-tab').tabs({
            active : oldIndex,
            
            activate : function( event, ui ){
                var newIndex = ui.newTab.parent().children().index(ui.newTab);
                dataStore.setItem( index, newIndex ) 
            }
        }); 
</script> -->


</body>
</html>





