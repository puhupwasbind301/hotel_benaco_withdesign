<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="url" content="<?= base_url() ?>"/>
    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/ds.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/animate.min.css"/>
    <?php include 'include/header.php'; ?>
    <title>Camera Singola</title>

</head>
<body>
<!-- section one-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 px-0">
            <div class="cs-banner-img">
                <div class="row mx-0 p-4 align-items-center">
                    <div class="col-6">
                        <div class="d-block wow fadeInLeft">
                            <a href="tel:123-456-7890" class="text-decoration-none">
                                <img class="p-1 ds-icon" src="<?= base_url() ?>assets/img/icon/telephone.png" alt="phone call"/>
                                <span class="text-white ds-s-bb"> Chiamaci</span></a>
                        </div>
                        <div class="d-block wow fadeInLeft">
                            <a href="#" class="text-decoration-none">
                                <img class="p-1 ds-icon" src="<?= base_url() ?>assets/img/chat.png" alt="chat"/>
                                <span class="text-white ds-s-bb"> Scrivici </span></a>
                        </div>
                        <div class="d-block" >
                            <a href="<?= base_url() ?>index" class="text-decoration-none"><img class="p-1 ds-icon"  src="<?= base_url() ?>assets/img/prev.png"  alt="chat"/ >
                            <span class="text-white ds-s-bb"> Home Page </span></a>
                        </div>
                    </div>

                    <div class="col-6">
                      
                            <table style="float: right;">
                           <!--      <tr><td>
                            <a href="#loginModal" data-toggle="modal" style="float:right;font-weight:bolder;font-size:20px;color:white;text-decoration: none;" >Login   |</a>
                            </td>
                            <td>
                            <a href="#signUpModal" data-toggle="modal" style="float:right;font-weight:bolder;font-size:20px;color:white;text-decoration: none">Sign Up</a>
                            </td>
                            </tr> -->
                            <tr><td colspan="2">
                            <select class="form-select wow fadeInRight ds-op text-white border-0 bg-transparent w-auto ds-ml-auto   hotel-bacano-language" aria-label="Default select example" data-aos="fade-in" data-aos-duration="2500">>
                                <option value="" disabled="disabled" selected>Select Language</option>
                                <option value="ITALIAN" >Italian</option>
                                <option value="ENGLISH" >English</option>
                            </select>
                            </td></tr>
                        
                        </table>
                    </div>
                </div>
                <a href="#" class="cs-abs"><img class="ds-sd wow fadeInDown" src="<?= base_url() ?>assets/img/icon/scroll-down.png"
                                                alt="down arrow"/></a>
            </div>
        </div>
    </div>
</div>
<!-- & section one-->

<!-- section two-->
<section class="ds-section ds-bg-gr ds-py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="fw-lighter px-2 ds-ff p-md-5 wow fadeInLeft"><?= $double_room->dr_heading_title ?></h4>
                <h1 class="py-4 ds-bgt wow fadeInLeft"><?= $double_room->dr_heading ?></h1>
                <h4 class="fw-lighter ds-ff text-uppercase px-2 px-md-5 wow fadeInLeft"><?= $double_room->dr_sub_heading ?></h4>
                <p class="p-2 p-md-5 ds-p wow fadeInLeft"><?= nl2br($double_room->dr_description) ?>
                </p>
            </div>
            <div class="col-md-12 ds-text-center">
                <div class="row">
                    <div class="col-md-4">
                        <div class="d-inline-block wow fadeInRight">
                            <label for="arrive" class="d-block mb-1 small">Arrival Date:</label>
                            <input class="cs-arrive-date" type="date" value="2021-07-14"
                            />
                        </div>
                        <div class="d-inline-block wow fadeInRight">
                            <label for="arrive" class="d-block mb-1 small">Arrival Date:</label>
                            <input class="cs-arrive-date" type="date" value="2021-07-21"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="d-inline-block wow fadeInRight">
                            <p class="mb-0 small">Rooms:</p>
                            <div class="cs-grid-2">
                                <div class="ds-ns">1</div>
                                <div class="">
                                    <span class="d-block cs-ib">+</span>
                                    <span class="d-block cs-ib ds-mn">-</span>
                                </div>
                            </div>
                        </div>
                        <div class="d-inline-block wow fadeInRight ds-ml">
                            <p class="mb-0 small">Adults:</p>
                            <div class="cs-grid-2">
                                <div class="ds-ns">2</div>
                                <div class="">
                                    <span class="d-block cs-ib">+</span>
                                    <span class="d-block cs-ib ds-mn">-</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="d-inline-block wow fadeInRight">
                            <label class="d-block mb-1 small">Promo Code</label>
                            <input class="cs-arrive-date-1" type="text"/>
                        </div>
                        <div class="d-inline-block wow fadeInRight ds-ml">
                            <label class="d-block mb-1 small"></label>
                            <button class="btn cs-btn">Check Availability</button>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-3 wow fadeInRight">
                        <a href="#" class="small text-dark text-decoration-none">
                            modify/cancel an existing reservation
                        </a>
                    </div>
                    <div class="col-md-4 wow fadeInRight">
                        <img class="img-fluid ds-mxw-30" src="<?= base_url() ?>assets/img/approved.png" alt="approved">
                        <span class="text-dark ds-ml">Simple <strong>Booking</strong></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- & section two-->


<!-- section three-->
<div class="cs-bn-img wow fadeInRight">
    <a href="#"><img class="cs-ri-arrow wow flip" src="<?= base_url() ?>assets/img/icon/arrow-pointing-to-left.png" alt="arrow"></a>
</div>
<!-- & section three-->


<!-- section four-->
<section class="ds-section ds-bg-grey ds-py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="py-4 cs-bgt-1 wow fadeInLeft"><?= $feature_and_service->fas_feature ?></h1>
                <p class="px-2 px-md-5 ds-p wow fadeInLeft">
                    <?php foreach ($room_features as $key => $value): ?>
                    <span> <i class="<?= $value->fasd_iconname ?>"></i>&nbsp;&nbsp;&nbsp; <?= $value->fasd_feature ?></span> <br>
                    <?php endforeach ?>
                </p>
            </div>

            <div class="col-md-6 offset-md-5">
                <h1 class="py-4 cs-bgt-1 wow fadeInRight"><?= $feature_and_service->fas_service ?></h1>
                <p class="px-2 px-md-5 ds-p wow fadeInRight">
                    <?php foreach ($room_services as $key => $value): ?>
                     <span><i class="<?= $value->fasd_iconname ?>"></i>&nbsp;&nbsp;&nbsp; <?= $value->fasd_feature ?></span> <br>
                    <?php endforeach ?>
                </p>
            </div>
        </div>
    </div>
</section>
<!-- & section four-->


<!-- section five-->
<!-- <section class="ds-bg-gr ds-py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="cs-bgt-1 pb-5 fw-lighter ds-z wow fadeInLeft">Servono Informazioni?</h1>
            </div>
            <div class="col-md-8 py-2 py-md-5">
                <div class="row px-2 px-md-5">
                    <div class="col-md-6 mb-4">
                        <input class="form-control wow fadeInRight ds-form-con" type="text" placeholder="name & cogname"
                               aria-label="default input example">
                    </div>
                    <div class="col-md-6 mb-4">
                        <input class="form-control wow fadeInRight ds-form-con" type="text"
                               placeholder="Indirizzo e-mail"
                               aria-label="default input example">
                    </div>
                    <div class="col-md-8 mb-4">
                        <input class="form-control wow fadeInRight ds-form-con" type="text" placeholder="oggetto"
                               aria-label="default input example">
                    </div>
                    <div class="col-md-12 wow fadeInRight">
                        <label for="exampleFormControlTextarea1" class="form-label">messaggio</label>
                        <textarea class="form-control ds-form-con mb-3" id="exampleFormControlTextarea1"
                                  rows="6"></textarea>
                        <input class="form-check-input bg-transparent bor-rd-0" type="checkbox" value=""
                               id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">
                            Ho letto ed accettato la <a href="#">Privacy Policy</a>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-4 py-2 py-md-5 align-self-end">
                <h1 class="cs-bgt-1 wow fadeInRight fw-lighter">Invia</h1>
            </div>
        </div>
    </div>
</section> -->

<?php include 'include/need_information.php'; ?>

<!-- & section five-->

<!-- section five-->
<section class="ds-bg-gr ds-py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="cs-bgt-1 py-2 py-lg-5 fw-lighter ds-z wow fadeInLeft"><?= $discover_other_room->dor_heading ?></h1>
            </div>
            <?php if(!empty($discover_other_room->dor_description)): ?>
            <p><?= $discover_other_room->dor_description ?></p>
            <?php endif; ?>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-8 px-0">
                <div class="ds-re">
                    <a href="#"><img class="ds-ri-arrow" src="<?= base_url() ?>assets/img/icon/arrow-pointing-to-left.png" alt="prev"></a>
                </div>
                <img class="img-fluid wow fadeInLeft ds-i5" src="<?= base_url() ?>assets/img/hotel-benaco/camere.jpg" alt="camere">
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 px-0">
                <div class="ds-bg-grey">
                    <div class="px-2 px-lg-5">
                        <h1 class="fw-lighter pt-4 pb-4 wow fadeInRight"><?= $discover_other_room->dor_guest_number ?></h1>
                        <div class="row py-5">

                            <!-- <div class="col-md-5 wow fadeInRight pb-4">
                                <img class="ds-w-30" src="<?= base_url() ?>assets/img/icon/room-size.png" alt="resize">
                                <span> 20 - 25 mq</span>
                            </div> -->

                            <?php foreach ($discover_other_room_facilities as $key => $value): ?>
                            <div class="col-md-5 pb-4">
                                <i class="<?= $value->dorf_iconname ?> nav-icon"></i>
                                <span><?= $value->dorf_room_facilities ?> </span>
                            </div>
                            <?php endforeach ?>

                           <!--  <div class="col-md-7 wow fadeInRight pb-4">
                                <img class="ds-w-30" src="<?= base_url() ?>assets/img/icon/bed.png" alt="double bed">
                                <span> Max 3 Phosti Letto</span>
                            </div>
                            <div class="col-md-5 wow fadeInRight pb-4">
                                <img class="ds-w-30" src="<?= base_url() ?>assets/img/icon/tv-sat.png" alt="tv">
                                <span> TV Satellitare</span>
                            </div>
                            <div class="col-md-7 wow fadeInRight pb-4">
                                <img class="ds-w-30" src="<?= base_url() ?>assets/img/icon/air-conditional.png" alt="ac">
                                <span> Area Condizionata</span>
                            </div>
                            <div class="col-md-5 wow fadeInRight pb-4">
                                <img class="ds-w-30" src="<?= base_url() ?>assets/img/icon/wi-fi.png" alt="tv">
                                <span> Wi-Fi</span>
                            </div> -->
                            <div class="col-md-12 wow fadeInRight text-end">
                                <button class="btn ds-btn-g">Scopri</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- & section five-->


<!-- section ten-->
<!-- <section class="ds-bg-gr ds-pt-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 px-0">
                <img class="img-fluid wow fadeInLeft" src="<?= base_url() ?>assets/img/hotel-benaco/hotel-benaco-1200.jpg" alt="room">
            </div>
            <div class="col-md-6 align-self-center">
                <div class="row p-2 p-md-5">
                    <div class="col-md-6">
                        <h1 class="ds-bgt-2 fw-lighter wow fadeInRight">VISITA</h1>
                    </div>
                    <div class="col-md-6 align-self-center">
                        <p class="ds-p wow fadeInRight">Via Cavour, 30 25015 Decenzano d/G Bricia</p>
                        <a href="#" class="text-decoration-none text-dark wow fadeInRight">Google Map</a>
                    </div>

                    <div class="col-md-6 py-2 py-md-5">
                        <h1 class="ds-bgt-2 fw-lighter wow fadeInRight">SCRIVI</h1>
                    </div>
                    <div class="col-md-6 py-2 py-md-5 align-self-center">
                        <p class="ds-p wow fadeInRight">info@hotelbenaco.com</p>
                    </div>

                    <div class="col-md-6">
                        <h1 class="ds-bgt-2 fw-lighter wow fadeInRight">CIAMA</h1>
                    </div>
                    <div class="col-md-6 align-self-center">
                        <p class="ds-p wow fadeInRight">t. +39 030 9141710</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row py-4">
            <div class="col-md-2 pb-3 pb-md-0 text-center">
                <a href="#"><img class="ds-w-24 wow fadeInLeft" src="<?= base_url() ?>assets/img/icon/facebook.png" alt="facebook"></a>
                <a href="#"><img class="ds-w-24 wow fadeInLeft" src="<?= base_url() ?>assets/img/icon/instagram.png" alt="instagram"></a>
            </div>
            <div class="col-md-10">
                <div class="ds-border-bottom"></div>
                <ul class="ds-ul wow fadeInRight">
                    <li><a href="#">&copy; Hotel Benaco 2021</a></li>
                    <li><a href="#">Viale Cavour, 30 Desenzono del Garda (BS) 25015 ITALIA</a></li>
                    <li><a href="#">P.IVA IT00601680986</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Cookie Policy</a></li>
                    <li><a href="#">Design by Magma Studio</a></li>
                </ul>
            </div>
        </div>
    </div>
</section> -->
<?php include 'include/contact_detail.php'; ?>
<!-- & section ten-->


<script src="<?= base_url() ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>assets/js/wow.min.js"></script>
<script>
    new WOW().init();
</script>
<?php  include('include/footer.php'); ?>



</body>
</html>
