<!-- section nine-->
<section class="ds-bg-gr sec-100vh sec-100vh-2 pt-md-5 ds-py-5" data-scroll
         data-scroll-direction="vertical"
         data-scroll-speed="1">
    <div class="row mx-0">
        <div class="col-md-12 text-center">
            <h1 class="ds-bgt-cus fw-lighter ds-z" data-scroll
                data-scroll-direction="vertical"
                data-scroll-speed="1"><?= $this->l->l('do_you_need_information') ?></h1>
        </div>
    </div>
    <div class="container">
        <div class="messageNeed"></div>
        <form class="need_information" method="post">
        <div class="row">
            <div class="col-md-8 py-2 py-md-5" style="margin-bottom:0px;">
                <div class="row px-2 px-md-5" data-scroll
                     data-scroll-direction="vertical"
                     data-scroll-speed="2">
                    <div class="col-md-6 mb-4">
                        <input class="form-control ds-form-con" type="text" name="username" placeholder="<?= $this->l->l('name_surname') ?>"
                               aria-label="default input example">
                    </div>
                    <div class="col-md-6 mb-4">
                        <input class="form-control ds-form-con" type="email" name="email"
                               placeholder="<?= $this->l->l('email_address') ?>"
                               aria-label="default input example">
                    </div>
                    <div class="col-md-8 mb-4">
                        <input class="form-control ds-form-con" type="text" name="object" placeholder="<?= $this->l->l('object') ?>"
                               aria-label="default input example">
                    </div>
                    <div class="col-md-12">
                        <label for="exampleFormControlTextarea1" class="form-label"><?= $this->l->l('message') ?></label>
                        <textarea class="form-control ds-form-con mb-3" id="exampleFormControlTextarea1"
                                  rows="6" name="message"></textarea>
                        <input class="form-check-input bg-transparent bor-rd-0" type="checkbox" value=""
                               id="flexCheckDefault" name="privacy_need_information">
                        <label class="form-check-label ds-ff" for="flexCheckDefault">
                            <?= $this->l->l('i_have_read_and_accepted') ?><a href="#" class="text-dark"><?= $this->l->l('privacy_policy') ?></a>
                        </label>
                    </div>
                    
                </div>
            </div>
            <div class="col-md-4  py-md-5 align-self-end pull-right" style="margin-top:-80px;">
                <h1 class="ds-bgt-1 fw-lighter"><button type="submit" name="submit" style="border:0px"><?= $this->l->l('send') ?></button></h1>
            </div>
            
        </div>
        </form>
    </div>
</section>
<!-- & section nine-->
