  <div class="top-header-area business-color">
    <div class="container-fluid">
      <div class="row align-items-center">
        <div class="col-lg-5 col-md-4">
          <ul class="top-header-social-links">
            <li><a href="<?= base_url() ?>#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
            <li><a href="<?= base_url() ?>#" target="_blank"><i class="fab fa-instagram"></i></a></li>
            <li><a href="<?= base_url() ?>#" target="_blank"><i class="fab fa-twitter"></i></a></li>
            <li><a href="<?= base_url() ?>#" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
          </ul>
        </div>
        <div class="col-lg-7 col-md-8">
          <div class="top-header-contact-info text-end">
            <a href="<?= base_url() ?>#" class="email"><i class="far fa-envelope"></i> <span class="__cf_email__"
                data-cfemail="">info@letusstore.com</span></a>
            <a href="<?= base_url() ?>#" class="number"><i class="fas fa-phone-alt"></i> (+91) - 000 - 000 0000</a>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="navbar-area navbar-style-two bg-f9faff business-color">
    <div class="noke-responsive-nav">
      <div class="container">
        <div class="noke-responsive-menu">
          <div class="logo">
            <a href="<?= base_url() ?>index.php"><img class="ds-logo-width" src="<?= base_url() ?>/assets/img/logo/letusstore-logo.png" alt="logo"></a>
          </div>
        </div>
      </div>
    </div>
    <div class="noke-nav">
      <div class="container-fluid">
        <nav class="navbar navbar-expand-md navbar-light">
          <a class="navbar-brand" href="<?= base_url() ?>index.php" ><img class="ds-logo-width" src="<?= base_url() ?>/assets/img/logo/letusstore-logo.png" alt="logo"></a>
          <div class="collapse navbar-collapse mean-menu">
            <ul class="navbar-nav">
              <li class="nav-item megamenu"><a href="<?= base_url('home') ?>" class="nav-link active">Home</a></li>
              <li class="nav-item"><a href="<?= base_url('home/about-us') ?>#" class="dropdown-toggle nav-link">About Us</a>
                <ul class="dropdown-menu">

                  <li class="nav-item"><a href="<?= base_url('home/company-profile') ?>" class="nav-link">Company Profile</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/about-ceo') ?>" class="nav-link">About CEO</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/cri') ?>" class="nav-link">CRI</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/covid-precautions') ?>" class="nav-link">Covid Precautions</a></li>

                </ul>
              </li>
              <li class="nav-item"><a href="<?= base_url() ?>#" class="dropdown-toggle nav-link">Storage Types</a>
                <ul class="dropdown-menu">

                  <li class="nav-item"><a href="<?= base_url('home/private-rooms') ?>" class="nav-link">Private Rooms</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/box-storage') ?>" class="nav-link">Box Storage</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/utility-locker') ?>" class="nav-link">Utility Lockers</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/bike-storage') ?>" class="nav-link">Bike Storage</a></li>

                </ul>
              </li>
              <li class="nav-item megamenu"><a href="<?= base_url('home/benefits') ?>" class="nav-link">Benefits</a>
              </li>

              <li class="nav-item"><a href="<?= base_url() ?>#" class="dropdown-toggle nav-link">Locations</a>
                <ul class="dropdown-menu">
                  <li class="nav-item"><a href="<?= base_url('home/delhi-mundka') ?>" class="nav-link">Delhi-Mundka</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/gurgaon-hondachowk') ?>" class="nav-link">Gurgaon- Honda Chowk</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/pune-ranjangaon') ?>" class="nav-link">Pune-Ranjangaon</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/chennai') ?>" class="nav-link">Chennai</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/hyderabad') ?>" class="nav-link">Hyderabad</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/kolkatta') ?>" class="nav-link">Kolkatta</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/mumbai') ?>" class="nav-link">Mumbai</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/goa') ?>" class="nav-link">Goa</a></li>
                </ul>
              </li>



              <li class="nav-item"><a href="<?= base_url() ?>#" class="dropdown-toggle nav-link">Resources</a>
                <ul class="dropdown-menu">
                  <li class="nav-item"><a href="<?= base_url('home/testimonial') ?>" class="nav-link">Testimonials</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/blogs') ?>" class="nav-link">Blogs</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/gallery') ?>" class="nav-link">Gallery</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/size-guide') ?>" class="nav-link">Size Guide</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/faqs') ?>" class="nav-link">FAQ's</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/terms-and-conditions') ?>" class="nav-link">T&C's</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/privacy-policy') ?>" class="nav-link">Privacy Policy</a></li>
                  <li class="nav-item"><a href="<?= base_url('home/news') ?>" class="nav-link">News</a></li>
                </ul>
              </li>
              <li class="nav-item"><a href="<?= base_url('home/contact-us') ?>" class="nav-link">Contact</a></li>
            </ul>
            <div class="others-option d-flex align-items-center">
              <div class="option-item">
                <div class="search-box">
                  <input type="text" class="input-search" placeholder="Search">
                  <button type="submit"><i class="fas fa-search"></i></button>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </div>
    </div>
    <div class="others-option-for-responsive">
      <div class="container">
        <div class="dot-menu">
          <div class="inner">
            <div class="circle circle-one"></div>
            <div class="circle circle-two"></div>
            <div class="circle circle-three"></div>
          </div>
        </div>
        <div class="container">
          <div class="option-inner">
            <div class="others-option">
              <div class="search-box">
                <input type="text" class="input-search" placeholder="Search">
                <button type="submit"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
