
<!-- Modal -->
<div class="modal fade" id="loginModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= $this->l->l('login') ?></h4>
      </div>
      <div class="modal-body">
      	<div id="messageLogin"></div>
        <form class="login">
          <div class="form-group">
            <label for="username"><?= $this->l->l('username') ?>:</label>
            <input type="text" class="form-control" id="username" placeholder="<?= $this->l->l('enter_username') ?>" name="user">
          </div>
          <div class="form-group">
            <label for="pwd"><?= $this->l->l('password') ?>:</label>
            <input type="password" class="form-control" id="psw" placeholder="<?= $this->l->l('enter_password') ?>" name="psw">
          </div>
          <!-- <div class="checkbox">
            <label><input type="checkbox" name="remember_me"> Remember me</label>
          </div> -->
          <button type="submit"  class="btn btn-default"><?= $this->l->l('submit') ?></button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->l->l('close') ?></button>
      </div>
    </div>
    
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="signUpModal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= $this->l->l('sign_up') ?></h4>
      </div>
      <div class="modal-body">
      	<div id="messageForm"></div>
       <form class="modal-content signUp" >
          <div class="container">
            <h1><?= $this->l->l('sign_up') ?></h1>
            <p><?= $this->l->l('please_fill_in_this_form') ?></p>
            <hr>
            <label for="email"><b><?= $this->l->l('username') ?></b></label>
            <input type="text" placeholder="<?= $this->l->l('enter_username') ?>" class="form-control" name="user" required>

            <label for="email"><b><?= $this->l->l('email') ?></b></label>
            <input type="text" placeholder="<?= $this->l->l('enter_email') ?>" class="form-control" name="email" required>

            <label for="psw"><b><?= $this->l->l('password') ?></b></label>
            <input type="password" placeholder="<?= $this->l->l('enter_password') ?>" class="form-control" name="psw" required>

            <label for="psw-repeat"><b><?= $this->l->l('repeat_password') ?></b></label>
            <input type="password" placeholder="<?= $this->l->l('repeat_password') ?>" class="form-control" name="psw-repeat" required>

            <!-- <label for="psw-repeat">
              <input type="checkbox" checked="checked" class="form-control" name="remember_me" style="margin-bottom:15px"> Remember me
            </label> -->

            <!-- <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p> -->

            <div class="clearfix">
              <button type="button" class="close" data-dismiss="modal"><?= $this->l->l('cancel') ?></button>
              <button type="submit" name="submit" class="signup"><?= $this->l->l('sign_up') ?></button>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->l->l('close') ?></button>
      </div>
    </div>
  </div>
</div>