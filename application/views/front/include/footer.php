
  <!-- <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> -->
  <script data-cfasync="false" src="https://firepitsly.com/cdn-cgi/scripts/5c5dd728/Cloudflare-static/email-decode.min.js"></script>
  <!-- jQuery -->
      <!-- jQuery -->
      <script src="<?= base_url(); ?>admin_assets/plugins/jquery/jquery.min.js"></script>
      <!-- jQuery UI 1.11.4 -->
      <script src="<?= base_url(); ?>admin_assets/plugins/jquery-ui/jquery-ui.min.js"></script>
      <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
      <script>
        $.widget.bridge('uibutton', $.ui.button)
      </script>
      <!-- Bootstrap 4 -->
      <script src="<?= base_url(); ?>admin_assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- ChartJS -->
      <script src="<?= base_url(); ?>admin_assets/plugins/chart.js/Chart.min.js"></script>
      <!-- Sparkline -->
      <script src="<?= base_url(); ?>admin_assets/plugins/sparklines/sparkline.js"></script>
      <!-- JQVMap -->
      <script src="<?= base_url(); ?>admin_assets/plugins/jqvmap/jquery.vmap.min.js"></script>
      <script src="<?= base_url(); ?>admin_assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
      <!-- jQuery Knob Chart -->
      <script src="<?= base_url(); ?>admin_assets/plugins/jquery-knob/jquery.knob.min.js"></script>
      <!-- daterangepicker -->
      <script src="<?= base_url(); ?>admin_assets/plugins/moment/moment.min.js"></script>
      <script src="<?= base_url(); ?>admin_assets/plugins/daterangepicker/daterangepicker.js"></script>
      <!-- Tempusdominus Bootstrap 4 -->
      <script src="<?= base_url(); ?>admin_assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
      <!-- Summernote -->
      <script src="<?= base_url(); ?>admin_assets/plugins/summernote/summernote-bs4.min.js"></script>
      <!-- overlayScrollbars -->
      <script src="<?= base_url(); ?>admin_assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
      <!-- AdminLTE App -->
      <script src="<?= base_url(); ?>admin_assets/dist/js/adminlte.js"></script>
      <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
      <script src="<?= base_url(); ?>admin_assets/dist/js/pages/dashboard.js"></script>
      <!-- AdminLTE for demo purposes -->
      <script src="<?= base_url(); ?>admin_assets/dist/js/custom.js"></script>


      <script src="<?= base_url() ?>admin_assets/plugins/datatables/jquery.dataTables.js"></script>
      <script src="<?= base_url() ?>admin_assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
      <script src="<?= base_url() ?>admin_assets/plugins/toastr/toastr.min.js"></script>
      <script src="<?= base_url()?>admin_assets/dist/js/jquery.timeselector.js"></script>

      <script type="text/javascript" >
        // $('.hotel-bacano-language').on('change', function() {
        //   var l = $(this).val();
        //   alert(l);  
        // });

        $('.hotel-bacano-language').change(function(e) {
            e.preventDefault();
            
            let lanselect = $(this).find(":selected").val();
            // setTimeout(()=>{
            //   $(this).find(":selected").attr('selected');
            // },3000);
            localStorage.setItem("language", lanselect);
            $.ajax({
                url: '<?php echo base_url();?>Home/changeLang',
                type: 'POST',
                dataType: 'json',
                data: {lang: lanselect},
            })
            .done(function(result) {
                if (result=='true') {
                  // return false;
                    window.location.reload()
                    // 

                }
            })
            .fail(function(jqXHR,exception) {
            console.log(jqXHR.responseText);
          })
        });
        
        // $('.hotel-bacano-language').find(":selected").attr('selected');
      </script>

      <script type="text/javascript">
       
        // $('.carousel').carousel({
         // interval: 2000
        // })

        // document.getElementsById("hSlider").classList.add('active');
        
            // P Starts
            // $('#hSlider').first().addClass('active');
                // document.getElementsById("hSlider").firstElementChild.classList.add('active');
                // console.log('hklasdf');
                // console.log(document.getElementsById("hSlider").firstElementChild+'askdflksd');
            // P Ends
        
      </script>

      <script type="text/javascript">
        $(function() {

          $('.need_information').on('submit' , function(e){
          e.preventDefault();
          
          let url = $('meta[name=url]').attr("content");

          let data = new FormData($(this).get(0))
          ajax(url+"home/insert-need-information", data).then(function(result) {
              
            if(result.result){
              // window.location.reload()
              $('.messageNeed').html('<div class="alert alert-success">'+result.msg+'</div>');
            }
            else{
              $('.messageNeed').html('<div class="alert alert-danger">'+result.msg+'</div>');
              // $('#mobileror').html(result);
            }
            // window.location.reload()
          }).catch(function(e){
            console.log(e)
          })

         })
        })

      </script>
<!--       <script src="<?= base_url()?>admin_assets\plugins\bootstrap-nice-number-increamentor\dist\jquery.nice-number.js"></script>
      <script type="text/javascript">
        
          $('#numberFormat').niceNumber();
        $('#numberFormat').niceNumber({

          // auto resize the number input
          autoSize: true,

          // the number of extra character
          autoSizeBuffer: 1,

          // custom button text
          buttonDecrement: '-',
          buttonIncrement: "+",

          // 'around', 'left', or 'right'
          buttonPosition: 'around'
          
        });
      </script> -->
      <script type="text/javascript">
        
        $('.roomsNumberIncreament').on('click',function (e) {
            e.preventDefault();
            // 144 calculator
            let noOfRooms = parseInt($('.roomsBooked').text());
            if(noOfRooms >= 100) return false;
            $('.roomsBooked').html(noOfRooms+1);
            console.log(noOfRooms)
            // let squarefeet_maximum = $('#squarefeet_maximum').html();
          });

        $('.roomsNumberDecreament').on('click',function (e) {
            e.preventDefault();
            // 144 calculator
            let noOfRooms = parseInt($('.roomsBooked').text());
            if(noOfRooms < 1) return false;
            $('.roomsBooked').html(noOfRooms-1);
            console.log(noOfRooms)
            // let squarefeet_maximum = $('#squarefeet_maximum').html();
          });

        $('.adultsIncreament').on('click',function (e) {
            e.preventDefault();
            // 144 calculator
            let adultsTotal = parseInt($('.adultsTotal').text());
            if(adultsTotal >= 100) return false;
            $('.adultsTotal').html(adultsTotal+1);
            console.log(adultsTotal)
            // let squarefeet_maximum = $('#squarefeet_maximum').html();
          });

        $('.adultsDecreament').on('click',function (e) {
            e.preventDefault();
            // 144 calculator
            let adultsTotal = parseInt($('.adultsTotal').text());
            if(adultsTotal < 1) return false;
            $('.adultsTotal').html(adultsTotal-1);
            console.log(adultsTotal)
            // let squarefeet_maximum = $('#squarefeet_maximum').html();
          });

        

        $('.check_availability_hotel').on('submit', function(e) {
           e.preventDefault();
          
           const curDate    = moment().format('YYYY-MM-DD');
           const ad         = $('input[name=arrival_date]').val();
           const dd         = $('input[name=departure_date]').val();
           const rooms      = parseInt($('.roomsBooked').text());
           const adults     = parseInt($('.adultsTotal').text());
           const promocode  = $('input[name=departure_date]').val();
           console.log(ad)
           console.log(dd)
           console.log(rooms)
           console.log(adults)
           console.log(promocode)

           if(curDate >= ad){
            console.log('nooo')
            $('input[name=arrival_date]').css('border','2px solid red')
            $('.arrivalError').html('Date not allowed.')
           }

           if(curDate >= dd){
            console.log('nooo')
            $('input[name=departure_date]').css('border','2px solid red')
            $('.arrivalError').html('Date not allowed.')
           }

           if(ad >= dd){
            console.log('nooo')
            $('input[name=arrival_date]').css('border','2px solid red')
            $('input[name=departure_date]').css('border','2px solid red')
            $('.arrivalError').html('Departure date not allowed.')
           }

           if(ad >= dd || curDate >= dd || curDate >= ad){
            return false;
           }


           const key = "1906";
           (function (i, s, o, g, r, a, m) {
               i['SBSyncroBoxParam'] = r; i[r] = i[r] || function () {
                   (i[r].q = i[r].q || []).push(arguments)
               }, i[r].l = 1 * new Date(); a = s.createElement(o),
               m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
           })(window, document, 'script', 'https://cdn.simplebooking.it/search-box-script.axd?IDA=key','SBSyncroBox');
           SBSyncroBox({
               CodLang: 'EN',
           });

           // const simpleBooking = async (key) => {
           //   const base = "https://cdn.simplebooking.it/search-box-script.axd";
           //   const query = `?IDA=key${key}`;
           //   const res = await fetch(base+query);
           //   const data = await res.json();
           //   return data;
           // }

           // simpleBooking(1906)
           // .then(data=>{
           //     console.log(data);
           // }).then(data=>console.log(data))
           // .catch(err=>console.log(err));

          
           // let url = $('meta[name=url]').attr("content");
           // let data = new FormData(this);
           // data.append('adultsTotal',adults);
           // data.append('roomsBooked',rooms);
           // data.append('calDetail',JSON.stringify(calDetail));

           // ajax(url+"home/insert-hotel-booking", data).then(function(result) {
           //  console.log(result)
           //  return false;
           //   if(result){
           //      $('#messageForm').html('<div class="alert alert-success">'+result.msg+'</div>');    
           //      setTimeout(()=>{
           //        window.location.reload();
           //      },3000);    
           //   }
           //   else{
           //     $('#messageForm').html('<div class="alert alert-danger">'+result.msg+'</div>');
           //     setTimeout(()=>{
           //       window.location.reload();
           //     },3000);        
           //     // $('#mobileror').html(result);
           //   }
             
           // }).catch(function(e){
           //   console.log(e)
           // })
          })

      </script>
      <script type="text/javascript">
         $('.login').on('submit' , function (e) {
         e.preventDefault();
        let url = $('meta[name=url]').attr("content");
         let data = new FormData($(this).get(0))
         ajax(url+"home/login-check", data).then(function(result) {
          // console.log(result)
          // exit()
           if(result.result){
             $('#messageLogin').html('<div class="alert alert-success">'+result.msg+'</div>');
           }
           else{
             $('#messageLogin').html('<div class="alert alert-danger">'+result.msg+'</div>');
             // $('#mobileror').html(result);
           }
           setTimeout(()=>{
             window.location.reload()
           },2500)
         }).catch(function(e){
           console.log(e)
         })
         // .fail(function(jqXHR,exception) {
         //   console.log(jqXHR.responseText);
         // })
        })

          $('.signUp').on('submit' , function (e) {
          e.preventDefault();
         let url = $('meta[name=url]').attr("content");
          let data = new FormData($(this).get(0))
          ajax(url+"home/sign-up", data).then(function(result) {
           // console.log(result);
           // return false;
            if(result.result){
              $('#messageForm').html('<div class="alert alert-success">'+result.msg+'</div>');
            } else {
              $('#messageForm').html('<div class="alert alert-danger">'+result.msg+'</div>');
              // $('#mobileror').html(result);
            }
            setTimeout(()=>{
              window.location.reload()
            },2500)
          }).catch(function(e){
            console.log(e)
          })
          // .fail(function(jqXHR,exception) {
          //   console.log(jqXHR.responseText);
          // })

         })


      </script>
  