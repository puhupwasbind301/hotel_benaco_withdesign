
<!-- section ten-->
<section class="ds-bg-gr ds-pt-5" data-scroll
         data-scroll-direction="vertical"
         data-scroll-speed="1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 px-0">
                
                 <?php if(empty($contact_detail->cd_image)): ?>
                  <img class="img-fluid ds-mb-insm" src="https://via.placeholder.com/700x500" alt="room" data-scroll
                       data-scroll-direction="vertical"
                       data-scroll-speed="1">    
                 <?php else: ?>
                 
                   <img class="img-fluid ds-mb-insm" src="<?= base_url() ?><?= $contact_detail->cd_image ?>" alt="room" data-scroll
                        data-scroll-direction="vertical"
                        data-scroll-speed="1">   
                 <?php endif; ?>     
            </div>
            <div class="col-md-6 align-self-center">
                <div class="row p-2 p-md-5">
                    <div class="col-md-6">
                        <h1 class="ds-bgt-2 fw-lighter" data-scroll
                            data-scroll-direction="vertical"
                            data-scroll-speed="1"><?= $this->l->l('visit') ?></h1>
                    </div>
                    <div class="col-md-6 align-self-center" data-scroll
                         data-scroll-direction="vertical"
                         data-scroll-speed="1">
                        <p class="ds-p ds-fg"><?= $contact_detail->cd_visit ?></p>
                        <a href="#" class="text-decoration-none text-dark">Google Map</a>
                    </div>

                    <div class="col-md-6 py-2 py-md-5" data-scroll
                         data-scroll-direction="vertical"
                         data-scroll-speed="2">
                        <h1 class="ds-bgt-2 fw-lighter"><?= $this->l->l('write') ?></h1>
                    </div>
                    <div class="col-md-6 py-2 py-md-5 align-self-center" data-scroll
                         data-scroll-direction="vertical"
                         data-scroll-speed="2">
                        <p class="ds-p ds-fg"><?= $contact_detail->cd_write_email ?></p>
                    </div>

                    <div class="col-md-6" data-scroll
                         data-scroll-direction="vertical"
                         data-scroll-speed="3">
                        <h1 class="ds-bgt-2 fw-lighter"><?= $this->l->l('who_loves') ?></h1>
                    </div>
                    <div class="col-md-6 align-self-center" data-scroll
                         data-scroll-direction="vertical"
                         data-scroll-speed="3">
                        <p class="ds-p ds-fg"><?= $contact_detail->cd_contact_no ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row py-5" data-scroll-position="bottom" data-scroll-speed="2" data-scroll>
            <div class="col-md-2 pb-3 pb-md-0 text-center" data-scroll
                 data-scroll-direction="horizontal"
                 data-scroll-speed="1">
                <a href="#"><img class="ds-w-24" src="<?= base_url() ?>assets/img/icon/facebook.png" alt="facebook"></a>
                <a href="#"><img class="ds-w-24" src="<?= base_url() ?>assets/img/icon/instagram.png" alt="instagram"></a>
            </div>
            <div class="col-md-10" data-scroll
                 data-scroll-direction="horizontal"
                 data-scroll-speed="1">
                <div class="ds-border-bottom"></div>
                <ul class="ds-ul">
                    <li><a href="#">&copy; Hotel Benaco 2021</a></li>
                    <li><a href="#">Viale Cavour, 30 Desenzano del Garda (BS) 25015 ITALIA</a></li>
                    <li><a href="#">P.IVA IT00601680986</a></li>
                    <li><a href="#"><?= $this->l->l('privacy_policy') ?></a></li>
                    <li><a href="#"><?= $this->l->l('cookie_policy') ?></a></li>
                    <!-- <li><a href="#">Design by Magma Studio</a></li> -->
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- & section ten-->
