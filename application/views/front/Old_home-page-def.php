<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="url" content="<?= base_url() ?>"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/locomotive-scroll/dist/locomotive-scroll.min.css">
    <script src="<?= base_url() ?>assets/plugins/locomotive-scroll/dist/locomotive-scroll.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.0/ScrollTrigger.min.js"></script>
    <title>Hotel</title>
    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://unpkg.com/aos@2.3.0/dist/aos.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/ds.css"/>
    <!-- <link rel="stylesheet" href="<?= base_url() ?>assets/css/animate.min.css"/> -->
    <?php include 'include/header.php'; ?>
    <title>Hotel Benaco</title>
</head>
<body data-scroll-container style="background-color:#eaeaea;">
<!-- section one-->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 px-0">
            <div class="ds-grid">
                <img class="img-fluid w-100 ds-banner-img"
                     src="<?= base_url() ?>assets/img/hotel-benaco/hotel-benaco-banner-1.jpg"
                     alt="hotel benaco banner"/>
                <div class="ds-grid-content" data-scroll data-scroll-speed="2">
                    <div class="row mx-0 align-items-center">
                        <div class="col-4">
                            <div class="d-block">
                                <a href="tel:123-456-7890" class="text-decoration-none">
                                    <img class="p-1 ds-icon" src="<?= base_url() ?>assets/img/icon/telephone.png" alt="phone call"
                                         data-aos="fade-in" data-aos-duration="2500"/>
                                    <span class="text-white ds-s-bb" data-aos="fade-in" data-aos-duration="2000"> <?= $this->l->l('call_us') ?></span></a>
                            </div>
                            <div class="d-block">
                                <a href="#" class="text-decoration-none">
                                    <img class="p-1 ds-icon" src="<?= base_url() ?>assets/img/chat.png" alt="chat" data-aos="fade-in"
                                         data-aos-duration="2500"/>
                                    <span class="text-white ds-s-bb" data-aos="fade-in" data-aos-duration="2000">  <?= $this->l->l('write_to_us') ?> </span>
                                </a>
                            </div>
                            <div class="d-block">
                                <a href="<?= base_url() ?>home/camera-singola" class="text-decoration-none"><img class="p-1 ds-icon" src="<?= base_url() ?>assets/img/next.png"  alt="chat"/>
                                    <span class="text-white ds-s-bb"> Next Page </span></a>
                            </div>
                        </div>
                        <div class="col-4 text-center">
                            <a href="#"><img class="p-4 ds-hotel-logo" src="<?= base_url() ?>assets/img/icon/logo-hotel-benaco.png"
                                             alt="hotel" data-aos="fade-in" data-aos-duration="2500"/></a>
                        </div>
                        <div class="col-4">
                            <table style="float: right;">
                                <tr><td>
                            <a href="#loginModal" data-toggle="modal" style="float:right;font-weight:bolder;font-size:20px;color:white;text-decoration: none;" >Login   |</a>
                            </td>
                            <td>    
                            <a href="#signUpModal" data-toggle="modal" style="float:right;font-weight:bolder;font-size:20px;color:white;text-decoration: none">Sign Up</a>
                            </td>
                            </tr>
                            <tr><td colspan="2">
                            <select class="form-select ds-op text-white border-0 bg-transparent w-auto ds-ml-auto   hotel-bacano-language" aria-label="Default select example" data-aos="fade-in" data-aos-duration="2500" style="float: right;">
                                <option value="" disabled="disabled" selected>Select Language</option>
                                <option value="ITALIAN" >Italian</option>
                                <option value="ENGLISH" >English</option>
                            </select>
                            </td></tr>
                        
                        </table>
                    </div>
                    <div class="row mx-0">
                        <div class="col-md-12 ds-mt-9 text-center">
                            <a href="#"><img class="ds-arrow-border" src="<?= base_url() ?>assets/img/icon/play-video.png"
                                             alt="right arrow" data-aos="fade-in" data-aos-duration="2500"/></a>
                            <h1 class="text-white fw-light ds-ff pt-1 pt-md-5" data-aos="fade-up"
                                data-aos-duration="2000">
                                Le emozioni del vostro prossimo viaggio partono da qui.
                            </h1>
                        </div>
                    </div>
                    <div id="sb-container">Booking Details</div> 
                    <div class="row ds-po-re align-items-center mx-0" data-aos="fade-up" data-aos-duration="2500">
                        <div class="col-md-2">
                            <a href="#"><img class="ds-sd" src="<?= base_url() ?>assets/img/icon/scroll-down.png" alt="down arrow"/></a>
                        </div>
                        <div class="col-md-10 ds-text-center">
                            <form class="check_availability_hotel" method="post">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="d-inline-block">
                                        <label for="arrive" class="d-block mb-1 text-white small">Arrival Date:</label>

                                    <input type="date" value="" name="arrival_date" class="ds-arrive-date"/>

                                    </div>
                                    
                                    <div class="d-inline-block">
                                        <label for="arrive" class="d-block mb-1 text-white small">Departure Date:</label>
                                        <input   type="date" value="" name="departure_date" class="ds-arrive-date"/>
                                    </div>
                                    <p class="arrivalError text-white text-bolder" style="font-weight:bolder"></p>
                                </div>

                                <div class="col-md-3">
                                    <div class="d-inline-block">
                                        <p class="mb-0 text-white small">Rooms:</p>
                                        <div class="ds-grid-2 nice-numbers" style="width: auto;">
                                            <!-- <div class="ds-ns" id="numberFormat">1</div> -->
                                            <div class="ds-ns roomsBooked">1</div>
                                            <div class="">
                                                <span class="d-block ds-ib roomsNumberIncreament">+</span>
                                                <span class="d-block ds-ib ds-mn roomsNumberDecreament">-</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-inline-block ds-ml">
                                        <p class="mb-0 text-white small">Adults:</p>
                                        <div class="ds-grid-2" style="width: auto;">
                                            <div class="ds-ns adultsTotal">2</div>
                                            <div class="">
                                                <span class="d-block ds-ib adultsIncreament">+</span>
                                                <span class="d-block ds-ib ds-mn adultsDecreament">-</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="d-inline-block">
                                        <label class="d-block mb-1 text-white small">Promo Code</label>
                                        <input class="ds-arrive-date-1" type="text" name="promo_code" value=""/>
                                    </div>
                                    <div class="d-inline-block ds-ml">
                                        <label class="d-block mb-1 text-white small"></label>
                                        <button class="btn ds-btn" type="submit" name="submit">Check Availability</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                            <div class="row mt-4">
                                <div class="col-md-3">
                                    <a href="#" class="text-white small text-decoration-none">
                                        modify/cancel an existing reservation
                                    </a>
                                </div>
                                <div class="col-md-4">
                                    <img class="img-fluid ds-mxw-30" src="<?= base_url() ?>assets/img/approved.png" alt="approved">
                                    <span class="text-dark ds-ml">Simple <strong>Booking</strong></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- & section one-->

<!-- section two-->
<section class="ds-section ds-bg-gr ds-py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12" data-scroll data-scroll-speed="1">
                <h4 class="fw-lighter px-2 ds-ff p-md-1"><?= $home_page_def->hpd_heading_title ?></h4>
                <h1 class="py-4 ds-bgt" data-scroll
                    data-scroll-direction="vertical"
                    data-scroll-speed="2"><?= $home_page_def->hpd_heading ?></h1>
                <h4 class="fw-lighter ds-ff text-uppercase px-2 px-md-1" data-scroll
                    data-scroll-direction="vertical"
                    data-scroll-speed="2"><?= $home_page_def->hpd_sub_heading ?>
                </h4>
                <p class="p-2 p-md-1 ds-p" data-scroll
                   data-scroll-direction="vertical"
                   data-scroll-speed="2"><?= $home_page_def->hpd_description ?></p>
            </div>
        </div>
    </div>
</section>
<!-- & section two-->


<!-- section three-->
<div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel" data-scroll data-scroll-speed="1">
    <div class="carousel-inner" data-scroll
         data-scroll-direction="vertical"
         data-scroll-speed="1">
        <?php foreach($welcome_section_slider as $key => $value): ?> 
        <div class="carousel-item <?= ($key==0) ? 'active': ''?>">
            <img src="<?= base_url() ?><?= $value->si_image ?>" class="d-block w-100 ds-sih" alt="...">
            <a href="#" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <img class="ds-ri-arrow" src="<?= base_url() ?>assets/img/icon/arrow-pointing-to-right.png" alt="arrow"></a>
            <div class="ds-slide-go">
                <div class="">
                    <span class="h5 text-white"><?= ++$key ?></span>
                </div>
                <div class="">
                    </span><span class="h5 ds-befo text-white"><?= count($welcome_section_slider) ?></span>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
     <!--    <div class="carousel-item">
            <img src="<?= base_url() ?>assets/img/hotel-benaco/hotel-benaco-banner.jpg" class="d-block w-100 ds-sih" alt="...">
            <a href="#" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <img class="ds-ri-arrow" src="<?= base_url() ?>assets/img/icon/arrow-pointing-to-right.png" alt="arrow"></a>
            <div class="ds-slide-go">
                <div class="">
                    <span class="h5 text-white">01</span>
                </div>
                <div class="">
                    </span><span class="h5 ds-befo text-white">08</span>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img src="<?= base_url() ?>assets/img/hotel-benaco/hotel-sec.jpg" class="d-block w-100 ds-sih" alt="...">
            <a href="#" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <img class="ds-ri-arrow" src="<?= base_url() ?>assets/img/icon/arrow-pointing-to-right.png" alt="arrow"></a>
            <div class="ds-slide-go">
                <div class="">
                    <span class="h5 text-white">01</span>
                </div>
                <div class="">
                    </span><span class="h5 ds-befo text-white">08</span>
                </div>
            </div>
        </div> -->
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>

<!-- & section three-->

<!-- section four-->
<section class="ds-bg-grey ds-py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-5" data-scroll style="z-index: 2;">
                <h1 class="ds-bgt-1 fw-lighter text-uppercase ds-z" data-scroll
                    data-scroll-direction="vertical"
                    data-scroll-speed="1"><?= $experience->e_heading ?></h1>
                <p class="ds-p ds-pr-4"><?= $experience->e_description ?></p>
            </div>
            <div class="col-md-7" data-scroll
                 data-scroll-direction="vertical"
                 data-scroll-speed="-1">
                <img class="ds-img-1" src="<?= base_url() ?>assets/img/hotel-benaco/hotel-benaco-700x.jpg" alt="red wine" data-scroll
                     data-scroll-direction="vertical"
                     data-scroll-speed="1">
                <div class="ds-re">
                    <div class="ds-ab">
                        <a href="#"><img class="ds-24" src="<?= base_url() ?>assets/img/back.png" alt="back"></a>
                        <a href="#"><img class="ds-24" src="<?= base_url() ?>assets/img/next.png" alt="next"></a>
                    </div>
                </div>
                <img class="ds-img-2" src="<?= base_url() ?>assets/img/hotel-benaco/hotel-benaco-500x.jpg" alt="pool" data-scroll
                     data-scroll-direction="vertical"
                     data-scroll-speed="-1">
            </div>
        </div>
    </div>
</section>
<!-- & section four-->

<!-- section five-->
<section class="ds-bg-gr ds-py-5" data-scroll
         data-scroll-direction="vertical"
         data-scroll-speed="1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="ds-bgt-1 fw-lighter text-uppercase ds-z" data-scroll
                    data-scroll-direction="vertical"
                    data-scroll-speed="1"><?= $living_hotel_banaco_room->lhbr_heading ?></h1>
                <p class="pb-2 pb-md-5" data-scroll
                   data-scroll-direction="horizontal"
                   data-scroll-speed="-2"><?= $living_hotel_banaco_room->lhbr_description ?></p>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-xl-8 px-0" data-scroll
                 data-scroll-direction="vertical"
                 data-scroll-speed="1">
                <div class="ds-re">
                    <a href="#"><img class="ds-ri-arrow" src="<?= base_url() ?>assets/img/icon/arrow-pointing-to-left.png"
                                     alt="prev"></a>
                </div>
                <?php if(empty($living_hotel_banaco_room->lhbr_image)): ?>
                <!-- <img class="img-fluid" src="https://via.placeholder.com/700x500" alt="room"> -->
                <img class="img-fluid ds-i5" src="<?= base_url() ?>assets/img/hotel-benaco/camere.jpg" alt="camere">
                <?php else: ?>
                <img class="img-fluid ds-i5" src="<?= base_url() ?><?= 
                $living_hotel_banaco_room->lhbr_image ?>" alt="camere">
                <?php endif; ?>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-4 px-0" data-scroll
                 data-scroll-direction="vertical"
                 data-scroll-speed="2">
                <div class="ds-bg-grey">
                    <div class="px-2 px-lg-5">
                        <h1 class="fw-lighter pt-4 pb-4">
                            <?= $living_hotel_banaco_room->lhbr_guest_number ?> </h1>
                        <div class="row py-5">
                            <!-- <div class="col-md-5 pb-4">
                                <img class="ds-w-30" src="<?= base_url() ?>assets/img/icon/room-size.png" alt="resize">
                                <span> 20 - 25 mq</span>
                            </div> -->

                            <?php foreach ($living_hotel_banaco_facilities as $key => $value): ?>
                            <div class="col-md-5 pb-4">
                                <i class="<?= $value->lhb_iconname ?> nav-icon"></i>
                                <span><?= $value->lhb_room_facilities ?> </span>
                            </div>
                            <?php endforeach ?>

                            <!-- <div class="col-md-7 pb-4">
                                <img class="ds-w-30" src="<?= base_url() ?>assets/img/icon/bed.png" alt="double bed">
                                <span> Max 3 Phosti Letto</span>
                            </div>
                            <div class="col-md-5 pb-4">
                                <img class="ds-w-30" src="<?= base_url() ?>assets/img/icon/tv-sat.png" alt="tv">
                                <span> TV Satellitare</span>
                            </div>
                            <div class="col-md-7 pb-4">
                                <img class="ds-w-30" src="<?= base_url() ?>assets/img/icon/air-conditional.png" alt="ac">
                                <span> Area Condizionata</span>
                            </div>
                            <div class="col-md-5 pb-4">
                                <img class="ds-w-30" src="<?= base_url() ?>assets/img/icon/wi-fi.png" alt="tv">
                                <span> Wi-Fi</span>
                            </div> -->
                            <div class="col-md-12 text-end">
                                <button class="btn ds-btn-g">Scopri</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- & section five-->

<!-- section six-->
<section class="ds-bg-grey ds-py-5" data-scroll
         data-scroll-direction="vertical"
         data-scroll-speed="1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="ds-bgt-1 fw-lighter text-uppercase ds-z" data-scroll
                    data-scroll-direction="vertical"
                    data-scroll-speed="1">PISCINA & GIARDINO</h1>
                <p class="pb-5" data-scroll
                   data-scroll-direction="horizontal"
                   data-scroll-speed="1">Pace e relax a pochi passi dal centro.</p>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 px-0">
                <div class="ds-grid-six">
                    <img class="img-fluid w-100 ds-sec-1" src="<?= base_url() ?>assets/img/hotel-benaco/garden.jpg" alt="garden" data-scroll
                         data-scroll-direction="vertical"
                         data-scroll-speed="1">
                    <div class="ds-sec-2 ds-pl-5" data-scroll
                         data-scroll-direction="vertical"
                         data-scroll-speed="1">
                        <div class="row mx-0">
                            <div class="col-md-12 pb-5 text-center">
                                <a href="#"><img class="ds-ri-arrow-1"
                                                 src="<?= base_url() ?>assets/img/icon/arrow-pointing-to-right-g.png"
                                                 alt="next"></a>
                            </div>
                            <div class="row ds-pos-relative">
                                <div class="col-md-5 p-0 ds-bor-b">
                                    <h1 class="text-white">Piscina</h1>
                                </div>
                                <div class="col-md-5"></div>
                                <div class="col-md-2 ds-pl-0 ds-bor-b">
                                    <h1 class="text-white">Giardino</h1>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- & section six-->

<!-- section seven-->
<section class="ds-bg-grey ds-py-5" data-scroll
         data-scroll-direction="vertical"
         data-scroll-speed="1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 px-0">
                <div class="ds-grid-six">
                    <img class="img-fluid ds-sec-3" src="<?= base_url() ?>assets/img/hotel-benaco/hotel-benaco-banner-1.jpg"
                         alt="hotel benaco">
                    <div class="ds-sec-2 ds-pl-5">
                        <div class="row mx-0">
                            <div class="col-md-12 pb-5 text-center" data-scroll
                                 data-scroll-direction="vertical"
                                 data-scroll-speed="1">
                                <a href="#"><img class="ds-arrow-border" src="<?= base_url() ?>assets/img/icon/play-video.png"
                                                 alt="next"></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- & section seven-->
<!-- section eight-->

<section class="ds-bg-gr ds-py-5" data-scroll
         data-scroll-direction="vertical"
         data-scroll-speed="1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <img class="img-fluid ds-imgs" src="<?= base_url() ?>assets/img/hotel-benaco/s1.jpg" alt="hotel benaco" data-scroll
                     data-scroll-direction="vertical"
                     data-scroll-speed="1">
            </div>
            <div class="col-md-6 pt-5">
                <div class="ds-re">
                    <a href="#"><img class="ds-ri-arrow-1 ds-img-pos-1"
                                     src="<?= base_url() ?>assets/img/icon/arrow-pointing-to-right-g.png"
                                     alt="next" data-scroll
                                     data-scroll-direction="vertical"
                                     data-scroll-speed="1"></a>
                </div>
                <img class="img-fluid" src="<?= base_url() ?>assets/img/hotel-benaco/s2.jpg" alt="hotel benaco" data-scroll
                     data-scroll-direction="vertical"
                     data-scroll-speed="1">
            </div>
            <div class="col-md-3 pt-5">
                <img class="img-fluid pt-5 ds-imgs" src="<?= base_url() ?>assets/img/hotel-benaco/s3.jpg" alt="hotel benaco" data-scroll
                     data-scroll-direction="vertical"
                     data-scroll-speed="1">
            </div>
        </div>
    </div>
</section>
<!-- & section eight-->


<?php include 'include/signup_and_login.php'; ?>
<?php include 'include/need_information.php'; ?>
<?php include 'include/contact_detail.php'; ?>

<!-- & section ten-->

<script src="<?= base_url() ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="https://unpkg.com/aos@2.3.0/dist/aos.js"></script>
<!-- <script src="<?= base_url() ?>assets/js/wow.min.js"></script> -->

<?php  include('include/footer.php'); ?>

<script>
    // new WOW().init();
    AOS.init({
        duration: 1200,
    })


    const scroller = new LocomotiveScroll({
        el: document.querySelector('[data-scroll-container]'),
        smooth: true
    })

    // gsap.registerPlugin(ScrollTrigger)


    scroller.on('scroll', ScrollTrigger.update)

    ScrollTrigger.scrollerProxy(
        '.container', {
            scrollTop(value) {
                return arguments.length ?
                    scroller.scrollTo(value, 0, 0) :
                    scroller.scroll.instance.scroll.y
            },
            getBoundingClientRect() {
                return {
                    left: 0, top: 0,
                    width: window.innerWidth,
                    height: window.innerHeight
                }
            }
        }
    )


    ScrollTrigger.create({
        trigger: '.image-mask',
        scroller: '.container',
        start: 'top+=30% 50%',
        end: 'bottom-=40% 50%',
        animation: gsap.to('.image-mask', {backgroundSize: '120%'}),
        scrub: 2,
        // markers: true
    })


    ScrollTrigger.addEventListener('refresh', () => scroller.update())


    ScrollTrigger.refresh()
</script>
<script type="text/javascript">
    $('#startDate').daterangepicker({
      singleDatePicker: true,
      startDate: moment().subtract(6, 'days')
    });

    $('#endDate').daterangepicker({
      singleDatePicker: true,
      startDate: moment()
    });

    if ($('#startDate').val().length)
      options.startDate = $('#startDate').val();

    if ($('#endDate').val().length)
      options.endDate = $('#endDate').val();

</script>


<!-- & section ten-->

<script type="text/javascript">
(function (i, s, o, g, r, a, m) {
    i['SBSyncroBoxParam'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://cdn.simplebooking.it/search-box-script.axd?IDA=REPLACE_THIS_WITH_YOUR_HOTEL_ID','SBSyncroBox');
SBSyncroBox({
    CodLang: 'EN',
});

</script>
<script type="text/javascript">
(function (i, s, o, g, r, a, m) {
    i['SBSyncroBoxParam'] = r; i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://cdn.simplebooking.it/search-box-script.axd?IDA=REPLACE_THIS_WITH_YOUR_HOTEL_ID','SBSyncroBox');

SBSyncroBox({
    CodLang: 'EN',
    
});
</script>

</body>
</html>


<!-- section nine-->
<section class="ds-bg-gr ds-py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="ds-bgt-1 pb-5 fw-lighter text-uppercase ds-z">Servono Informazioni?</h1>
            </div>
            <div id="messageForm"></div>
            <form class="need_information" method="post">
            <div class="col-md-8 py-2 py-md-5">
                <div class="row px-2 px-md-5">
                    <div class="col-md-6 mb-4">
                        <input class="form-control ds-form-con" name="username" type="text" placeholder="name & cogname"
                               aria-label="default input example">
                    </div>
                    <div class="col-md-6 mb-4">
                        <input class="form-control ds-form-con" name="email" type="text" placeholder="Indirizzo e-mail"
                               aria-label="default input example">
                    </div>
                    <div class="col-md-8 mb-4">
                        <input class="form-control ds-form-con" name="object" type="text" placeholder="oggetto"
                               aria-label="default input example">
                    </div>
                    <div class="col-md-12">
                        <label for="exampleFormControlTextarea1" class="form-label">messaggio</label>
                        <textarea class="form-control ds-form-con mb-3" id="exampleFormControlTextarea1"
                                  rows="6" name="message"></textarea>
                        <input class="form-check-input bg-transparent bor-rd-0" type="checkbox" value=""
                               id="flexCheckDefault">
                        <label class="form-check-label" for="flexCheckDefault">
                            Ho letto ed accettato la <a href="#">Privacy Policy</a>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-4 py-2 py-md-5 align-self-end">
                <h1 class="ds-bgt-1 fw-lighter"><button type="submit" name="submit" class="btn btn-primary btn-lg">Invia</button></h1>
            </div>
            </form>
        </div>
    </div>
</section>
<!-- & section nine-->


<!-- section ten-->
<section class="ds-bg-gr ds-pt-5" data-scroll
         data-scroll-direction="vertical"
         data-scroll-speed="1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 px-0">
                 <?php if(empty($contact_detail->cd_image)): ?>
                 <img class="img-fluid" src="https://via.placeholder.com/700x500" alt="room" data-scroll
                      data-scroll-direction="vertical"
                      data-scroll-speed="1">
                 <?php else: ?>
                 <img class="img-fluid" src="<?= base_url() ?><?= $contact_detail->cd_image ?>" alt="room" data-scroll
                      data-scroll-direction="vertical" data-scroll-speed="1">
                 <?php endif; ?>    
            </div>
            <div class="col-md-6 align-self-center">
                <div class="row p-2 p-md-5">
                    <div class="col-md-6">
                        <h1 class="ds-bgt-2 fw-lighter" data-scroll
                            data-scroll-direction="vertical"
                            data-scroll-speed="1">VISITA</h1>
                    </div>
                    <div class="col-md-6 align-self-center" data-scroll
                         data-scroll-direction="vertical"
                         data-scroll-speed="1">
                        <p class="ds-p"><?= $contact_detail->cd_visit ?></p>
                        <a href="#" class="text-decoration-none text-dark">Google Map</a>
                    </div>

                    <div class="col-md-6 py-2 py-md-5" data-scroll
                         data-scroll-direction="vertical"
                         data-scroll-speed="2">
                        <h1 class="ds-bgt-2 fw-lighter">SCRIVI</h1>
                    </div>
                    <div class="col-md-6 py-2 py-md-5 align-self-center" data-scroll
                         data-scroll-direction="vertical"
                         data-scroll-speed="2">
                        <p class="ds-p"><?= $contact_detail->cd_write_email ?></p>
                    </div>

                    <div class="col-md-6" data-scroll
                         data-scroll-direction="vertical"
                         data-scroll-speed="3">
                        <h1 class="ds-bgt-2 fw-lighter">CIAMA</h1>
                    </div>
                    <div class="col-md-6 align-self-center" data-scroll
                         data-scroll-direction="vertical"
                         data-scroll-speed="3">
                        <p class="ds-p"><?= $contact_detail->cd_contact_no ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row py-4" data-scroll-position="bottom" data-scroll-speed="2" data-scroll>
            <div class="col-md-2 pb-3 pb-md-0 text-center" data-scroll
                 data-scroll-direction="horizontal"
                 data-scroll-speed="1">
                <a href="#"><img class="ds-w-24" src="<?= base_url() ?>assets/img/icon/facebook.png" alt="facebook"></a>
                <a href="#"><img class="ds-w-24" src="<?= base_url() ?>assets/img/icon/instagram.png" alt="instagram"></a>
            </div>
            <div class="col-md-10" data-scroll
                 data-scroll-direction="horizontal"
                 data-scroll-speed="1">
                <div class="ds-border-bottom"></div>
                <ul class="ds-ul">
                    <li><a href="#">&copy; Hotel Benaco 2021</a></li>
                    <li><a href="#">Viale Cavour, 30 Desenzono del Garda (BS) 25015 ITALIA</a></li>
                    <li><a href="#">P.IVA IT00601680986</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Cookie Policy</a></li>
                    <li><a href="#">Design by Magma Studio</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>