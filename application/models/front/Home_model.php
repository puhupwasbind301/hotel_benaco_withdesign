<?php 

class Home_model extends CI_Model{


			public function authUser($username, $password) {

				$this->db->where('user_name', $username);
				$query = $this->db->get('user');
				
				if($query->num_rows() === 1) {
					$db_row = $query->row();
					// return $db_row;
					if(password_verify($password, $db_row->user_password)) {
						// $this->destroyLockscreen();
						// $this->session->set_userdata('authAdminLogin', $db_row->admin_session_key);
						// $this->session->set_userdata('authAdminLockscreen', '0');
						return true;
					} else {
						return false;	
					}

				} else {
					return false;
				}

			}

		    /*-------------------------------------------------------------------------
			| Method : set admin remember me token	
			|-------------------------------------------------------------------------*/
			public function setRememberMeToken($username, $remember_me_token, $ip_address, $lockscreen) {
				$this->db->set('admin_remember_me_token', $remember_me_token);
				$this->db->set('admin_lockscreen', $lockscreen);
				$this->db->set('admin_public_ip', $ip_address);
				$this->db->where('admin_name', $username);
				$this->db->update('admin');
				if($this->db->affected_rows() === 1) {
					return true;
				}
			}

		    /*-------------------------------------------------------------------------
			| Method : validate remember me token	
			|-------------------------------------------------------------------------*/
			public function authRememberMeCookie($remember_me_token, $username, $ipaddress) {

				$this->db->where('admin_name', $username);
				$this->db->where('admin_public_ip', $ipaddress);
				$query = $this->db->get('admin');

				if($query->num_rows() === 1) {
					$db_row = $query->row();

					if(hash_equals($db_row->admin_remember_me_token, $remember_me_token)) {
						$this->session->set_userdata('authAdminLogin', $db_row->admin_session_key);
						$this->session->set_userdata('authAdminLockscreen', $db_row->admin_lockscreen);
						return true;
					}

				}

			}

		    /*-------------------------------------------------------------------------
			| Method : set admin remember me token	
			|-------------------------------------------------------------------------*/
			public function setLockscreen() {

				$this->db->set('admin_lockscreen', 1);
				$this->db->where('admin_session_key', $this->session->userdata('authAdminLogin'));
				$this->db->update('admin');
				$this->session->set_userdata('authAdminLockscreen', '1');
				if($this->db->affected_rows() === 1) {
					return true;
				}

			}

		    /*-------------------------------------------------------------------------
			| Method : destroy lockscreen
			|-------------------------------------------------------------------------*/
			public function destroyLockscreen() {
				$this->db->set('admin_lockscreen', 0);
				$this->db->where('admin_session_key', $this->session->userdata('authAdminLogin'));
				$this->db->update('admin');
				if($this->db->affected_rows() === 1) {
					$this->session->unset_userdata('authAdminLockscreen');
					return true;
				}
			}

		    /*-------------------------------------------------------------------------
		    | Method : Old Password Check
		    |-------------------------------------------------------------------------*/
		    public function oldPasswordCheck($id){

		    	$uid = $id;

		    	$opass = $this->input->post('opassword');

		    	$this->db->where('admin_session_key', $uid);
		    	$query = $this->db->get('admin');

		    	if($query->num_rows() === 1 ){

		    		$row    = $query->row();
		    		$dbpass = $row->admin_password;

		    		if( password_verify($opass, $dbpass )){

		    			return true;    
		    		}
		    		else{
		    			return false;
		    		}
		    	}
		    	else{
		    		return false;
		    	}

		    }

		     /*-------------------------------------------------------------------------
		    | Method : Change Password
		    |-------------------------------------------------------------------------*/
		    public function changePassword($uid){

		    	$npass      = $this->input->post('npassword');
		    	$password_hash = password_hash($npass, PASSWORD_BCRYPT);

		    	$data          = array(
		    		'admin_password' => $password_hash
		    	); 

		    	$this->db->where('admin_session_key', $uid);

		    	if( $this->db->update('admin', $data) ) {

		    		return true;

		    	} else {

		    		return false;

		    	}


		    }


	    /*-------------------------------------------------------------------------
		| Method : Sign Up
		|-------------------------------------------------------------------------*/

			public function createNewUser(){
				$option = ['cost' => 18];
				$password         = $this->input->post('psw');
				$data = [
					'user_name' 	=> $this->input->post('user'),
					'user_email' 	=> $this->input->post('email'),
					'user_password' => password_hash($password, PASSWORD_BCRYPT,$option)
				];

				$this->db->insert('user',$data);
				if($this->db->affected_rows() == 1){
					return $this->db->insert_id();
				} else {
					return false;
				}
				
			}


			
	    /*-------------------------------------------------------------------------
		| Method : Login
		|-------------------------------------------------------------------------*/
			public function userLogin(){
				$option = ['cost' => 18];
				$password         = $this->input->post('psw');
				$data = [
					'user_name' 	=> $this->input->post('username'),
					'user_email' 	=> $this->input->post('email'),
					'user_password' => password_hash($password, PASSWORD_BCRYPT,$option)
				];

				$this->db->insert('user',$data);
				if($this->db->affected_rows() == 1){
					return $this->db->insert_id();
				} else {
					return false;
				}
				
			}

		/*-------------------------------------------------------------------------
		| Method : Insert insertNeedInformation
		|-------------------------------------------------------------------------*/
			public function insertNeedInformation(){
					$data = array(
						'uni_name' => $this->input->post('username'),
						'uni_email' => $this->input->post('email'),
						'uni_object' => $this->input->post('object'),
						'uni_message' => $this->input->post('message')
					);
					
					$this->db->insert('user_need_information',$data);
						if($this->db->affected_rows() == 1){
							return $this->db->insert_id();
						} else {
							return false;
						}
					}

		// public function fetchCatProductPrice($product_name) {
		// 	$this->db->where('sc_name_key',$product_name);
		// 	return $this->db->get('storage_calculator')->row('sc_name_price');
		// }

		// public function fetchCalculators($cat_id) {
		// 	$this->db->where('sc_cat_id',$cat_id);
		// 	return $this->db->get('storage_calculator')->result();
		// }	


		// public function getCatofStorageCalculator(){
		// 	return	$this->db->get('storage_calculator_categories')->result();
		// }


  

	    /*-------------------------------------------------------------------------
		| Method : get table first row
		|-------------------------------------------------------------------------*/
		public function fetchFirstRowTable($table,$table_id,$limit){
			$this->db->limit($limit);
			$this->db->order_by($table_id,'asc');
			$query = $this->db->get($table);
			return $query->result();
		}

		/*-------------------------------------------------------------------------
		| Method : get other rows
		|-------------------------------------------------------------------------*/
		public function fetchOtherRowsTable($table,$table_id,$except_rows){
			$this->db->select('*');
			$this->db->order_by($table_id,'asc');
			$query = $this->db->get($table);

			$this->db->limit($query->num_rows(), $except_rows);
			$this->db->order_by($table_id,'asc');
			$query2 = $this->db->get($table);
			return $query2->result();
		}

		/*-------------------------------------------------------------------------
		| Method : get all rows
		|-------------------------------------------------------------------------*/
		public function fetchAllRowsTable($table,$table_id){
			$this->db->select('*');
			$this->db->order_by($table_id,'asc');
			$query = $this->db->get($table);
			return $query->result();
		}


		/*-------------------------------------------------------------------------
		| Method : get banner image
		|-------------------------------------------------------------------------*/
		public function fetchBannerImage($page_name){
			$this->db->where('bi_image_page',$page_name);
			$query = $this->db->get('banner_image');
			return $query->row('bi_image');
		}


		/*-------------------------------------------------------------------------
		| Method : get Home
		|-------------------------------------------------------------------------*/
		public function fetchHome($page_name){
			$this->db->where('home_image_page',$page_name);
			$query = $this->db->get('home');
			return $query->result();
		}


    /*-------------------------------------------------------------------------
	| Method : get Table
	|-------------------------------------------------------------------------*/
	public function fetchTable($table,$table_id){
		$this->db->select('*');
		$this->db->order_by($table_id,'asc');
		$query = $this->db->get($table);
		return $query->result();
	}

	/*-------------------------------------------------------------------------
	| Method : get Fetch Table with Language
	|-------------------------------------------------------------------------*/
	public function fetchTableWithLanguage($table,$table_lang_column,$language=''){
		$this->db->where($table_lang_column,$language);
		// $this->db->order_by($table_id,'asc');
		$query = $this->db->get($table);
		return $query->result();
	}

	/*-------------------------------------------------------------------------
	| Method : get Fetch Table with Language with limit
	|-------------------------------------------------------------------------*/
	public function fetchTableWithLanguageLimit($table,$table_id,$table_lang_column,$language='',$limit=''){
		$this->db->where($table_lang_column,$language);
		$this->db->order_by($table_id,'desc');
		$this->db->limit($limit);
		$query = $this->db->get($table);
		return $query->result();
	}

	/*-------------------------------------------------------------------------
	| Method : get Fetch Table with Language
	|-------------------------------------------------------------------------*/
	public function fetchTableWithLanguageWhere($table,array $condition = []){
		$this->db->where($condition);
		// $this->db->order_by($table_id,'asc');
		$query = $this->db->get($table);
		return $query->result();
	}

	/*-------------------------------------------------------------------------
	| Method : get Fetch Table with Desc
	|-------------------------------------------------------------------------*/
	public function fetchTableWithDescWhere($table,$table_id,array $condition = []){
		$this->db->where($condition);
		$this->db->order_by($table_id,'desc');
		$query = $this->db->get($table);
		return $query->result();
	}






















    }
    ?>