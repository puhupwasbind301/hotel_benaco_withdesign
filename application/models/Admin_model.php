<?php 

class Admin_model extends CI_Model{

	/*-------------------------------------------------------------------------
	| Method : Language change	
	|-------------------------------------------------------------------------*/
	public function get_language($key = ''){
	    $this->db->where('lang_key', $key);
	    $query = $this->db->get('language');
	    return $query->row();
	}


	/*-------------------------------------------------------------------------
	| Method : authenticate admin	
	|-------------------------------------------------------------------------*/

	
	public function authAdmin($username, $password) {

		$this->db->where('admin_name', $username);
		$query = $this->db->get('admin');

		if($query->num_rows() === 1) {
			$db_row = $query->row();
			if(password_verify($password, $db_row->admin_password)) {
				$this->destroyLockscreen();
				$this->session->set_userdata('authAdminLogin', $db_row->admin_session_key);
				$this->session->set_userdata('authAdminLockscreen', '0');
				return true;
			} else {
				return false;	
			}

		} else {
			return false;
		}

	}

	/*-------------------------------------------------------------------------
	| Method : check admin	
	|-------------------------------------------------------------------------*/

	public function checkAdmin($email) {
		$this->db->where('admin_email',$email);
		$admin = $this->db->count_all_results('admin');
		if($admin === 1){
			return true;
		} else {
			return false;
		}

	}	

    /*-------------------------------------------------------------------------
	| Method : set admin remember me token	
	|-------------------------------------------------------------------------*/
	public function setRememberMeToken($username, $remember_me_token, $ip_address, $lockscreen) {
		$this->db->set('admin_remember_me_token', $remember_me_token);
		$this->db->set('admin_lockscreen', $lockscreen);
		$this->db->set('admin_public_ip', $ip_address);
		$this->db->where('admin_name', $username);
		$this->db->update('admin');
		if($this->db->affected_rows() === 1) {
			return true;
		}
	}

    /*-------------------------------------------------------------------------
	| Method : validate remember me token	
	|-------------------------------------------------------------------------*/
	public function authRememberMeCookie($remember_me_token, $username, $ipaddress) {

		$this->db->where('admin_name', $username);
		$this->db->where('admin_public_ip', $ipaddress);
		$query = $this->db->get('admin');

		if($query->num_rows() === 1) {
			$db_row = $query->row();

			if(hash_equals($db_row->admin_remember_me_token, $remember_me_token)) {
				$this->session->set_userdata('authAdminLogin', $db_row->admin_session_key);
				$this->session->set_userdata('authAdminLockscreen', $db_row->admin_lockscreen);
				return true;
			}

		}

	}

    /*-------------------------------------------------------------------------
	| Method : set admin remember me token	
	|-------------------------------------------------------------------------*/
	public function setLockscreen() {

		$this->db->set('admin_lockscreen', 1);
		$this->db->where('admin_session_key', $this->session->userdata('authAdminLogin'));
		$this->db->update('admin');
		$this->session->set_userdata('authAdminLockscreen', '1');
		if($this->db->affected_rows() === 1) {
			return true;
		}

	}

    /*-------------------------------------------------------------------------
	| Method : destroy lockscreen
	|-------------------------------------------------------------------------*/
	public function destroyLockscreen() {
		$this->db->set('admin_lockscreen', 0);
		$this->db->where('admin_session_key', $this->session->userdata('authAdminLogin'));
		$this->db->update('admin');
		if($this->db->affected_rows() === 1) {
			$this->session->unset_userdata('authAdminLockscreen');
			return true;
		}
	}

    /*-------------------------------------------------------------------------
    | Method : Old Password Check
    |-------------------------------------------------------------------------*/
    public function oldPasswordCheck($id){

    	$uid = $id;

    	$opass = $this->input->post('opassword');

    	$this->db->where('admin_session_key', $uid);
    	$query = $this->db->get('admin');

    	if($query->num_rows() === 1 ){

    		$row    = $query->row();
    		$dbpass = $row->admin_password;

    		if( password_verify($opass, $dbpass )){

    			return true;    
    		}
    		else{
    			return false;
    		}
    	}
    	else{
    		return false;
    	}

    }

     /*-------------------------------------------------------------------------
    | Method : Change Password
    |-------------------------------------------------------------------------*/
    public function changePassword($uid){

    	$npass      = $this->input->post('npassword');
    	$password_hash = password_hash($npass, PASSWORD_BCRYPT);

    	$data          = array(
    		'admin_password' => $password_hash
    	); 

    	$this->db->where('admin_session_key', $uid);

    	if( $this->db->update('admin', $data) ) {

    		return true;

    	} else {

    		return false;

    	}


    }

    /*-------------------------------------------------------------------------
	| Method : get admin profile data
	|-------------------------------------------------------------------------*/
	public function getAdminProfile() {

		$this->db->select('admin_name as username, admin_img');
		$this->db->where('admin_session_key', $this->session->userdata('authAdminLogin'));
		$query = $this->db->get('admin');
		return $query->row();

	}

	/*-------------------------------------------------------------------------
	| Method : Insert  Room  Categories 
	|-------------------------------------------------------------------------*/
	public function checkAlreadyRoomCategories($category_name){
		$this->db->where('rc_name',$category_name);
		$alcat = $this->db->count_all_results('room_categories');
		if($alcat === 0){
			return true;
		} else {
			return false;
		}

	}


	/*-------------------------------------------------------------------------
	| Method : Insert  Room Categories 
	|-------------------------------------------------------------------------*/
	public function insertRoomCategories($data){

		$this->db->insert('room_categories',$data);
		if($this->db->affected_rows() === 1){
			return true;
		} else {
			return false;
		}

	}


	/*-------------------------------------------------------------------------
	| Method : Update  Room Categories 
	|-------------------------------------------------------------------------*/
	public function updateRoomCategories(array $data,$rc_id){
		$this->db->where('rc_id',$rc_id);
		$this->db->update('room_categories',$data);
		if($this->db->affected_rows() === 1){
			return true;
		} else {
			return false;
		}

	}

	public function getCatofRooms(){
		return	$this->db->get('room_categories')->result();
	}


 /*-------------------------------------------------------------------------
	| Method : count Dashboard
	|-------------------------------------------------------------------------*/
	public function countUser() {
		return $this->db->count_all_results('user');
	}

	public function countUserMessages() {
		return $this->db->count_all_results('user_need_information');
	}

	

    /*-------------------------------------------------------------------------
	| Method : get Company Profile
	|-------------------------------------------------------------------------*/
	public function fetchTable($table,$table_id){
		$this->db->select('*');
		$this->db->order_by($table_id,'desc');
		$query = $this->db->get($table);
		return $query->result();
	}


  /*-------------------------------------------------------------------------
	| Method : get Table
	|-------------------------------------------------------------------------*/
	public function fetchTableWhere($table,$table_id,$page_column_name,$page){
		$this->db->where($page_column_name,$page);
		$this->db->order_by($table_id,'desc');
		$query = $this->db->get($table);
		return $query->result();
	}


	public function fetchTableSlider($table,$table_id,$page_field,$page_name,$join_table,$join_table_id){
		$this->db->from($table.' si');
		$this->db->join($join_table.' sip','sip.'.$join_table_id.'=si.'.$page_field,'inner');
		$this->db->where($page_field,$page_name);
		$this->db->order_by($table_id,'desc');
		$query = $this->db->get();
		return $query->result();
	}

    /*-------------------------------------------------------------------------
	| Method : get table first row
	|-------------------------------------------------------------------------*/
	public function fetchFirstRowTable($table,$table_id,$limit){
		$this->db->limit($limit);
		$this->db->order_by($table_id,'asc');
		$query = $this->db->get($table);
		return $query->result();
	}

	

	/*-------------------------------------------------------------------------
	| Method : Delete Company Profile by id
	|-------------------------------------------------------------------------*/
	public function deleteTable($table,$table_id,$deleted_id){
		$this->db->where($table_id,$deleted_id);
		$this->db->delete($table);
		if($this->db->affected_rows() === 1){

			return true;
		}
		else{
			return false;
		}

	}


	public function deleteAllDataTable($table){
		$this->db->empty_table($table);
		if($this->db->affected_rows() === 1){
			return true;
		}else{
			return false;
		}
	}

	/*-------------------------------------------------------------------------
	| Method : get Fetch Table with Language
	|-------------------------------------------------------------------------*/
	public function fetchTableWithLanguageWhere($table,array $condition = []){
		$this->db->where($condition);
		// $this->db->order_by($table_id,'asc');
		$query = $this->db->get($table);
		return $query->result();
	}

	/*-------------------------------------------------------------------------
	| Method : get Fetch Table with Desc
	|-------------------------------------------------------------------------*/
	public function fetchTableWithDescWhere($table,$table_id,array $condition = []){
		$this->db->where($condition);
		$this->db->order_by($table_id,'desc');
		$query = $this->db->get($table);
		return $query->result();
	}

	/*-------------------------------------------------------------------------
	| Method : get Deletes tables
	|-------------------------------------------------------------------------*/
	public function emptyTables(array $tables = []){
		foreach ($tables as $key => $table) {
			$query = $this->db->empty_table($table);
		}
		return $query->result();
	}



		  /*-------------------------------------------------------------------------
		| Method : Insert insertHomeDetail
		|-------------------------------------------------------------------------*/
		public function insertHomeDetail(){
				
				if($this->input->post('hpid') != ''){
					$id = $this->input->post('hpid');
				}
				else{
					$id = '';
				}

	$data = array(
		'hp_video_header_link' 			 => $this->input->post('video_header_link'),
		'hp_hotel_benaco_heading' 		 => $this->input->post('hotel_benaco_heading'),
		'hp_intro_header_title'			 => $this->input->post('intro_header_title'),
		'hp_intro_header' 	 		     => $this->input->post('intro_header'),
		'hp_intro_sub_header'            => $this->input->post('intro_sub_header'),
		'hp_intro_description'           => $this->input->post('intro_description'),
		'hp_home_experience'             => $this->input->post('home_experience')
	);

				if(empty($id)){
				$data['hp_id']	= $id;
				}

				$this->db->where('hp_id', $id);
				$q = $this->db->get('home_page');

				if($q->num_rows() === 1){
					$this->db->where('hp_id', $id);
					$this->db->update('home_page',$data);
					// echo json_encode($this->db->last_query());
					// exit();
					// if($this->db->num_rows() > 0 ){
						return 'updated';
					// }
					// else{

					// 	return false;
					// }
				} else {
					$this->db->insert('home_page',$data);
					if($this->db->affected_rows() == 1){
						return $this->db->insert_id();
					} else {
						return false;
					}
				}
			}



	  /*-------------------------------------------------------------------------
	| Method : Insert insertRoomDetail
	|-------------------------------------------------------------------------*/
	public function insertRoomDetail(){
			
			if($this->input->post('rpid') != ''){
				$id = $this->input->post('rpid');
			} else {
				$id = '';
			}
		// $this->session->set_userdata('room_id',$this->input->post('room_category_id'));
			// print_r($this->input->post('room_category_id'));
			// echo json_encode($this->input->post('room_category_id'));
$data = array(
	'rp_language' 				 => HOTEL_BANACO_LANGUAGE,
	'rp_room_id' 				 => $this->input->post('room_category_id'),
	'rp_room_title' 			 => $this->input->post('room_title'),
	'rp_intro_text' 			 => $this->input->post('intro_text'),
	'rp_double_room'			 => $this->input->post('double_room'),
	'rp_with_or_without_balcony' => $this->input->post('with_or_without_balcony'),
	'rp_double_room_desc'        => $this->input->post('double_room_desc')
);

			if(empty($id)){
				$data['rp_id']	= $id;
			}

			$this->db->where('rp_id', $id);
			// $this->db->where('rp_room_id', $this->input->post('room_category_id') );
			// $this->db->where('rp_language', HOTEL_BANACO_LANGUAGE);
			$q = $this->db->get('room_page');

			// $this->db->where('rp_id', $id);
			// $this->db->where('rp_language', 'ENGLISH');
			// $s = $this->db->get('room_page');

			if($q->num_rows() === 1){
				$this->db->where('rp_id', $id);
				$this->db->update('room_page',$data);
				// echo json_encode($this->db->last_query());
				// exit();
				// if($this->db->num_rows() > 0 ){
					return 'updated';
				// }
				// else {
				// 	return false;
				// }
			} else {
				$this->db->insert('room_page',$data);
				if($this->db->affected_rows() == 1){
					return $this->db->insert_id();
				} else {
					return false;
				}
			}
		}


		/*------------------------------------------------------------------------
		| Method : Insert insertRoomFeature
		|-------------------------------------------------------------------------*/

		public function insertRoomFeature(){
							
				// foreach ($this->input->post('rf_single_id') as $key => $update_id){
				// 	$data = [
				// 	'fasd_feature' => $this->input->post('roomFeature')[$key],
				// 	'fasd_iconname'=> $this->input->post('iconnameFeature')[$key]
				// 	];
				// 		$this->db->where('fasd_id', $update_id);
				// 		$this->db->update('room_feature_and_service',$data);
				// }
				
				$result = false;
				foreach ($this->input->post('roomFeature') as $key => $value) {
			   		if(!empty($this->input->post('rf_single_id')[$key])){
			   			$data = [
			   			'fasd_feature' => $this->input->post('roomFeature')[$key],
			   			'fasd_iconname'=> $this->input->post('iconnameFeature')[$key]
			   			];
			   				$this->db->where('fasd_id', $this->input->post('rf_single_id')[$key]);
			   				$this->db->update('room_feature_and_service',$data);
			   			} else {	
			   		$this->db->set('fasd_feature', $value);
			   		$this->db->set('fasd_iconname', $this->input->post('iconnameFeature')[$key]);
					$this->db->set('fasd_language', HOTEL_BANACO_LANGUAGE);
					$this->db->set('fasd_page_name', $this->input->post('feature'));
					$this->db->set('fasd_room_id', $this->input->post('room_category_id'));
					$result =	$this->db->insert('room_feature_and_service');
						}
					}
					return $result;
		}					


		/*------------------------------------------------------------------------
		| Method : Insert insertRoomService
		|-------------------------------------------------------------------------*/

		public function insertRoomService(){
					
			// foreach ($this->input->post('rs_single_id') as $key => $update_id){
			// 	$data = [
			// 	'fasd_feature' => $this->input->post('roomService')[$key],
			// 	'fasd_iconname'=> $this->input->post('iconnameService')[$key]
			// 	];
			// 		$this->db->where('fasd_id', $update_id);
			// 		$this->db->update('room_feature_and_service',$data);
			// }

			$result = false;
			foreach ($this->input->post('roomService') as $key => $value) {
		   		if(!empty($this->input->post('rs_single_id')[$key])){
		   			$data = [
						'fasd_feature' => $this->input->post('roomService')[$key],
						'fasd_iconname'=> $this->input->post('iconnameService')[$key]
						];
					$this->db->where('fasd_id', $this->input->post('rs_single_id')[$key]);
					$result = $this->db->update('room_feature_and_service',$data);
					} else {
			   	$this->db->set('fasd_feature', $value);
		   	   	$this->db->set('fasd_iconname', $this->input->post('iconnameService')[$key]);
				$this->db->set('fasd_language', HOTEL_BANACO_LANGUAGE);
				$this->db->set('fasd_page_name', $this->input->post('service'));
				$this->db->set('fasd_room_id', $this->input->post('room_category_id'));
				$result = $this->db->insert('room_feature_and_service');
					}
				}

				return $result;
		}					
			


 /*-------------------------------------------------------------------------
	| Method : Insert insertContactDetail
	|-------------------------------------------------------------------------*/
	public function insertContactDetail(){
			
			if($this->input->post('cdid') != ''){
				$id = $this->input->post('cdid');
			}
			else{
				$id = '';
			}

			//$date = $this->input->post('doj');
			//$newDate = date("Y-m-d", strtotime($date));

			$data = array(
				'cd_visit' => $this->input->post('visit'),
				'cd_write_email' => $this->input->post('write_email'),
				'cd_contact_no' => $this->input->post('contact_no')
			);

			if(empty($id)){
			$data['cd_id']	= $id;
			}

			$this->db->where('cd_id', $id);
			$q = $this->db->get('contact_detail');

			if($q->num_rows() === 1){
				$this->db->where('cd_id', $id);
				$this->db->update('contact_detail',$data);
				// echo json_encode($this->db->last_query());
				// exit();
				// if($this->db->num_rows() > 0 ){
					return $id;
				// }
				// else{

				// 	return false;
				// }
			} else {
				$this->db->insert('contact_detail',$data);
				if($this->db->affected_rows() == 1){
					return $this->db->insert_id();
				} else {
					return false;
				}
			}
		}

	/*-------------------------------------------------------------------------
	| Method : Insert insertAddSliderImage
	|-------------------------------------------------------------------------*/

	public function insertAddSliderImage(){
			if($this->input->post('hpdid') != '') {
				$id = $this->input->post('hpdid');
			} else {
				$id = '';
			}
			
			$data = array(
				'si_image_page' => $this->input->post('image_page')
			);

			if(empty($id)){
				$data['si_id'] = $id;
			}

			$this->db->where('si_id', $id);
			$q = $this->db->get('slider_images');

			if($q->num_rows() === 1){
				$this->db->where('si_id', $id);
				$this->db->update('slider_images',$data);
				// echo json_encode($this->db->last_query());
				// exit();
				// if($this->db->num_rows() > 0 ){
					return $id;
				// }
				// else{
				// 	return false;
				// }
			} else {
				$insert_id_arr = [];
				foreach ($this->input->post('image_page') as $key => $value) {
					echo json_encode($value);
				// $this->db->set('si_image_page', $value);
				// $this->db->insert('slider_images');
				// if($this->db->affected_rows() == 1){
				// 	$insert_id_arr[] = $this->db->insert_id();
				}
				// array_push($insert_id_arr,$this->db->insert_id());
				
				return $insert_id_arr;
				// if($this->db->affected_rows() == 1){
				// 	return $this->db->insert_id();
				// } else {
				// 	return false;
				// }
			}
			
		}					
	


	// /*-------------------------------------------------------------------------
	// 		| Method : Insert Contact Us
	// 		|-------------------------------------------------------------------------*/
	// 		public function insertContactUs(){
					
	// 				if($this->input->post('cuid') != ''){
	// 					$id = $this->input->post('cuid');
	// 				}
	// 				else{
	// 					$id = '';
	// 				}

	// 				//$date = $this->input->post('doj');
	// 				//$newDate = date("Y-m-d", strtotime($date));

	// 				$data = array(
	// 					'cu_address' => $this->input->post('address'),
	// 					'cu_phone' => $this->input->post('phone'),
	// 					'cu_email' => $this->input->post('email'),
	// 					'cu_working_hours' => $this->input->post('working_hours')
	// 				);

	// 				if(empty($id)){
	// 					$data['cu_id'] = $id;
	// 				}

	// 				$this->db->where('cu_id', $id);
	// 				$q = $this->db->get('contact_us');

	// 				if($q->num_rows() === 1){
	// 					$this->db->where('cu_id', $id);
	// 					$this->db->update('contact_us',$data);
						   


	// 					// echo json_encode($this->db->last_query());
	// 					// exit();

	// 					// if($this->db->num_rows() > 0 ){

	// 						return $id;
	// 					// }
	// 					// else{

	// 					// 	return false;
	// 					// }
	// 				}
	// 				else{

	// 					$this->db->insert('contact_us',$data);

	// 					if($this->db->affected_rows() == 1){

	// 						return $this->db->insert_id();
	// 					}
	// 					else{

	// 						return false;
	// 					}
	// 				}

					
	// 			}

			



			


    }
    ?>