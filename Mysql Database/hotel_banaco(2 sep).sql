-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 02, 2021 at 07:03 PM
-- Server version: 8.0.26-0ubuntu0.20.04.2
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel_banaco`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_session_key` varchar(255) NOT NULL,
  `admin_remember_me_token` varchar(255) DEFAULT NULL,
  `admin_public_ip` varchar(255) DEFAULT NULL,
  `admin_lockscreen` tinyint(1) DEFAULT '0' COMMENT '0 - false, 1- true',
  `admin_img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_password`, `admin_email`, `admin_session_key`, `admin_remember_me_token`, `admin_public_ip`, `admin_lockscreen`, `admin_img`) VALUES
(1, 'admin', '$2y$10$EJ041.Jp4b9HyEGr4nLC.eTB86Mva0PDKgG72GNzysmBhom2955ZS', 'admin@gmail.com', 'iAmAuthAdmin', '15e068459ce7961efe0480fbaaadcf32debe1e3e', '::1', 0, 'http://localhost/portfolio/uploads/admin_image/profileImage07092019023233716.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `banner_image`
--

CREATE TABLE `banner_image` (
  `bi_id` int NOT NULL,
  `bi_image` varchar(255) DEFAULT NULL,
  `bi_image_page` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `banner_image`
--

INSERT INTO `banner_image` (`bi_id`, `bi_image`, `bi_image_page`) VALUES
(2, 'uploads/banner_image/file_61110ab9405e61628506809.jpg', 'company-profile'),
(3, 'uploads/banner_image/file_61110e7e599101628507774.jpg', 'about-ceo'),
(6, 'uploads/banner_image/file_611113b3560aa1628509107.jpg', 'covid-precautions'),
(7, 'uploads/banner_image/file_6111144da9c101628509261.jpg', 'private-rooms'),
(8, 'uploads/banner_image/file_6111148ad6e0d1628509322.jpg', 'box-storage'),
(9, 'uploads/banner_image/file_611114c9d26e61628509385.jpg', 'utility-locker'),
(10, 'uploads/banner_image/file_61111512d09a71628509458.jpg', 'bike-storage'),
(11, 'uploads/banner_image/file_61111580089681628509568.jpg', 'benefits'),
(12, 'uploads/banner_image/file_611115d6183681628509654.png', 'testimonial'),
(13, 'uploads/banner_image/file_6111161d3eabe1628509725.jpg', 'blogs'),
(14, 'uploads/banner_image/file_6111164dbef321628509773.jpg', 'gallery'),
(15, 'uploads/banner_image/file_611116887b09b1628509832.jpg', 'size-guide'),
(16, 'uploads/banner_image/file_611116c42148e1628509892.png', 'faqs'),
(17, 'uploads/banner_image/file_611117b2a42441628510130.jpg', 'privacy-policy'),
(18, 'uploads/banner_image/file_611117bad555d1628510138.jpg', 'news'),
(19, 'uploads/banner_image/file_611117c38388e1628510147.jpg', 'contact-us'),
(20, 'uploads/banner_image/file_61111e55b11ea1628511829.png', 'terms-and-conditions'),
(21, 'uploads/banner_image/file_61166170e56811628856688.jpg', 'cri');

-- --------------------------------------------------------

--
-- Table structure for table `contact_detail`
--

CREATE TABLE `contact_detail` (
  `cd_id` int NOT NULL,
  `cd_visit` varchar(255) NOT NULL,
  `cd_write_email` varchar(255) NOT NULL,
  `cd_contact_no` varchar(255) NOT NULL,
  `cd_image` varchar(100) NOT NULL,
  `cd_created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `contact_detail`
--

INSERT INTO `contact_detail` (`cd_id`, `cd_visit`, `cd_write_email`, `cd_contact_no`, `cd_image`, `cd_created_date`) VALUES
(87, '<p>Via Cavour, 30 25015 Decenzano d/G Bricia</p>\r\n', 'info@hotelbenaco.com', 't. +39 030 9141710', 'uploads/contact_detail/file_61238f11f18341629720337.jpg', '2021-08-23 17:35:37');

-- --------------------------------------------------------

--
-- Table structure for table `discover_other_room`
--

CREATE TABLE `discover_other_room` (
  `dor_id` int NOT NULL,
  `dor_language` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dor_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `dor_description` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `dor_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `dor_guest_number` int NOT NULL,
  `dor_page_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `discover_other_room`
--

INSERT INTO `discover_other_room` (`dor_id`, `dor_language`, `dor_heading`, `dor_description`, `dor_image`, `dor_guest_number`, `dor_page_name`) VALUES
(8, 'ITALIAN', 'VIVERE HOTEL BENACO  asdfsadfasdfsa', '<p>Le Camere</p>\r\n', NULL, 3, 'home_page_def');

-- --------------------------------------------------------

--
-- Table structure for table `discover_other_room_facilities`
--

CREATE TABLE `discover_other_room_facilities` (
  `dorf_id` int NOT NULL,
  `dorf_language` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dorf_room_facilities` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `dorf_iconname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `dorf_page_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `discover_other_room_facilities`
--

INSERT INTO `discover_other_room_facilities` (`dorf_id`, `dorf_language`, `dorf_room_facilities`, `dorf_iconname`, `dorf_page_name`) VALUES
(101, 'ENGLISH', '20 - 25 mq', 'fas fa-person-booth', 'discover_other_room'),
(102, 'ENGLISH', 'Max 3 Phosti Letto', 'fas fa-bed', 'discover_other_room'),
(103, 'ENGLISH', 'TV Satellitare', 'fas fa-tv', 'discover_other_room'),
(104, 'ENGLISH', 'Area Condizionata', 'fab fa-accusoft', 'discover_other_room');

-- --------------------------------------------------------

--
-- Table structure for table `double_room`
--

CREATE TABLE `double_room` (
  `dr_id` int NOT NULL,
  `dr_language` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dr_heading_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dr_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dr_sub_heading` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dr_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `double_room`
--

INSERT INTO `double_room` (`dr_id`, `dr_language`, `dr_heading_title`, `dr_heading`, `dr_sub_heading`, `dr_description`) VALUES
(6, 'ITALIAN', 'Vi presentiamo', 'Camera Doppia', 'CON O SENZA BALCONE', '<p>L&rsquo;eleganza di queste camere non delude i clienti pi&ugrave; esigenti. Sar&agrave; la vostra migliore soluzione per alloggiare a Desenzano a tariffe vantaggiose senza rinunciare ad un ottimo servizio. Le nostre camere doppie sono arredate con gust'),
(10, 'ENGLISH', 'We present you', 'Double room', 'CON O SENZA BALCONE', 'L’eleganza di queste camere non delude i clienti più esigenti. Sarà la vostra migliore soluzione per alloggiare a Desenzano a tariffe vantaggiose senza rinunciare ad un ottimo servizio. Le nostre camere doppie sono arredate con gust');

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `e_id` int NOT NULL,
  `e_language` varchar(255) NOT NULL,
  `e_heading` varchar(255) NOT NULL,
  `e_description` text NOT NULL,
  `e_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`e_id`, `e_language`, `e_heading`, `e_description`, `e_image`) VALUES
(7, 'ENGLISH', 'EXPERIENCE', '<p>The building, now completely modernized, has elegant and comfortable rooms and preserves the ancient charm of the elegant manor houses that lived through the Bella Epoque season.</p>\r\n\r\n<p>The hotel is surrounded by Art Nouveau villas and is surrounded by a large garden guarding the last of the shady horse chestnuts that once adorned the avenue that leads from the train station to Lake Garda. The private parking, the green garden surrounding the swimming pool and the solarium make our hotel ideal for a business trip or a relaxing holiday.</p>\r\n', 'uploads/home_page_def/file_612720510b3251629954129.jpg'),
(8, 'ITALIAN', 'ESPERIENZA', '<p>L&rsquo;edificio, oggi completamente rimodernato dispone di camere eleganti e confortevoli e conserva intatto l&rsquo;antico fascino delle eleganti ville padronali che hanno vissuto la stagione della Bella EpoqL&rsquo;hotel &egrave; circondato da ville liberty ed &egrave; immerso in un grande giardino custode dell&rsquo;ultimo degli ombrosi ippocastani che un tempo ornavano il viale che dalla stazione ferroviaria porta al Lago di Garda. Il parcheggio privato, il verdeggiante giardino che circonda la piscina e il solarium rendono il nostro hotel ideale per un soggiorno di lavoro o per una vacanza all&rsquo;insegna del relax.ue.</p>\r\n\r\n<p>L&rsquo;hotel &egrave; circondato da ville liberty ed &egrave; immerso in un grande giardino custode dell&rsquo;ultimo degli ombrosi ippocastani che un tempo ornavano il viale che dalla stazione ferroviaria porta al Lago di Garda. Il parcheggio privato, il verdeggiante giardino che circonda la piscina e il solarium rendono il nostro hotel ideale per un soggiorno di lavoro o per una vacanza all&rsquo;insegna del relax.</p>\r\n', 'uploads/home_page_def/file_612720eadf5e21629954282.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `feature_and_service`
--

CREATE TABLE `feature_and_service` (
  `fas_id` int NOT NULL,
  `fas_language` varchar(255) NOT NULL,
  `fas_feature` varchar(255) NOT NULL,
  `fas_service` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feature_and_service`
--

INSERT INTO `feature_and_service` (`fas_id`, `fas_language`, `fas_feature`, `fas_service`) VALUES
(1, 'ENGLISH', 'Feature', 'Service'),
(2, 'ITALIAN', 'Caratteristiche', 'Servizi');

-- --------------------------------------------------------

--
-- Table structure for table `feature_and_service_detail`
--

CREATE TABLE `feature_and_service_detail` (
  `fasd_id` int NOT NULL,
  `fasd_language` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `fasd_feature` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fasd_iconname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `fasd_page_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `feature_and_service_detail`
--

INSERT INTO `feature_and_service_detail` (`fasd_id`, `fasd_language`, `fasd_feature`, `fasd_iconname`, `fasd_page_name`) VALUES
(123, 'ENGLISH', 'wifi', 'fas fa-wifi', 'FEATURE'),
(124, 'ENGLISH', 'Utensils', 'fas fa-utensils', 'FEATURE'),
(125, 'ITALIAN', 'wifi', 'fas fa-wifi', 'FEATURE'),
(126, 'ITALIAN', 'adjust', 'fas fa-adjust', 'FEATURE');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `home_id` int NOT NULL,
  `home_title_heading` varchar(255) DEFAULT NULL,
  `home_title` varchar(255) NOT NULL,
  `home_image` varchar(255) DEFAULT NULL,
  `home_link_name` varchar(255) DEFAULT NULL,
  `home_link` varchar(255) DEFAULT NULL,
  `home_desc` varchar(255) NOT NULL,
  `home_image_page` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`home_id`, `home_title_heading`, `home_title`, `home_image`, `home_link_name`, `home_link`, `home_desc`, `home_image_page`) VALUES
(24, NULL, 'asdfsda', 'uploads/home/file_611216c3c8b9e1628575427.jpg', NULL, NULL, 'sdafsdaf', 'meet-the-team'),
(25, ' ', 'Puhupwas', 'uploads/home/file_611b521aa30261629180442.png', ' ', ' ', 'sadfsadf', 'news-and-articles'),
(28, NULL, '', 'uploads/home/file_61121dfd1d85a1628577277.png', NULL, NULL, '', 'meet-the-team'),
(33, NULL, 'title1', 'uploads/home/file_61121e51519c51628577361.jpg', NULL, NULL, 'description 1', 'our-works'),
(34, NULL, 'title 2', 'uploads/home/file_61122641a85261628579393.jpg', NULL, NULL, 'asdfdsf', 'meet-the-team'),
(37, ' ', 'dgfdg', 'uploads/home/file_6112ba063464d1628617222.jpg', ' ', ' ', 'asdfadsf', 'our-works'),
(39, 'INTRODUCING WORKFLOW', 'INTRODUCING WORKFLOW India\'s Best Let Us Store Facility', 'uploads/home/file_6112baa4ddd8a1628617380.jpg', 'Contact Us', 'http://localhost/letusstore/home/contact-us', 'Access to adequate space, at home or office, is always an issue. Unused items tend to lie around and kill productivity. They also take up valuable space.', 'main-section'),
(40, 'ABOUT LET US STORE', 'The Thought Behind Let Us Store', 'uploads/home/file_6112bae8b6c121628617448.jpg', 'Benefits', 'http://localhost/letusstore/home/benefits', 'Our work and personal places are shrinking with each passing year and the clutter of our ever increasing “stuff” obstructs our ability to think clearly and enjoy the spaces in which we spend our time.\r\n\r\nwas created to solve the challenges of cluttering b', 'main-section'),
(41, 'Title main', 'title ', NULL, 'link1', 'google.com', 'This is the description of the  Titlemain', 'main-section'),
(42, 'Title maind', 'title ', 'uploads/home/file_61164121eb99e1628848417.png', 'link1', 'google.com', 'This is the description of the  Titlemain', 'main-section');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_def`
--

CREATE TABLE `home_page_def` (
  `hpd_id` int NOT NULL,
  `hpd_language` varchar(255) NOT NULL,
  `hpd_heading_title` varchar(255) NOT NULL,
  `hpd_heading` varchar(255) NOT NULL,
  `hpd_sub_heading` varchar(255) NOT NULL,
  `hpd_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `home_page_def`
--

INSERT INTO `home_page_def` (`hpd_id`, `hpd_language`, `hpd_heading_title`, `hpd_heading`, `hpd_sub_heading`, `hpd_description`) VALUES
(3, 'ITALIAN', 'Tutte le forme dell’accoglienza', 'Benvenuti al Benaco', 'NEL MAGNIFICO CONTESTO DEL LAGO DI GARDA', '<p>Nel cuore storico dell’isola pedonale di Desenzano, il nostro hotel vi accoglierà in un’atmosfera calda e familiare di una struttura 3 stelle che si affaccia sugli scorci più belli ed eleganti della città. La sua partic'),
(5, 'ENGLISH', 'All forms of hospitality', 'Welcome to the Benaco', 'IN THE MAGNIFICENT CONTEXT OF LAKE GARDA', '<p>In the historic heart of the pedestrian area of ​​Desenzano, our hotel will welcome you in the warm and familiar atmosphere of a 3-star hotel that overlooks the most beautiful and elegant views of the city. Its particular position is particularly appre');

-- --------------------------------------------------------

--
-- Table structure for table `living_hotel_banaco_facilities`
--

CREATE TABLE `living_hotel_banaco_facilities` (
  `lhb_id` int NOT NULL,
  `lhb_language` varchar(255) NOT NULL,
  `lhb_room_facilities` varchar(255) DEFAULT NULL,
  `lhb_iconname` varchar(255) DEFAULT NULL,
  `lhb_page_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `living_hotel_banaco_facilities`
--

INSERT INTO `living_hotel_banaco_facilities` (`lhb_id`, `lhb_language`, `lhb_room_facilities`, `lhb_iconname`, `lhb_page_name`) VALUES
(101, 'ENGLISH', '20 - 25 mq', 'fas fa-person-booth', 'home_page_def'),
(102, 'ENGLISH', 'Max 3 Phosti Letto', 'fas fa-bed', 'home_page_def'),
(103, 'ENGLISH', 'TV Satellitare', 'fas fa-tv', 'home_page_def'),
(104, 'ENGLISH', 'Area Condizionata', 'fab fa-accusoft', 'home_page_def'),
(126, 'ITALIAN', '20 - 25 mq', 'fas fa-person-booth', 'home_page_def'),
(127, 'ITALIAN', 'Max 3 Phosti Letto', 'fas fa-bed', 'home_page_def'),
(128, 'ITALIAN', 'TV Satellitare', 'fas fa-tv', 'home_page_def'),
(129, 'ITALIAN', 'Area Condizionata', 'fab fa-accusoft', 'home_page_def');

-- --------------------------------------------------------

--
-- Table structure for table `living_hotel_banaco_room`
--

CREATE TABLE `living_hotel_banaco_room` (
  `lhbr_id` int NOT NULL,
  `lhbr_language` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lhbr_heading` varchar(255) DEFAULT NULL,
  `lhbr_description` text,
  `lhbr_image` varchar(255) DEFAULT NULL,
  `lhbr_guest_number` int NOT NULL,
  `lhbr_page_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `living_hotel_banaco_room`
--

INSERT INTO `living_hotel_banaco_room` (`lhbr_id`, `lhbr_language`, `lhbr_heading`, `lhbr_description`, `lhbr_image`, `lhbr_guest_number`, `lhbr_page_name`) VALUES
(8, 'ITALIAN', 'VIVERE HOTEL BENACO  asdfsadfasdfsa', '<p>Le Camere</p>\r\n', NULL, 3, 'home_page_def');

-- --------------------------------------------------------

--
-- Table structure for table `size_guide`
--

CREATE TABLE `size_guide` (
  `sg_id` int NOT NULL,
  `sg_total_size` varchar(255) NOT NULL,
  `sg_cat_id` int NOT NULL,
  `sg_name_key` varchar(255) NOT NULL,
  `sg_item` varchar(255) NOT NULL,
  `sg_flat_size` varchar(255) NOT NULL,
  `sg_date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `size_guide`
--

INSERT INTO `size_guide` (`sg_id`, `sg_total_size`, `sg_cat_id`, `sg_name_key`, `sg_item`, `sg_flat_size`, `sg_date_created`) VALUES
(62, '51 to 68', 2, '', 'Mattress, sofa, kitchen table, bicycles, small boxes and other similar items.', 'Suitable for small one-bedroom flat', '2021-08-11 13:29:02'),
(63, '51 to 70', 18, '', 'Mattress, sofa, kitchen table, bicycles, small boxes and other similar items.', 'Suitable', '2021-08-11 13:29:17'),
(64, '51 to 75', 2, '', 'Mattress, sofa, kitchen table, bicycles, small boxes and other similar items.', 'Suitable for small one-bedroom flat', '2021-08-11 14:56:28'),
(65, '51 to 80', 1, '', 'Mattress, sofa, kitchen table, bicycles, small boxes and other similar items.', 'Suitable for small one-bedroom flat', '2021-08-11 15:17:41'),
(66, '51 to 88', 2, '', 'Mattress, sofa, kitchen table, bicycles, small boxes and other similar items.', 'Suitable for small one-bedroom flat', '2021-08-11 15:18:40');

-- --------------------------------------------------------

--
-- Table structure for table `size_guide_location`
--

CREATE TABLE `size_guide_location` (
  `sgl_id` int NOT NULL,
  `sgl_name` varchar(255) NOT NULL,
  `sgl_name_key` varchar(255) NOT NULL,
  `sgl_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `size_guide_location`
--

INSERT INTO `size_guide_location` (`sgl_id`, `sgl_name`, `sgl_name_key`, `sgl_date`) VALUES
(1, 'Noida', 'noida', '2021-07-28 17:56:33'),
(2, 'Gurugram', 'gurugram', '2021-07-28 17:56:33'),
(18, 'DELHI', 'delhi', '2021-08-11 12:13:43'),
(19, 'MUMBAI', 'mumbai', '2021-08-11 15:17:47'),
(20, 'GAO', 'gao', '2021-08-13 17:12:46');

-- --------------------------------------------------------

--
-- Table structure for table `slider_images`
--

CREATE TABLE `slider_images` (
  `si_id` int NOT NULL,
  `si_image` varchar(255) NOT NULL,
  `si_image_page` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `slider_images`
--

INSERT INTO `slider_images` (`si_id`, `si_image`, `si_image_page`) VALUES
(41, 'uploads/slider_images/file_612873b30b6001630041011.jpg', '1'),
(42, 'uploads/slider_images/file_6128eaa650e291630071462.jpg', '1'),
(44, 'uploads/slider_images/file_61287462300021630041186.jpg', '2'),
(45, 'uploads/slider_images/file_612874d2b10361630041298.jpg', '2'),
(46, 'uploads/slider_images/file_612874d2ca67c1630041298.jpg', '2'),
(47, 'uploads/slider_images/file_612874d2e0de11630041298.jpg', '2'),
(48, 'uploads/slider_images/file_612874d312d0a1630041299.jpg', '2'),
(49, 'uploads/slider_images/file_612874d335baa1630041299.jpg', '2'),
(50, 'uploads/slider_images/file_612874d34c6f71630041299.jpg', '2'),
(51, 'uploads/slider_images/file_612874d3603641630041299.jpg', '2'),
(52, 'uploads/slider_images/file_61288aca2d9411630046922.jpg', '3'),
(53, 'uploads/slider_images/file_61288aca409f61630046922.jpg', '3'),
(54, 'uploads/slider_images/file_61288aca4ae081630046922.jpg', '3'),
(55, 'uploads/slider_images/file_61288ae94cc661630046953.jpg', '4'),
(56, 'uploads/slider_images/file_61288ae95e5aa1630046953.jpg', '4'),
(57, 'uploads/slider_images/file_61288ae96ef4e1630046953.jpg', '4'),
(58, 'uploads/slider_images/file_61288ae9793601630046953.jpg', '4'),
(63, 'uploads/slider_images/file_6129c668e886d1630127720.jpg', '5'),
(64, 'uploads/slider_images/file_6129c6691558d1630127721.jpg', '5'),
(65, 'uploads/slider_images/file_6129c669345ac1630127721.jpg', '5'),
(66, 'uploads/slider_images/file_6129c870ac66d1630128240.jpg', '5'),
(67, 'uploads/slider_images/file_6129c870c06c21630128240.jpg', '5'),
(68, 'uploads/slider_images/file_6129c870d2bbe1630128240.jpg', '5'),
(69, 'uploads/slider_images/file_6129c8f7d22d71630128375.jpg', '5'),
(70, 'uploads/slider_images/file_6129c8f7ec0ed1630128375.jpg', '5'),
(71, 'uploads/slider_images/file_6129c8f81c0d61630128376.jpg', '5'),
(72, 'uploads/slider_images/file_6129d09625f2e1630130326.jpg', '5'),
(73, 'uploads/slider_images/file_6129d096512a01630130326.jpg', '5'),
(74, 'uploads/slider_images/file_6129d09679b1a1630130326.jpg', '5'),
(75, 'uploads/slider_images/file_6129d0d20788f1630130386.jpg', '5'),
(76, 'uploads/slider_images/file_6129d0d22031c1630130386.jpg', '5'),
(77, 'uploads/slider_images/file_6129d0d23fb0c1630130386.jpg', '5'),
(78, 'uploads/slider_images/file_6129d10adcf9c1630130442.jpg', '3'),
(79, 'uploads/slider_images/file_6129d10aedd281630130442.jpg', '3'),
(80, 'uploads/slider_images/file_6129d4244f3911630131236.jpg', '3'),
(81, 'uploads/slider_images/file_6129d424653261630131236.jpg', '3'),
(82, 'uploads/slider_images/file_613057f7aaa3b1630558199.jpg', '6'),
(83, 'uploads/slider_images/file_613057f7c2a571630558199.jpg', '6');

-- --------------------------------------------------------

--
-- Table structure for table `slider_images_page`
--

CREATE TABLE `slider_images_page` (
  `sip_id` int NOT NULL,
  `sip_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `slider_images_page`
--

INSERT INTO `slider_images_page` (`sip_id`, `sip_name`) VALUES
(1, 'welcome_to_hotel_banaco'),
(2, 'experience'),
(3, 'pool_and_garden1'),
(4, 'pool_and_garden2'),
(5, 'pool_and_garden3'),
(6, 'double_room');

-- --------------------------------------------------------

--
-- Table structure for table `storage_calculator`
--

CREATE TABLE `storage_calculator` (
  `sc_id` int NOT NULL,
  `sc_name` varchar(255) NOT NULL,
  `sc_cat_id` int NOT NULL,
  `sc_name_key` varchar(255) NOT NULL,
  `sc_name_price` varchar(255) NOT NULL,
  `sc_image` varchar(255) NOT NULL,
  `sc_date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `storage_calculator`
--

INSERT INTO `storage_calculator` (`sc_id`, `sc_name`, `sc_cat_id`, `sc_name_key`, `sc_name_price`, `sc_image`, `sc_date_created`) VALUES
(4, 'Side Table', 1, 'side_table', '0.5', '', '2021-07-29 12:23:01'),
(5, 'Single bed', 1, 'single_bed', '7.5', '', '2021-07-29 16:12:09'),
(6, 'Queen size bed', 1, 'queen_size_bed', '12.5', '', '2021-07-29 16:12:09'),
(7, 'King Size Bed', 1, 'king_size_bed', '15', '', '2021-07-29 16:12:09'),
(9, 'Chest of drawers (3)', 1, 'chest_of_drawers_3', '2.5', '', '2021-07-29 16:12:09'),
(10, 'Almirah', 1, 'almirah', '5', '', '2021-07-29 16:12:09'),
(11, 'Almirah Big', 1, 'almirah_big', '4.5', '', '2021-07-29 16:12:09'),
(12, 'Dressing Table', 1, 'dressing_table', '3.75', '', '2021-07-29 16:12:09'),
(13, 'Plastic Chairs', 2, 'plastic_chairs', '0.50', '', '2021-07-29 16:26:09'),
(14, 'Plastic Stools', 2, 'plastic_stools', '0.50', '', '2021-07-29 16:26:09'),
(15, 'Center Table', 2, 'center_table', '2.5', '', '2021-07-29 16:26:09'),
(16, 'TV', 2, 'tv', '1.875', '', '2021-07-29 16:26:09'),
(17, 'Tv unit Wooden', 2, 'tv_unit_wooden', '2.5', '', '2021-07-29 16:26:09'),
(18, 'Gas Stove', 3, 'gas_stove', '0.75', '', '2021-07-29 17:25:36'),
(19, 'Water Purifier/Dispenser', 3, 'water_purifier_dispenser', '1.125', '', '2021-07-29 17:25:36'),
(20, 'Mixture', 3, 'mixture', '0.25', '', '2021-07-29 17:25:36'),
(21, 'OTG small size', 3, 'otg_small_size', '0.625', '', '2021-07-29 17:25:36'),
(22, 'OTG big size', 3, 'otg_big_size', '1.25', '', '2021-07-29 17:25:36'),
(23, 'Microwave', 3, 'microwave', '0.375', '', '2021-07-29 17:25:36'),
(24, 'Double Door Fridge', 3, 'double_door_fridge', '3.5', '', '2021-07-29 17:25:36'),
(25, 'Small Box (1x1x1ft)', 4, 'small_box_1x1x1ft', '0.125', '', '2021-07-29 17:43:09'),
(26, 'Medium Box (1×1.5x1ft)', 4, 'medium_box_1151ft', '0.1875', '', '2021-07-29 17:43:09'),
(27, 'Large Box (1.5x2x1.5ft)', 4, 'large_box_15215ft', '0.5625', '', '2021-07-29 17:43:09'),
(28, 'Extra Large Box (2x3x2ft)', 4, 'extra_large_box_232ft', '1.5', '', '2021-07-29 17:45:21'),
(29, 'Filing Cabinet', 5, 'filing_cabinet', '4', '', '2021-07-29 17:51:57'),
(30, 'Office chair', 5, 'office_chair', '3.5', '', '2021-07-29 17:51:57'),
(31, 'Study Lamp', 5, 'study_lamp', '0.25', '', '2021-07-29 17:54:28'),
(32, 'Study Table', 5, 'study_table', '3.75', '', '2021-07-29 17:54:28'),
(33, 'Camp/Tent', 6, 'camp_tent', '2.5', '', '2021-07-29 18:00:15'),
(34, 'Porch Swing', 6, 'porch_swing', '50', '', '2021-07-29 18:00:15'),
(35, '2 Wheeler', 7, '2_wheeler', '50', '', '2021-07-29 18:06:18'),
(36, 'Bicycle', 7, 'bicycle', '3.75', '', '2021-07-29 18:06:18'),
(37, 'Air purifier', 8, 'air_purifier', '0.625', '', '2021-07-29 18:16:31'),
(38, 'Room heater', 8, 'room_heater', '0.625', '', '2021-07-29 18:16:31'),
(39, 'Vacuum Cleaner', 8, 'vacuum_cleaner', '1.25', '', '2021-07-29 18:16:31'),
(45, 'Storage calculator 4', 15, 'storage_calculator_4', '50', '', '2021-08-02 12:14:00'),
(47, 'Storage Calculator 11', 15, 'storage_calculator_11', '15', '', '2021-08-02 17:15:40'),
(48, 'Storage Calculator 11', 15, 'storage_calculator_11', '15', '', '2021-08-02 17:15:53'),
(49, 'Company Profile 1', 15, 'company_profile_1', '15', '', '2021-08-02 17:20:52'),
(50, 'Storage calculator 1', 15, 'storage_calculator_1', '15', '', '2021-08-02 17:23:33'),
(55, 'Mattress (Single Bed)', 1, 'mattress__single_bed_', '1.25', 'uploads/storage_calculator/file_6108c5ff7f63e1627964927.png', '2021-08-03 09:58:47'),
(56, 'Mattress (Queen Bed)', 1, 'mattress__queen_bed_', '1', 'uploads/storage_calculator/file_6108c6bb254331627965115.png', '2021-08-03 10:01:54'),
(57, 'Mattress (King Bed)', 1, 'mattress__king_bed_', '15', 'uploads/storage_calculator/file_6108c6df6fcee1627965151.png', '2021-08-03 10:02:31'),
(58, 'Almirah chest', 1, 'almirah_chest', '5', 'uploads/storage_calculator/file_6108c74cba83c1627965260.png', '2021-08-03 10:04:20'),
(59, 'Product 1', 15, 'product_1', '1.50', '', '2021-08-03 18:42:24'),
(60, 'Product 1', 15, 'product_1', '1.50', 'uploads/storage_calculator/file_610940d1189a41627996369.png', '2021-08-03 18:42:49');

-- --------------------------------------------------------

--
-- Table structure for table `storage_calculator_categories`
--

CREATE TABLE `storage_calculator_categories` (
  `scc_id` int NOT NULL,
  `scc_name` varchar(255) NOT NULL,
  `scc_name_key` varchar(255) NOT NULL,
  `scc_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `storage_calculator_categories`
--

INSERT INTO `storage_calculator_categories` (`scc_id`, `scc_name`, `scc_name_key`, `scc_date`) VALUES
(1, 'BEDROOM', 'bedroom', '2021-07-28 17:56:33'),
(2, 'LIVING ROOM', 'living-room', '2021-07-28 17:56:33'),
(3, 'KITCHEN/DINING', 'kitchen-dining', '2021-07-28 17:56:33'),
(4, 'BOXES', 'boxes', '2021-07-28 17:56:33'),
(5, 'OFFICE', 'office', '2021-07-28 17:56:33'),
(6, 'OUTDOOR', 'outdoor', '2021-07-28 17:56:33'),
(7, 'VEHICLE', 'vehicle', '2021-07-28 17:56:33'),
(8, 'MISCELLANEOUS', 'miscellaneous', '2021-07-28 17:56:33'),
(15, 'STORAGE CATEGORY 1', 'storage-category-1', '2021-08-02 10:34:51');

-- --------------------------------------------------------

--
-- Table structure for table `user_contact_details`
--

CREATE TABLE `user_contact_details` (
  `ucd_id` int NOT NULL,
  `ucd_name` varchar(255) DEFAULT NULL,
  `ucd_storage` varchar(255) DEFAULT NULL,
  `ucd_number` varchar(255) DEFAULT NULL,
  `ucd_email` varchar(255) NOT NULL,
  `ucd_storage_list` varchar(255) NOT NULL,
  `ucd_created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `user_contact_details`
--

INSERT INTO `user_contact_details` (`ucd_id`, `ucd_name`, `ucd_storage`, `ucd_number`, `ucd_email`, `ucd_storage_list`, `ucd_created_date`) VALUES
(2, 'Puhupwas', 'option2', '196149894', 'puhupwas@gmail@mailG.com', 'asdf', '2021-08-10 16:56:43'),
(3, 'puhupwas', 'option2', '8510062896', 'puhupwasbind301@lfjdslf', 'lsjdflsflsdafsfs', '2021-08-10 18:08:18'),
(4, 'puhupwas', 'option2', '8510062896', 'puhupwasbind301@lfjdslf', 'lsjdflsflsdafsfs', '2021-08-10 18:10:20'),
(5, 'Puhupwas', 'option2', '196149894', 'puhupwas@gmail@mailG.com', 'sadffsadfasdf sfsdfsdf', '2021-08-10 18:11:01'),
(6, 'asdfjlsfjsl', 'option2', '8516166446', 'puhupwasdeveloper@gmail.com', 'aslfsdlfjsdflsdafsdfs', '2021-08-10 18:22:13'),
(7, 'Puhupwas', 'option2', '196149894', 'puhupwas@gmail@mailG.com', 'asdfsdafsa', '2021-08-10 18:27:38'),
(8, 'phupwasffljl', 'option2', '9465164646', 'puhupwas@gmail@mailG.com', 'slfjsadlf lsjdflsdaslj lsdf', '2021-08-10 18:29:18'),
(9, 'phupwasffljl', 'option2', '9465164646', 'puhupwas@gmail@mailG.com', 'slfjsadlf lsjdflsdaslj lsdf', '2021-08-10 18:37:06'),
(10, 'phupwasffljl', 'option2', '9465164646', 'puhupwas@gmail@mailG.com', 'slfjsadlf lsjdflsdaslj lsdf', '2021-08-10 18:37:48'),
(11, 'phupwasffljl', 'option2', '9465164646', 'puhupwas@gmail@mailG.com', 'slfjsadlf lsjdflsdaslj lsdf', '2021-08-10 18:39:04'),
(12, 'phupwasffljl', 'option2', '9465164646', 'puhupwas@gmail@mailG.com', 'slfjsadlf lsjdflsdaslj lsdf', '2021-08-10 18:50:10'),
(13, 'phupwasffljl', 'option2', '9465164646', 'puhupwas@gmail@mailG.com', 'asdfsd sfsdds sadfdfsdfas', '2021-08-10 18:50:19'),
(14, 'puhupwas', 'option2', '9465164646', 'puhupwasdeveloper@gmail.com', 'asdfsd sfsdds sadfdfsdfas', '2021-08-10 18:51:16'),
(15, 'puhupwas', 'option2', '9465164646', 'puhupwasdeveloper@gmail.com', 'asdfsd sfsdds sadfdfsdfas', '2021-08-10 18:53:41'),
(16, 'Puhupwas', 'option2', '9213791237912', 'puhupwasdeveloper@gmail.com', 'let us store list', '2021-08-10 18:54:42'),
(17, 'Puhupwas', 'option2', '196149894', 'puhupwasdeveloper@gmail.com', 'asdfsda sfsdfsdfsd', '2021-08-10 18:56:13'),
(18, 'Puhupwas', 'option2', '196149894', 'puhupwasdeveloper@gmail.com', 'asdfsda sfsdfsdfsd', '2021-08-10 18:59:19'),
(19, 'Puhupwas', 'option2', '196149894', 'puhupwasdeveloper@gmail.com', 'asdfsda sfsdfsdfsd', '2021-08-10 19:04:50'),
(20, 'Puhupwas', 'option2', '496465465', 'puhupwasdeveloper@gmail.com', 'lsdjdfls lsadfljsdlfjsdfsdfsd ', '2021-08-10 19:05:10'),
(21, 'Puhupwas', 'option2', '196149894', 'puhupwasdeveloper@gmail.com', 'sladfj saldfjlsdsa sadlfjls jsafsd dfs', '2021-08-10 19:05:53'),
(22, 'Puhupwas', 'option2', '196149894', 'puhupwasdeveloper@gmail.com', 'sladfj saldfjlsdsa sadlfjls jsafsd dfs', '2021-08-10 19:09:53'),
(23, 'let us store', 'option2', '196149894', 'puhupwasdeveloper@gmail.com', 'sladfj saldfjlsdsa sadlfjls jsafsd dfs', '2021-08-10 19:10:25'),
(24, 'Puhupwas', 'option1', '196149894', 'puhupwasdeveloper@gmail.com', 'Storage 1 , Storage 2, Storage 3', '2021-08-10 19:11:52'),
(25, 'Puhupwas', 'option1', '85131321466', 'puhupwasdeveloper@gmail.com', 'sdljf asldfjl slfjsdlf ', '2021-08-10 19:32:04'),
(26, 'Puhupwas', 'option1', '196149894', 'puhupwasdeveloper@gmail.com', 'sldjff ssalffjsl aslffjdslf lfjdsl sfds', '2021-08-11 11:24:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_need_information`
--

CREATE TABLE `user_need_information` (
  `uni_id` int NOT NULL,
  `uni_name` varchar(255) DEFAULT NULL,
  `uni_email` varchar(255) DEFAULT NULL,
  `uni_object` varchar(255) NOT NULL,
  `uni_message` text NOT NULL,
  `uni_created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `user_need_information`
--

INSERT INTO `user_need_information` (`uni_id`, `uni_name`, `uni_email`, `uni_object`, `uni_message`, `uni_created_date`) VALUES
(29, 'Puhupwas', 'puhupwasbind301@gmail.com', 'sfsldfjlsdfjsldlskfjl', 'lsajlfslfsjlfslsadfdsfsdff', '2021-08-23 14:36:33'),
(30, 'Puhupwas', 'fljsadlfj@sdfjsdlajf', 'lsjdlfjs', 'salfjsdklfs', '2021-08-23 14:36:43'),
(31, 'Puhupwas', 'fljsadlfj@sdfjsdlajf', 'lsjdlfjs', 'salfjsdklfssdfasdfsda', '2021-08-23 14:36:55'),
(32, 'puhupwas', 'sdfsdaf@dafsdf', 'sdlfjsladfjklasdf', 'sadfsdafasfsdfasdf', '2021-08-23 14:37:44'),
(33, 'Neeraj', 'Sharma@aslfjlf', 'asjlfjsldkafjsl', 'aslfjlsdfjldskfjlsdfjsd', '2021-08-23 14:38:25'),
(35, 'need infor name22222222', 'need@infogmai.coansndfljdl222222222', 'object lsjdflsdafjlasdfj;22', 'aslfjjsdlakjsl;adkfjsl;aj2222222222', '2021-08-31 18:58:57'),
(36, 'Servono', 'Indrizolo', 'oggetto', 'messagio', '2021-08-31 19:11:57');

-- --------------------------------------------------------

--
-- Table structure for table `user_square_feet_detail`
--

CREATE TABLE `user_square_feet_detail` (
  `usfc_id` int NOT NULL,
  `usfc_username` varchar(255) NOT NULL,
  `usfc_email` varchar(255) NOT NULL,
  `usfc_mobile_number` varchar(255) NOT NULL,
  `usfc_square_feet` varchar(255) NOT NULL,
  `usfc_user_cal_detail` varchar(255) NOT NULL,
  `usfc_created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `user_square_feet_detail`
--

INSERT INTO `user_square_feet_detail` (`usfc_id`, `usfc_username`, `usfc_email`, `usfc_mobile_number`, `usfc_square_feet`, `usfc_user_cal_detail`, `usfc_created_date`) VALUES
(9, 'puhupwas', 'opsufkls@LJFLDSFS', '4646549654', '112.75-132.75', '[{\"calName\":\"Mattress (Single Bed)\",\"sqFeet\":\"3\"},{\"calName\":\"Mattress (Queen Bed)\",\"sqFeet\":\"4\"},{\"calName\":\"Mattress (King Bed)\",\"sqFeet\":\"7\"}]', '2021-08-13 11:03:59'),
(10, 'Dheeraj', 'puhupwas@sdf', '98484646163', '96-116', '[{\"calName\":\"Mattress (Queen Bed)\",\"sqFeet\":\"6\"},{\"calName\":\"Mattress (King Bed)\",\"sqFeet\":\"6\"}]', '2021-08-13 11:41:18'),
(11, 'Puhupwas', 'Puhupwas@adfsdf', '8161651631', '0-20', '[]', '2021-08-13 11:51:31'),
(12, 'sdsfsdf', 'Puhupwas@adfsdf', '1232', '9-29', '[{\"calName\":\"Mattress (Single Bed)\",\"sqFeet\":\"4\"},{\"calName\":\"Mattress (Queen Bed)\",\"sqFeet\":\"4\"}]', '2021-08-13 11:55:23'),
(13, 'sdafsdfds', 'sonu@gmail.com', '98666654654', '5.75-25.75', '[{\"calName\":\"Mattress (Single Bed)\",\"sqFeet\":\"3\"},{\"calName\":\"Mattress (Queen Bed)\",\"sqFeet\":\"2\"}]', '2021-08-13 11:57:12');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `vendor_id` int NOT NULL,
  `vendor_comp_name` varchar(255) DEFAULT NULL,
  `vendor_comp_phone` varchar(255) DEFAULT NULL,
  `vendor_comp_email` varchar(255) DEFAULT NULL,
  `vendor_comp_addr` varchar(255) DEFAULT NULL,
  `vendor_comp_country` varchar(255) DEFAULT NULL,
  `vendor_comp_state` varchar(255) DEFAULT NULL,
  `vendor_comp_city` varchar(255) DEFAULT NULL,
  `vendor_comp_pincode` varchar(255) DEFAULT NULL,
  `vendor_name` varchar(255) DEFAULT NULL,
  `vendor_phone` varchar(255) DEFAULT NULL,
  `vendor_addr` varchar(255) DEFAULT NULL,
  `vendor_bank_name` varchar(255) DEFAULT NULL,
  `vendor_branch_name` varchar(255) DEFAULT NULL,
  `vendor_account_no` varchar(255) DEFAULT NULL,
  `vendor_ifsc_code` varchar(255) DEFAULT NULL,
  `vendor_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vend_purchase_order`
--

CREATE TABLE `vend_purchase_order` (
  `vpo_id` int NOT NULL,
  `vpo_vendor_id` int NOT NULL,
  `vpo_category` varchar(255) DEFAULT NULL,
  `vpo_date` date DEFAULT NULL,
  `vpo_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `admin_session_key` (`admin_session_key`),
  ADD UNIQUE KEY `admin_name` (`admin_name`);

--
-- Indexes for table `banner_image`
--
ALTER TABLE `banner_image`
  ADD PRIMARY KEY (`bi_id`);

--
-- Indexes for table `contact_detail`
--
ALTER TABLE `contact_detail`
  ADD PRIMARY KEY (`cd_id`);

--
-- Indexes for table `discover_other_room`
--
ALTER TABLE `discover_other_room`
  ADD PRIMARY KEY (`dor_id`);

--
-- Indexes for table `discover_other_room_facilities`
--
ALTER TABLE `discover_other_room_facilities`
  ADD PRIMARY KEY (`dorf_id`);

--
-- Indexes for table `double_room`
--
ALTER TABLE `double_room`
  ADD PRIMARY KEY (`dr_id`);

--
-- Indexes for table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `feature_and_service`
--
ALTER TABLE `feature_and_service`
  ADD PRIMARY KEY (`fas_id`);

--
-- Indexes for table `feature_and_service_detail`
--
ALTER TABLE `feature_and_service_detail`
  ADD PRIMARY KEY (`fasd_id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`home_id`);

--
-- Indexes for table `home_page_def`
--
ALTER TABLE `home_page_def`
  ADD PRIMARY KEY (`hpd_id`);

--
-- Indexes for table `living_hotel_banaco_facilities`
--
ALTER TABLE `living_hotel_banaco_facilities`
  ADD PRIMARY KEY (`lhb_id`);

--
-- Indexes for table `living_hotel_banaco_room`
--
ALTER TABLE `living_hotel_banaco_room`
  ADD PRIMARY KEY (`lhbr_id`);

--
-- Indexes for table `size_guide`
--
ALTER TABLE `size_guide`
  ADD PRIMARY KEY (`sg_id`);

--
-- Indexes for table `size_guide_location`
--
ALTER TABLE `size_guide_location`
  ADD PRIMARY KEY (`sgl_id`);

--
-- Indexes for table `slider_images`
--
ALTER TABLE `slider_images`
  ADD PRIMARY KEY (`si_id`);

--
-- Indexes for table `slider_images_page`
--
ALTER TABLE `slider_images_page`
  ADD PRIMARY KEY (`sip_id`);

--
-- Indexes for table `storage_calculator`
--
ALTER TABLE `storage_calculator`
  ADD PRIMARY KEY (`sc_id`);

--
-- Indexes for table `storage_calculator_categories`
--
ALTER TABLE `storage_calculator_categories`
  ADD PRIMARY KEY (`scc_id`);

--
-- Indexes for table `user_contact_details`
--
ALTER TABLE `user_contact_details`
  ADD PRIMARY KEY (`ucd_id`);

--
-- Indexes for table `user_need_information`
--
ALTER TABLE `user_need_information`
  ADD PRIMARY KEY (`uni_id`);

--
-- Indexes for table `user_square_feet_detail`
--
ALTER TABLE `user_square_feet_detail`
  ADD PRIMARY KEY (`usfc_id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`vendor_id`);

--
-- Indexes for table `vend_purchase_order`
--
ALTER TABLE `vend_purchase_order`
  ADD PRIMARY KEY (`vpo_id`),
  ADD KEY `vpo_vendor_id` (`vpo_vendor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banner_image`
--
ALTER TABLE `banner_image`
  MODIFY `bi_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `contact_detail`
--
ALTER TABLE `contact_detail`
  MODIFY `cd_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `discover_other_room`
--
ALTER TABLE `discover_other_room`
  MODIFY `dor_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `discover_other_room_facilities`
--
ALTER TABLE `discover_other_room_facilities`
  MODIFY `dorf_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `double_room`
--
ALTER TABLE `double_room`
  MODIFY `dr_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `experience`
--
ALTER TABLE `experience`
  MODIFY `e_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `feature_and_service`
--
ALTER TABLE `feature_and_service`
  MODIFY `fas_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `feature_and_service_detail`
--
ALTER TABLE `feature_and_service_detail`
  MODIFY `fasd_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `home_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `home_page_def`
--
ALTER TABLE `home_page_def`
  MODIFY `hpd_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `living_hotel_banaco_facilities`
--
ALTER TABLE `living_hotel_banaco_facilities`
  MODIFY `lhb_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `living_hotel_banaco_room`
--
ALTER TABLE `living_hotel_banaco_room`
  MODIFY `lhbr_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `size_guide`
--
ALTER TABLE `size_guide`
  MODIFY `sg_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `size_guide_location`
--
ALTER TABLE `size_guide_location`
  MODIFY `sgl_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `slider_images`
--
ALTER TABLE `slider_images`
  MODIFY `si_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `slider_images_page`
--
ALTER TABLE `slider_images_page`
  MODIFY `sip_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `storage_calculator`
--
ALTER TABLE `storage_calculator`
  MODIFY `sc_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `storage_calculator_categories`
--
ALTER TABLE `storage_calculator_categories`
  MODIFY `scc_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user_contact_details`
--
ALTER TABLE `user_contact_details`
  MODIFY `ucd_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user_need_information`
--
ALTER TABLE `user_need_information`
  MODIFY `uni_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `user_square_feet_detail`
--
ALTER TABLE `user_square_feet_detail`
  MODIFY `usfc_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `vendor_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vend_purchase_order`
--
ALTER TABLE `vend_purchase_order`
  MODIFY `vpo_id` int NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
