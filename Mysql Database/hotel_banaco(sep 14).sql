-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2021 at 02:17 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel_banaco`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_session_key` varchar(255) NOT NULL,
  `admin_remember_me_token` varchar(255) DEFAULT NULL,
  `admin_public_ip` varchar(255) DEFAULT NULL,
  `admin_lockscreen` tinyint(1) DEFAULT 0 COMMENT '0 - false, 1- true',
  `admin_img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_password`, `admin_email`, `admin_session_key`, `admin_remember_me_token`, `admin_public_ip`, `admin_lockscreen`, `admin_img`) VALUES
(1, 'admin', '$2y$10$EJ041.Jp4b9HyEGr4nLC.eTB86Mva0PDKgG72GNzysmBhom2955ZS', 'admin@gmail.com', 'iAmAuthAdmin', '15e068459ce7961efe0480fbaaadcf32debe1e3e', '::1', 0, 'http://localhost/portfolio/uploads/admin_image/profileImage07092019023233716.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `contact_detail`
--

CREATE TABLE `contact_detail` (
  `cd_id` int(11) NOT NULL,
  `cd_visit` varchar(255) NOT NULL,
  `cd_write_email` varchar(255) NOT NULL,
  `cd_contact_no` varchar(255) NOT NULL,
  `cd_image` varchar(100) NOT NULL,
  `cd_created_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_detail`
--

INSERT INTO `contact_detail` (`cd_id`, `cd_visit`, `cd_write_email`, `cd_contact_no`, `cd_image`, `cd_created_date`) VALUES
(87, '<p>Via Cavour, 30 25015 Decenzano d/G Bricia</p>\r\n', 'info@hotelbenaco.com', 't. +39 030 9141710', 'uploads/contact_detail/file_61238f11f18341629720337.jpg', '2021-08-23 17:35:37');

-- --------------------------------------------------------

--
-- Table structure for table `discover_other_room`
--

CREATE TABLE `discover_other_room` (
  `dor_id` int(11) NOT NULL,
  `dor_language` varchar(255) NOT NULL,
  `dor_heading` varchar(255) DEFAULT NULL,
  `dor_description` text DEFAULT NULL,
  `dor_image` varchar(255) DEFAULT NULL,
  `dor_guest_number` varchar(255) NOT NULL,
  `dor_page_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discover_other_room`
--

INSERT INTO `discover_other_room` (`dor_id`, `dor_language`, `dor_heading`, `dor_description`, `dor_image`, `dor_guest_number`, `dor_page_name`) VALUES
(12, 'ITALIAN', 'Scopri le altre camere', '', NULL, 'Spaziose Camere per 3 ospiti', 'other_room'),
(13, 'ENGLISH', 'Discover the other rooms', '', NULL, 'Spacious rooms for 3 guests', 'home_page_def');

-- --------------------------------------------------------

--
-- Table structure for table `discover_other_room_facilities`
--

CREATE TABLE `discover_other_room_facilities` (
  `dorf_id` int(11) NOT NULL,
  `dorf_language` varchar(255) NOT NULL,
  `dorf_room_facilities` varchar(255) DEFAULT NULL,
  `dorf_iconname` varchar(255) DEFAULT NULL,
  `dorf_page_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discover_other_room_facilities`
--

INSERT INTO `discover_other_room_facilities` (`dorf_id`, `dorf_language`, `dorf_room_facilities`, `dorf_iconname`, `dorf_page_name`) VALUES
(101, 'ENGLISH', '20 - 25 mq', 'fas fa-person-booth', 'home_page_def'),
(102, 'ENGLISH', 'Max 3 Phosti Bed', 'fas fa-bed', 'home_page_def'),
(103, 'ENGLISH', 'Satellite TV', 'fas fa-tv', 'home_page_def'),
(104, 'ENGLISH', 'Conditioned Area', 'fab fa-accusoft', 'home_page_def'),
(126, 'ITALIAN', '20 - 25 mq', 'fas fa-person-booth', 'home_page_def'),
(127, 'ITALIAN', 'Max 3 Phosti Letto', 'fas fa-bed', 'home_page_def'),
(128, 'ITALIAN', 'TV Satellitare', 'fas fa-tv', 'home_page_def'),
(129, 'ITALIAN', 'Area condizionata ', 'fab fa-accusoft', 'home_page_def');

-- --------------------------------------------------------

--
-- Table structure for table `double_room`
--

CREATE TABLE `double_room` (
  `dr_id` int(11) NOT NULL,
  `dr_language` varchar(255) NOT NULL,
  `dr_heading_title` varchar(255) NOT NULL,
  `dr_heading` varchar(255) NOT NULL,
  `dr_sub_heading` varchar(255) NOT NULL,
  `dr_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `double_room`
--

INSERT INTO `double_room` (`dr_id`, `dr_language`, `dr_heading_title`, `dr_heading`, `dr_sub_heading`, `dr_description`) VALUES
(6, 'ITALIAN', 'Vi presentiamo', 'Camera Doppia', 'CON O SENZA BALCONE', '<p>L&rsquo;eleganza di queste camere non delude i clienti pi&ugrave; esigenti. Sar&agrave; la vostra migliore soluzione per alloggiare a Desenzano a tariffe vantaggiose senza rinunciare ad un ottimo servizio. Le nostre camere doppie sono arredate con gust'),
(10, 'ENGLISH', 'We present you', 'Double room', 'CON O SENZA BALCONE', 'L’eleganza di queste camere non delude i clienti più esigenti. Sarà la vostra migliore soluzione per alloggiare a Desenzano a tariffe vantaggiose senza rinunciare ad un ottimo servizio. Le nostre camere doppie sono arredate con gust');

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `e_id` int(11) NOT NULL,
  `e_language` varchar(255) NOT NULL,
  `e_heading` varchar(255) NOT NULL,
  `e_description` text NOT NULL,
  `e_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`e_id`, `e_language`, `e_heading`, `e_description`, `e_image`) VALUES
(7, 'ENGLISH', 'EXPERIENCE', '<p>The building, now completely modernized, has elegant and comfortable rooms and preserves the ancient charm of the elegant manor houses that lived through the Bella Epoque season.</p>\r\n\r\n<p>The hotel is surrounded by Art Nouveau villas and is surrounded by a large garden guarding the last of the shady horse chestnuts that once adorned the avenue that leads from the train station to Lake Garda. The private parking, the green garden surrounding the swimming pool and the solarium make our hotel ideal for a business trip or a relaxing holiday.</p>\r\n', 'uploads/home_page_def/file_612720510b3251629954129.jpg'),
(8, 'ITALIAN', 'ESPERIENZA', '<p>L&rsquo;edificio, oggi completamente rimodernato dispone di camere eleganti e confortevoli e conserva intatto l&rsquo;antico fascino delle eleganti ville padronali che hanno vissuto la stagione della Bella EpoqL&rsquo;hotel &egrave; circondato da ville liberty ed &egrave; immerso in un grande giardino custode dell&rsquo;ultimo degli ombrosi ippocastani che un tempo ornavano il viale che dalla stazione ferroviaria porta al Lago di Garda. Il parcheggio privato, il verdeggiante giardino che circonda la piscina e il solarium rendono il nostro hotel ideale per un soggiorno di lavoro o per una vacanza all&rsquo;insegna del relax.ue.</p>\r\n\r\n<p>L&rsquo;hotel &egrave; circondato da ville liberty ed &egrave; immerso in un grande giardino custode dell&rsquo;ultimo degli ombrosi ippocastani che un tempo ornavano il viale che dalla stazione ferroviaria porta al Lago di Garda. Il parcheggio privato, il verdeggiante giardino che circonda la piscina e il solarium rendono il nostro hotel ideale per un soggiorno di lavoro o per una vacanza all&rsquo;insegna del relax.</p>\r\n', 'uploads/home_page_def/file_612720eadf5e21629954282.jpg'),
(9, 'ENGLISH', 'EXPERIENCE', '<p>The building, now completely modernized, has elegant and comfortable rooms and preserves the ancient charm of the elegant manor houses that lived through the Bella Epoque season.</p>\r\n\r\n<p>The hotel is surrounded by Art Nouveau villas and is surrounded by a large garden guarding the last of the shady horse chestnuts that once adorned the avenue that leads from the train station to Lake Garda. The private parking, the green garden surrounding the swimming pool and the solarium make our hotel ideal for a business trip or a relaxing holiday.</p>\r\n', 'uploads/home_page_def/file_613ca9c40b76b1631365572.jpg'),
(10, 'ITALIAN', 'ESPERIENZA', '<p>The building, now completely modernized, has elegant and comfortable rooms and preserves the ancient charm of the elegant manor houses that lived through the Bella Epoque season.</p>\r\n\r\n<p>The hotel is surrounded by Art Nouveau villas and is surrounded by a large garden guarding the last of the shady horse chestnuts that once adorned the avenue that leads from the train station to Lake Garda. The private parking, the green garden surrounding the swimming pool and the solarium make our hotel ideal for a business trip or a relaxing holiday.</p>\r\n', 'uploads/home_page_def/file_613caae5ddfef1631365861.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `feature_and_service`
--

CREATE TABLE `feature_and_service` (
  `fas_id` int(11) NOT NULL,
  `fas_language` varchar(255) NOT NULL,
  `fas_feature` varchar(255) NOT NULL,
  `fas_service` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feature_and_service`
--

INSERT INTO `feature_and_service` (`fas_id`, `fas_language`, `fas_feature`, `fas_service`) VALUES
(1, 'ENGLISH', 'Feature', 'Service'),
(2, 'ITALIAN', 'Caratteristiche', 'Servizi');

-- --------------------------------------------------------

--
-- Table structure for table `feature_and_service_detail`
--

CREATE TABLE `feature_and_service_detail` (
  `fasd_id` int(11) NOT NULL,
  `fasd_language` varchar(255) NOT NULL,
  `fasd_feature` varchar(255) DEFAULT NULL,
  `fasd_iconname` varchar(255) DEFAULT NULL,
  `fasd_page_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feature_and_service_detail`
--

INSERT INTO `feature_and_service_detail` (`fasd_id`, `fasd_language`, `fasd_feature`, `fasd_iconname`, `fasd_page_name`) VALUES
(154, 'ITALIAN', '15 - 20 mq', 'fas fa-meteor', 'FEATURE'),
(155, 'ITALIAN', 'Max 2 posti letto', 'fas fa-bed', 'FEATURE'),
(156, 'ITALIAN', 'Bagno', 'fab fa-bandcamp', 'FEATURE'),
(157, 'ITALIAN', 'Con o senza Balcone', 'fas fa-home', 'FEATURE'),
(158, 'ENGLISH', '15 - 20 mq', 'fas fa-meteor', 'FEATURE'),
(159, 'ENGLISH', 'Max 2 beds', 'fas fa-bed', 'FEATURE'),
(160, 'ENGLISH', 'Bagno', 'fab fa-bandcamp', 'FEATURE'),
(161, 'ENGLISH', 'With or without balcony', 'fas fa-home', 'FEATURE'),
(162, 'ITALIAN', 'TV lcd satellitare', 'fab fa-acquisitions-incorporated', 'SERVICE'),
(163, 'ITALIAN', 'SKI TV', 'fas fa-tv', 'SERVICE'),
(164, 'ITALIAN', 'WI Fi gratuito', 'fas fa-wifi', 'SERVICE'),
(165, 'ITALIAN', 'Asciugacapelli', 'fab fa-accusoft', 'SERVICE'),
(166, 'ITALIAN', 'Cassaforte', 'far fa-compass', 'SERVICE'),
(167, 'ITALIAN', 'Insonorizzazione', 'fas fa-microphone-alt-slash', 'SERVICE'),
(168, 'ITALIAN', 'Aria condizionata', 'fas fa-wind', 'SERVICE'),
(169, 'ENGLISH', 'LCD satellite TV', 'fab fa-acquisitions-incorporated', 'SERVICE'),
(170, 'ENGLISH', 'SKI TV', 'fas fa-tv', 'SERVICE'),
(171, 'ENGLISH', 'Free WI Fi', 'fas fa-wifi', 'SERVICE'),
(172, 'ENGLISH', 'Hairdryer', 'fab fa-accusoft', 'SERVICE'),
(173, 'ENGLISH', 'Safe', 'far fa-compass', 'SERVICE'),
(174, 'ENGLISH', 'Soundproofing', 'fas fa-microphone-alt-slash', 'SERVICE'),
(175, 'ENGLISH', 'Air conditioning ', 'fas fa-wind', 'SERVICE');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `home_id` int(11) NOT NULL,
  `home_title_heading` varchar(255) DEFAULT NULL,
  `home_title` varchar(255) NOT NULL,
  `home_image` varchar(255) DEFAULT NULL,
  `home_link_name` varchar(255) DEFAULT NULL,
  `home_link` varchar(255) DEFAULT NULL,
  `home_desc` varchar(255) NOT NULL,
  `home_image_page` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`home_id`, `home_title_heading`, `home_title`, `home_image`, `home_link_name`, `home_link`, `home_desc`, `home_image_page`) VALUES
(24, NULL, 'asdfsda', 'uploads/home/file_611216c3c8b9e1628575427.jpg', NULL, NULL, 'sdafsdaf', 'meet-the-team'),
(25, ' ', 'Puhupwas', 'uploads/home/file_611b521aa30261629180442.png', ' ', ' ', 'sadfsadf', 'news-and-articles'),
(28, NULL, '', 'uploads/home/file_61121dfd1d85a1628577277.png', NULL, NULL, '', 'meet-the-team'),
(33, NULL, 'title1', 'uploads/home/file_61121e51519c51628577361.jpg', NULL, NULL, 'description 1', 'our-works'),
(34, NULL, 'title 2', 'uploads/home/file_61122641a85261628579393.jpg', NULL, NULL, 'asdfdsf', 'meet-the-team'),
(37, ' ', 'dgfdg', 'uploads/home/file_6112ba063464d1628617222.jpg', ' ', ' ', 'asdfadsf', 'our-works'),
(39, 'INTRODUCING WORKFLOW', 'INTRODUCING WORKFLOW India\'s Best Let Us Store Facility', 'uploads/home/file_6112baa4ddd8a1628617380.jpg', 'Contact Us', 'http://localhost/letusstore/home/contact-us', 'Access to adequate space, at home or office, is always an issue. Unused items tend to lie around and kill productivity. They also take up valuable space.', 'main-section'),
(40, 'ABOUT LET US STORE', 'The Thought Behind Let Us Store', 'uploads/home/file_6112bae8b6c121628617448.jpg', 'Benefits', 'http://localhost/letusstore/home/benefits', 'Our work and personal places are shrinking with each passing year and the clutter of our ever increasing “stuff” obstructs our ability to think clearly and enjoy the spaces in which we spend our time.\r\n\r\nwas created to solve the challenges of cluttering b', 'main-section'),
(41, 'Title main', 'title ', NULL, 'link1', 'google.com', 'This is the description of the  Titlemain', 'main-section'),
(42, 'Title maind', 'title ', 'uploads/home/file_61164121eb99e1628848417.png', 'link1', 'google.com', 'This is the description of the  Titlemain', 'main-section');

-- --------------------------------------------------------

--
-- Table structure for table `home_images_videos`
--

CREATE TABLE `home_images_videos` (
  `hiv_id` int(11) NOT NULL,
  `hiv_path` varchar(255) NOT NULL,
  `hiv_section` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `home_page`
--

CREATE TABLE `home_page` (
  `hp_id` int(11) NOT NULL,
  `hp_video_header_link` varchar(255) NOT NULL,
  `hp_hotel_benaco_heading` varchar(255) NOT NULL,
  `hp_intro_header_title` varchar(255) NOT NULL,
  `hp_intro_header` varchar(255) NOT NULL,
  `hp_intro_sub_header` varchar(255) NOT NULL,
  `hp_intro_description` text NOT NULL,
  `hp_home_experience` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `home_page_def`
--

CREATE TABLE `home_page_def` (
  `hpd_id` int(11) NOT NULL,
  `hpd_language` varchar(255) NOT NULL,
  `hpd_heading_title` varchar(255) NOT NULL,
  `hpd_heading` varchar(255) NOT NULL,
  `hpd_sub_heading` varchar(255) NOT NULL,
  `hpd_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `home_page_def`
--

INSERT INTO `home_page_def` (`hpd_id`, `hpd_language`, `hpd_heading_title`, `hpd_heading`, `hpd_sub_heading`, `hpd_description`) VALUES
(3, 'ITALIAN', 'Tutte le forme dell’accoglienza', 'Benvenuti al Benaco', 'NEL MAGNIFICO CONTESTO DEL LAGO DI GARDA', '<p>Nel cuore storico dell’isola pedonale di Desenzano, il nostro hotel vi accoglierà in un’atmosfera calda e familiare di una struttura 3 stelle che si affaccia sugli scorci più belli ed eleganti della città. La sua partic'),
(5, 'ENGLISH', 'All forms of hospitality', 'Welcome to the Benaco', 'IN THE MAGNIFICENT CONTEXT OF LAKE GARDA', '<p>In the historic heart of the pedestrian area of ​​Desenzano, our hotel will welcome you in the warm and familiar atmosphere of a 3-star hotel that overlooks the most beautiful and elegant views of the city. Its particular position is particularly appre');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_benaco_video`
--

CREATE TABLE `hotel_benaco_video` (
  `hbv_id` int(11) NOT NULL,
  `hbv_language` varchar(255) NOT NULL,
  `hbv_heading` varchar(255) NOT NULL,
  `hbv_video` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hotel_benaco_video`
--

INSERT INTO `hotel_benaco_video` (`hbv_id`, `hbv_language`, `hbv_heading`, `hbv_video`) VALUES
(29, 'ITALIAN', 'Le Emozioni Del Vostro Prossimo Viaggio Partono Da Qui.', 'uploads/hotel_benaco_video/file_613df1cbb80a81631449547.mp4'),
(30, 'ENGLISH', 'The emotions of your next trip start from here.', 'uploads/hotel_benaco_video/file_613df2e5b6eef1631449829.mp4');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_benaco_video_image_section`
--

CREATE TABLE `hotel_benaco_video_image_section` (
  `hbvis_id` int(11) NOT NULL,
  `hbvis_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hotel_benaco_video_image_section`
--

INSERT INTO `hotel_benaco_video_image_section` (`hbvis_id`, `hbvis_name`) VALUES
(1, 'room_video_section'),
(2, 'header_image_section'),
(3, 'double_room_slider_section'),
(4, 'hotel_benaco_video_section'),
(5, 'home_intro_images_section'),
(6, 'p_g_slider_images_section'),
(7, 'pool_and_garden_video_section'),
(8, 'p_g_footer_slider_images_section');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `lang_id` int(11) NOT NULL,
  `lang_key` varchar(255) NOT NULL,
  `lang_english` varchar(255) NOT NULL,
  `lang_italian` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`lang_id`, `lang_key`, `lang_english`, `lang_italian`) VALUES
(1, 'call_us', 'Call us', 'Chiamaci'),
(2, 'write_to_us', 'Write to us', 'Scrivici'),
(3, 'contact_detail', 'Contact Detail', 'Dettagli del contatto'),
(4, 'admin', 'Admin', 'Amministratrice'),
(5, 'dashboard', 'Dashboard', 'Pannello di controllo'),
(6, 'welcome_to_the_benaco', 'Welcome to the Benaco', 'Benvenuti al Benaco'),
(7, 'view', 'View', 'Visualizzazione'),
(8, 'slider_images', 'Slider Images', 'Immagini del dispositivo di scorrimento'),
(9, 'home', 'Home', 'Casa'),
(10, 'add_welcome_slider_image', 'Add Welcome Slider Image', 'Aggiungi immagine slider di benvenuto'),
(11, 'heading_title', 'Heading Title', 'Titolo intestazione'),
(12, 'heading', 'Heading', 'Intestazione'),
(13, 'sub_heading', 'Sub Heading', 'Sottotitolo'),
(14, 'submit', 'Submit', 'Invia'),
(15, 'choose_image', 'Choose Image', 'Scegli immagine'),
(16, 'description', 'Description', 'descrizione'),
(17, 'update', 'Update', 'Aggiornare'),
(18, 'delete', 'Delete', 'Elimina'),
(19, 'edit', 'Edit', 'Modificare'),
(20, 'manage', 'Manage', 'Maneggio'),
(21, 'language', 'Language', 'Lingua'),
(22, 'image', 'Image', 'Immagine'),
(23, 'name', 'Name', 'Nome'),
(24, 'email', 'Email', 'E-mail'),
(25, 'image_section', 'Image section', 'Sezione immagine'),
(26, 'section', 'Section', 'Sezione'),
(27, 'feature', 'Feature', 'Caratteristica'),
(28, 'service', 'Service', 'Servizio'),
(29, 'feature_and_service', 'Feature & Service', 'Funzionalità e servizio'),
(30, 'experience', 'Experience', 'Esperienza'),
(31, 'living_hotel_benaco', 'Living Hotel Benaco', 'Vivere Hotel Benaco'),
(32, 'pool_and_garden', 'Pool & Garden', 'Piscina e giardino'),
(33, 'need_information_detail', 'Need Information Detail', 'Hai bisogno di informazioni dettagliate'),
(35, 'rooms', 'Rooms', 'Camere'),
(36, 'double_room', 'Double Room', 'Camera doppia'),
(37, 'discover_the_other_rooms', 'Discover the other rooms', 'Scopri le altre stanze'),
(38, 'hotel_booking', 'Hotel Booking', 'Prenotazione d\'albergo'),
(39, 'booking', 'Booking', 'Prenotazione'),
(40, 'users', 'Users', 'Utenti'),
(41, 'setting', 'Setting', 'Collocamento'),
(42, 'lockscreen', 'Lockscreen', 'Blocca schermo'),
(43, 'logout', 'Logout', 'Disconnettersi'),
(44, 'italian', 'Italian', 'Italiana'),
(45, 'english', 'English', 'inglese'),
(46, 'banner_image', 'Banner Image', 'Immagine banner'),
(47, 'add', 'Add', 'Aggiungi '),
(48, 'are_you_sure_you_want_to_delete', 'Are you sure, you want to delete this?', 'Sei sicuro di voler cancellare questo?'),
(49, 'close', 'Close', 'Chiudere'),
(50, 'yes', 'Yes', 'sì'),
(51, 'detail', 'Detail', 'Dettaglio'),
(52, 'mobile', 'Mobile', 'Mobile'),
(53, 'welcome_to_hotel_benaco_detail', 'Welcome to hotel benaco detail', 'Benvenuto all\'hotel benaco dettaglio'),
(54, 'spacious_room_detail', 'Spacious Room Detail', 'Particolare della camera spaziosa'),
(55, 'add_room_facilites', 'Add Room Facilites', 'Aggiungi servizi in camera'),
(56, 'room_facilities', 'Room Facilities', 'Servizi in camera'),
(57, 'spacious_rooms_guests', 'Spacious Rooms Guests', 'Camere spaziose Ospiti'),
(58, 'add_experience_slider_image', 'Add Experience Slider Image', 'Aggiungi immagine slider esperienza'),
(59, 'add_more', 'Add more', 'Aggiungere altro'),
(60, 'lockscreen', 'Lockscreen', 'Blocca schermo'),
(61, 'password', 'password', 'parola d\'ordine'),
(62, 'enter_your_password_to_retrieve_your_session', 'Enter your password to retrieve your session', 'Inserisci la tua password per recuperare la tua sessione'),
(63, 'or_sign_in_as_a_different_user', 'Or sign in as a different user', 'Oppure accedi come un altro utente'),
(64, 'login', 'Login', 'Accedere'),
(65, 'backend_web', 'Backend Web', 'Web di backend'),
(66, 'hotel_benaco', 'Hotel Benaco', 'Hotel Benaco'),
(67, 'sign_in', 'Sign In', 'Registrazione'),
(68, 'user', 'User', 'Utente'),
(69, 'come_in', 'come in', 'entra'),
(70, 'forgot', 'Forgot', 'Dimenticato'),
(71, 'reset', 'Reset', 'Ripristina'),
(72, 'hotel_benaco_backend_administration_v', 'Hotel Benaco Backend Administration v', 'Hotel Benaco Backend Amministrazione '),
(73, 'support_magma_studio', 'Support Magma Studio', 'Sostieni Magma Studio'),
(74, 'enter_your_password_reset_email', 'Enter your password reset email', 'Inserisci la tua email per il reset password'),
(75, 'other_room_facilities', 'Other Room Facilities', 'Altri servizi in camera'),
(76, 'other_room_facilities_detail', 'Other Room Facilities Detail', 'Dettaglio altri servizi in camera'),
(77, 'icon', 'Icon', 'Icona'),
(78, 'choose_icon', 'Choose Icon', 'Scegli icona'),
(80, 'discover_other_room', 'Discover Other Room', 'Scopri l\'altra stanza'),
(81, 'discover_other_room_detail', 'Discover Other Room Detail', 'Scopri altri dettagli della camera'),
(82, 'pool_and_garden', 'Pool & Garden', 'Piscina e giardino'),
(83, 'slider', 'Slider', 'Cursore'),
(84, 'action', 'Action', 'Azione'),
(85, 'add_pool_and_garden_slider', 'Add Pool and Garden Slider', 'Aggiungi il cursore per piscina e giardino'),
(86, 'backend_web', 'Backend Web', 'Web di backend'),
(87, 'living_hotel_benaco_detail', 'Living Hotel  Benaco Detail', 'Living Hotel Benaco Dettaglio'),
(88, 'search_icon', 'Search Icon', 'Icona di ricerca'),
(89, 'room_feature', 'Room Feature', 'Caratteristica della stanza'),
(90, 'room_feature_detail', 'Room Feature Detail', 'Dettagli delle caratteristiche della camera'),
(91, 'room_service', 'Room Service', 'Servizio in camera'),
(92, 'room_service_detail', 'Room Service Detail', 'Dettaglio servizio in camera'),
(93, 'change_password', 'Change Password', 'Cambia la password'),
(94, 'old_password', 'Old Password', 'vecchia password'),
(95, 'new_password', 'New Password', 'nuova password'),
(96, 'confirm_password', 'Confirm Password', 'conferma password'),
(97, 'user_need_information_detail', 'User Need Information Detail', 'Dettagli sulle informazioni necessarie per l\'utente'),
(98, 'object', 'Object', 'Oggetto'),
(99, 'message', 'Message', 'Messaggio'),
(100, 'created_date', 'Created Date', 'Data di Creazione'),
(101, 'add_user_need_information_detail', 'Add User Need Information Detail', 'Aggiungi dettagli sulle informazioni necessarie per l\'utente'),
(102, 'visit', 'Visit', 'Visitare'),
(103, 'write_email', 'Write Email', 'Scrivi e-mail'),
(104, 'contact', 'Contact', 'Contatto'),
(105, 'add_other_room_slider', 'Add Other Room Slider', 'Aggiungi un altro dispositivo di scorrimento della stanza'),
(106, 'add_double_room_slider', 'Add Double Room Slider', 'Aggiungi il cursore della camera doppia'),
(107, 'double_room_detail', 'Double Room Detail', 'Dettaglio camera doppia'),
(108, 'experience_detail', 'Experience Detail', 'Dettagli dell\'esperienza'),
(109, 'experience_heading', 'Experience Heading', 'Intestazione dell\'esperienza'),
(110, 'add_room_services', 'Add Room Services', 'Aggiungi servizi in camera'),
(111, 'add_room_features', 'Add Room Features', 'Aggiungi caratteristiche della stanza'),
(112, 'room_feature_service', 'Room Feature Service', 'Servizio in camera'),
(113, 'feature_and_service_detail', 'Feature and Service Detail', 'Caratteristiche e dettagli del servizio'),
(114, 'banaco_slider_image', 'Banaco Slider Image', 'Immagine del cursore del banana'),
(115, 'user_detail', 'User Detail', 'Dettagli utente'),
(116, 'add_hotel_banaco_video', 'Add Hotel Banaco Video', 'Aggiungi il video di Hotel Banaco'),
(117, 'hotel_benaco_heading', 'Hotel Benaco Heading', 'Intestazione Hotel Benaco'),
(118, 'hotel_benaco_video', 'Hotel Benaco Video', 'Video Hotel Benaco'),
(119, 'choose_video', 'Choose Video', 'Scegli video'),
(120, 'pool_and_garden_video', 'Pool & Garden Video', 'Video su piscina e giardino'),
(121, 'add_room_images', 'Add Room Images', 'Aggiungi immagini della stanza'),
(122, 'room_images', 'Room Images', 'Immagini della stanza'),
(123, 'next_page', 'Next Page', 'Pagina successiva'),
(124, 'home_page', 'Home Page', 'Pagina iniziale'),
(125, 'login', 'Login', 'Accedere'),
(126, 'sign_up', 'Sign Up', 'Iscriviti'),
(127, 'select', 'Select', 'Selezionare'),
(128, 'arrival_date', 'Arrival Date', 'Data d\'arrivo'),
(129, 'departure_date', 'Departure Date', 'Data di partenza'),
(130, 'adults', 'Adults', 'Adults'),
(131, 'promo_code', 'Promo Code', 'Codice promozionale'),
(132, 'check_availability', 'Check Availability', 'Verificare la disponibilità'),
(133, 'modify_and_cancel', 'modify/cancel an existing reservation', 'modificare/cancellare una prenotazione esistente'),
(134, 'simple', 'Simple', 'Semplice'),
(135, 'you_discover', 'You discover', 'Scopri'),
(136, 'pool_garden', 'POOL & GARDEN', 'PISCINA & GIARDINO'),
(137, 'peace_and_relaxation_a_few_steps', 'Peace and relaxation a few steps from the center.', 'Pace e relax a pochi passi dal centro.'),
(138, 'pool', 'Pool', 'Piscina'),
(139, 'garden', 'Garden', 'Giardino'),
(140, 'username', 'Username', 'Nome utente'),
(141, 'password', 'Password', 'Parola d\'ordine'),
(142, 'enter_username', 'Enter Username', 'Inserire username'),
(143, 'enter_password', 'Enter password', 'Inserire la password'),
(144, 'please_fill_in_this_form', 'Please fill in this form to create an account.', 'Si prega di compilare questo modulo per creare un account.'),
(145, 'enter_email', 'Enter Email', 'Inserisci l\'email'),
(146, 'repeat_password', 'Repeat Password', 'Ripeti la password'),
(147, 'cancel', 'Cancel', 'Annulla'),
(148, 'do_you_need_information', 'Do you need information?', 'Servono Informazioni?'),
(149, 'i_have_read_and_accepted', 'I have read and accepted the', 'Ho letto ed accettato la'),
(150, 'privacy_policy', 'Privacy Policy', 'politica sulla riservatezza'),
(151, 'send', 'Send', 'Invia'),
(152, 'name_surname', 'name & surname', 'nome & cognome'),
(153, 'email_address', 'Email address', 'Indirizzo e-mail'),
(154, 'object', 'object', 'oggetto'),
(155, 'visit', 'VISIT', 'VISITA'),
(156, 'write', 'WRITE', 'SCRIVI'),
(157, 'who_loves', 'WHO LOVES', 'CHIAMA'),
(158, 'privacy_policy', 'Privacy Policy', 'politica sulla riservatezza'),
(159, 'cookie_policy', 'Cookie Policy', 'Gestione dei Cookie');

-- --------------------------------------------------------

--
-- Table structure for table `living_hotel_banaco_facilities`
--

CREATE TABLE `living_hotel_banaco_facilities` (
  `lhb_id` int(11) NOT NULL,
  `lhb_language` varchar(255) NOT NULL,
  `lhb_room_facilities` varchar(255) DEFAULT NULL,
  `lhb_iconname` varchar(255) DEFAULT NULL,
  `lhb_page_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `living_hotel_banaco_facilities`
--

INSERT INTO `living_hotel_banaco_facilities` (`lhb_id`, `lhb_language`, `lhb_room_facilities`, `lhb_iconname`, `lhb_page_name`) VALUES
(101, 'ENGLISH', '20 - 25 mq', 'fas fa-person-booth', 'home_page_def'),
(102, 'ENGLISH', 'Max 3 Phosti Letto', 'fas fa-bed', 'home_page_def'),
(103, 'ENGLISH', 'TV Satellitare', 'fas fa-tv', 'home_page_def'),
(104, 'ENGLISH', 'Area Condizionata', 'fab fa-accusoft', 'home_page_def'),
(126, 'ITALIAN', '20 - 25 mq', 'fas fa-person-booth', 'home_page_def'),
(127, 'ITALIAN', 'Max 3 Phosti Letto', 'fas fa-bed', 'home_page_def'),
(128, 'ITALIAN', 'TV Satellitare', 'fas fa-tv', 'home_page_def'),
(129, 'ITALIAN', 'Area Condizionata', 'fab fa-accusoft', 'home_page_def');

-- --------------------------------------------------------

--
-- Table structure for table `living_hotel_banaco_room`
--

CREATE TABLE `living_hotel_banaco_room` (
  `lhbr_id` int(11) NOT NULL,
  `lhbr_language` varchar(255) NOT NULL,
  `lhbr_heading` varchar(255) DEFAULT NULL,
  `lhbr_description` text DEFAULT NULL,
  `lhbr_image` varchar(255) DEFAULT NULL,
  `lhbr_guest_number` varchar(255) NOT NULL,
  `lhbr_page_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `living_hotel_banaco_room`
--

INSERT INTO `living_hotel_banaco_room` (`lhbr_id`, `lhbr_language`, `lhbr_heading`, `lhbr_description`, `lhbr_image`, `lhbr_guest_number`, `lhbr_page_name`) VALUES
(8, 'ITALIAN', 'VIVERE HOTEL BENACO', '<p>Le Camere</p>\r\n', NULL, 'Spaziose Camere per 3 ospiti', 'home_page_def'),
(23, 'ENGLISH', 'LIVING HOTEL BENACO', '<p>Rooms</p>\r\n', NULL, 'Spacious Rooms for 3 guests', 'home_page_def');

-- --------------------------------------------------------

--
-- Table structure for table `room_feature_and_service`
--

CREATE TABLE `room_feature_and_service` (
  `fasd_id` int(11) NOT NULL,
  `fasd_language` varchar(255) NOT NULL,
  `fasd_feature` varchar(255) DEFAULT NULL,
  `fasd_iconname` varchar(255) DEFAULT NULL,
  `fasd_page_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room_feature_and_service`
--

INSERT INTO `room_feature_and_service` (`fasd_id`, `fasd_language`, `fasd_feature`, `fasd_iconname`, `fasd_page_name`) VALUES
(200, '', 'rr2', 'fas fa-wifi', 'FEATURE'),
(201, '', 'rr3', 'fas fa-wifi', 'FEATURE'),
(202, '', 'ss2', 'fas fa-wifi', 'SERVICE'),
(203, '', 'ss3', 'fas fa-wifi', 'SERVICE'),
(204, '', 'rr2', 'fas fa-wifi', 'FEATURE'),
(205, '', 'rr3', 'fas fa-wifi', 'FEATURE'),
(206, '', 'ss2', 'fas fa-wifi', 'SERVICE'),
(207, '', 'ss3', 'fas fa-wifi', 'SERVICE'),
(208, '', 's4', 'fas fa-wifi', 'SERVICE');

-- --------------------------------------------------------

--
-- Table structure for table `room_images_videos`
--

CREATE TABLE `room_images_videos` (
  `riv_id` int(11) NOT NULL,
  `riv_path` varchar(255) NOT NULL,
  `riv_section` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room_images_videos`
--

INSERT INTO `room_images_videos` (`riv_id`, `riv_path`, `riv_section`) VALUES
(68, 'uploads/room_images_videos/file_06140409d1ac9c.mp4', '1'),
(69, 'uploads/room_images_videos/file_06140409d2acb2.jpg', '2'),
(70, 'uploads/room_images_videos/file_06140409d34208.jpg', '3'),
(71, 'uploads/room_images_videos/file_16140409d3cc59.jpg', '3'),
(72, 'uploads/room_images_videos/file_26140409d441ac.jpg', '3'),
(73, 'uploads/room_images_videos/file_36140409d4a182.jpg', '3'),
(74, 'uploads/room_images_videos/file_0614040f99df55.mp4', '1'),
(75, 'uploads/room_images_videos/file_0614040f9b0031.jpg', '2'),
(76, 'uploads/room_images_videos/file_0614040f9b686d.jpg', '3'),
(77, 'uploads/room_images_videos/file_1614040f9bcdfd.jpg', '3'),
(78, 'uploads/room_images_videos/file_2614040f9c24fc.jpg', '3'),
(79, 'uploads/room_images_videos/file_3614040f9c6c6f.jpg', '3');

-- --------------------------------------------------------

--
-- Table structure for table `room_page`
--

CREATE TABLE `room_page` (
  `rp_id` int(11) NOT NULL,
  `rp_room_title` varchar(255) NOT NULL,
  `rp_intro_text` varchar(255) NOT NULL,
  `rp_double_room` varchar(255) NOT NULL,
  `rp_with_or_without_balcony` varchar(255) NOT NULL,
  `rp_double_room_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room_page`
--

INSERT INTO `room_page` (`rp_id`, `rp_room_title`, `rp_intro_text`, `rp_double_room`, `rp_with_or_without_balcony`, `rp_double_room_desc`) VALUES
(17, 'Titolo camera', 'Testo intto', 'camera dopaa', 'condnfsd', '<p>jflsfjslfsd</p>'),
(18, 'Titolo camera', 'Testo intto', 'camera dopaa', 'condnfsd', '<p>jflsfjslfsd</p>');

-- --------------------------------------------------------

--
-- Table structure for table `slider_images`
--

CREATE TABLE `slider_images` (
  `si_id` int(11) NOT NULL,
  `si_image` varchar(255) NOT NULL,
  `si_image_page` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider_images`
--

INSERT INTO `slider_images` (`si_id`, `si_image`, `si_image_page`) VALUES
(142, 'uploads/slider_images/file_061322571e0cd1.jpg', '8'),
(143, 'uploads/slider_images/file_161322571f3ee6.jpg', '8'),
(144, 'uploads/slider_images/file_26132257214391.jpg', '8'),
(145, 'uploads/slider_images/file_0613ce8bfd2227.jpg', '3'),
(146, 'uploads/slider_images/file_1613ce8bfd707b.jpg', '3'),
(147, 'uploads/slider_images/file_2613ce8bfdc175.jpg', '3'),
(148, 'uploads/slider_images/file_3613ce8bfe13b3.jpg', '3'),
(149, 'uploads/slider_images/file_0613cf2e588684.mp4', '9'),
(152, 'uploads/slider_images/file_0613d7e88d84ba.jpg', '5'),
(153, 'uploads/slider_images/file_1613d7e88db4df.jpg', '5'),
(154, 'uploads/slider_images/file_2613d7e88ddca9.jpg', '5'),
(155, 'uploads/slider_images/file_0613d82eca2202.jpg', '5'),
(156, 'uploads/slider_images/file_1613d82eca4a87.jpg', '5'),
(157, 'uploads/slider_images/file_2613d82eca7ed0.jpg', '5'),
(158, 'uploads/slider_images/file_0613d946e52e09.jpg', '6'),
(159, 'uploads/slider_images/file_1613d946e565de.jpg', '6'),
(160, 'uploads/slider_images/file_2613d946e5afa0.jpg', '6'),
(161, 'uploads/slider_images/file_3613d946e5fd50.jpg', '6'),
(163, 'uploads/slider_images/file_0613db3ff970b1.jpg', '10'),
(164, 'uploads/slider_images/file_0613db5a4d7b30.jpg', '10'),
(167, 'uploads/slider_images/file_613e12761bc881631457910.mp4', '4');

-- --------------------------------------------------------

--
-- Table structure for table `slider_images_page`
--

CREATE TABLE `slider_images_page` (
  `sip_id` int(11) NOT NULL,
  `sip_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider_images_page`
--

INSERT INTO `slider_images_page` (`sip_id`, `sip_name`) VALUES
(1, 'welcome_to_hotel_banaco'),
(2, 'experience'),
(3, 'pool_and_garden_1'),
(4, 'pool_and_garden_video'),
(5, 'pool_and_garden_3'),
(6, 'double_room'),
(7, 'living_hotel_banaco'),
(8, 'discover_other_room'),
(9, 'hotel_benaco_video'),
(10, 'room_images');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_remember_me_token` varchar(255) DEFAULT NULL,
  `user_public_ip` varchar(255) DEFAULT NULL,
  `user_img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_password`, `user_email`, `user_remember_me_token`, `user_public_ip`, `user_img`) VALUES
(3, 'Puhupwas', '$2y$18$tXTAxaki3knYtSw7g1xUd.XITQUk6qD.TaIryNAUDiS7Ra7Lu8IU.', 'puhupwasbind301@gmail.com', NULL, NULL, NULL),
(17, 'sonusharma', '$2y$18$5JAn9zK7jJFEmeYXBcedzeEk1hbeDYhBxZALwYcM/FGeOBMSQ2ZrO', 'puhusdp@gmail.com', NULL, NULL, NULL),
(19, 'theacademiz', '$2y$18$wONPjRBzP9OaYO6zzYKo9Ol5JAmvEy19F2wvF140jXh/FqUBz2xce', 'www@gmail.com', NULL, NULL, NULL),
(25, 'Ravi', '$2y$18$X.ihuFFSC5QgwXmM7IfuHe/p9C241CQ1QouS8mXEruHzH2Kkz.pka', 'ravi@gmail.com', NULL, NULL, NULL),
(26, 'sohan', '$2y$18$zk0omjAroqsnLdZzwpqp/ObKfhMmrSTJ21fq7tpxcynNE4FCnCktW', 'sohan@gmail.com', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_contact_details`
--

CREATE TABLE `user_contact_details` (
  `ucd_id` int(11) NOT NULL,
  `ucd_name` varchar(255) DEFAULT NULL,
  `ucd_storage` varchar(255) DEFAULT NULL,
  `ucd_number` varchar(255) DEFAULT NULL,
  `ucd_email` varchar(255) NOT NULL,
  `ucd_storage_list` varchar(255) NOT NULL,
  `ucd_created_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_contact_details`
--

INSERT INTO `user_contact_details` (`ucd_id`, `ucd_name`, `ucd_storage`, `ucd_number`, `ucd_email`, `ucd_storage_list`, `ucd_created_date`) VALUES
(2, 'Puhupwas', 'option2', '196149894', 'puhupwas@gmail@mailG.com', 'asdf', '2021-08-10 16:56:43'),
(3, 'puhupwas', 'option2', '8510062896', 'puhupwasbind301@lfjdslf', 'lsjdflsflsdafsfs', '2021-08-10 18:08:18'),
(4, 'puhupwas', 'option2', '8510062896', 'puhupwasbind301@lfjdslf', 'lsjdflsflsdafsfs', '2021-08-10 18:10:20'),
(5, 'Puhupwas', 'option2', '196149894', 'puhupwas@gmail@mailG.com', 'sadffsadfasdf sfsdfsdf', '2021-08-10 18:11:01'),
(6, 'asdfjlsfjsl', 'option2', '8516166446', 'puhupwasdeveloper@gmail.com', 'aslfsdlfjsdflsdafsdfs', '2021-08-10 18:22:13'),
(7, 'Puhupwas', 'option2', '196149894', 'puhupwas@gmail@mailG.com', 'asdfsdafsa', '2021-08-10 18:27:38'),
(8, 'phupwasffljl', 'option2', '9465164646', 'puhupwas@gmail@mailG.com', 'slfjsadlf lsjdflsdaslj lsdf', '2021-08-10 18:29:18'),
(9, 'phupwasffljl', 'option2', '9465164646', 'puhupwas@gmail@mailG.com', 'slfjsadlf lsjdflsdaslj lsdf', '2021-08-10 18:37:06'),
(10, 'phupwasffljl', 'option2', '9465164646', 'puhupwas@gmail@mailG.com', 'slfjsadlf lsjdflsdaslj lsdf', '2021-08-10 18:37:48'),
(11, 'phupwasffljl', 'option2', '9465164646', 'puhupwas@gmail@mailG.com', 'slfjsadlf lsjdflsdaslj lsdf', '2021-08-10 18:39:04'),
(12, 'phupwasffljl', 'option2', '9465164646', 'puhupwas@gmail@mailG.com', 'slfjsadlf lsjdflsdaslj lsdf', '2021-08-10 18:50:10'),
(13, 'phupwasffljl', 'option2', '9465164646', 'puhupwas@gmail@mailG.com', 'asdfsd sfsdds sadfdfsdfas', '2021-08-10 18:50:19'),
(14, 'puhupwas', 'option2', '9465164646', 'puhupwasdeveloper@gmail.com', 'asdfsd sfsdds sadfdfsdfas', '2021-08-10 18:51:16'),
(15, 'puhupwas', 'option2', '9465164646', 'puhupwasdeveloper@gmail.com', 'asdfsd sfsdds sadfdfsdfas', '2021-08-10 18:53:41'),
(16, 'Puhupwas', 'option2', '9213791237912', 'puhupwasdeveloper@gmail.com', 'let us store list', '2021-08-10 18:54:42'),
(17, 'Puhupwas', 'option2', '196149894', 'puhupwasdeveloper@gmail.com', 'asdfsda sfsdfsdfsd', '2021-08-10 18:56:13'),
(18, 'Puhupwas', 'option2', '196149894', 'puhupwasdeveloper@gmail.com', 'asdfsda sfsdfsdfsd', '2021-08-10 18:59:19'),
(19, 'Puhupwas', 'option2', '196149894', 'puhupwasdeveloper@gmail.com', 'asdfsda sfsdfsdfsd', '2021-08-10 19:04:50'),
(20, 'Puhupwas', 'option2', '496465465', 'puhupwasdeveloper@gmail.com', 'lsdjdfls lsadfljsdlfjsdfsdfsd ', '2021-08-10 19:05:10'),
(21, 'Puhupwas', 'option2', '196149894', 'puhupwasdeveloper@gmail.com', 'sladfj saldfjlsdsa sadlfjls jsafsd dfs', '2021-08-10 19:05:53'),
(22, 'Puhupwas', 'option2', '196149894', 'puhupwasdeveloper@gmail.com', 'sladfj saldfjlsdsa sadlfjls jsafsd dfs', '2021-08-10 19:09:53'),
(23, 'let us store', 'option2', '196149894', 'puhupwasdeveloper@gmail.com', 'sladfj saldfjlsdsa sadlfjls jsafsd dfs', '2021-08-10 19:10:25'),
(24, 'Puhupwas', 'option1', '196149894', 'puhupwasdeveloper@gmail.com', 'Storage 1 , Storage 2, Storage 3', '2021-08-10 19:11:52'),
(25, 'Puhupwas', 'option1', '85131321466', 'puhupwasdeveloper@gmail.com', 'sdljf asldfjl slfjsdlf ', '2021-08-10 19:32:04'),
(26, 'Puhupwas', 'option1', '196149894', 'puhupwasdeveloper@gmail.com', 'sldjff ssalffjsl aslffjdslf lfjdsl sfds', '2021-08-11 11:24:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_need_information`
--

CREATE TABLE `user_need_information` (
  `uni_id` int(11) NOT NULL,
  `uni_name` varchar(255) DEFAULT NULL,
  `uni_email` varchar(255) DEFAULT NULL,
  `uni_object` varchar(255) NOT NULL,
  `uni_message` text NOT NULL,
  `uni_created_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_need_information`
--

INSERT INTO `user_need_information` (`uni_id`, `uni_name`, `uni_email`, `uni_object`, `uni_message`, `uni_created_date`) VALUES
(29, 'Puhupwas', 'puhupwasbind301@gmail.com', 'sfsldfjlsdfjsldlskfjl', 'lsajlfslfsjlfslsadfdsfsdff', '2021-08-23 14:36:33'),
(30, 'Puhupwas', 'fljsadlfj@sdfjsdlajf', 'lsjdlfjs', 'salfjsdklfs', '2021-08-23 14:36:43'),
(31, 'Puhupwas', 'fljsadlfj@sdfjsdlajf', 'lsjdlfjs', 'salfjsdklfssdfasdfsda', '2021-08-23 14:36:55'),
(32, 'puhupwas', 'sdfsdaf@dafsdf', 'sdlfjsladfjklasdf', 'sadfsdafasfsdfasdf', '2021-08-23 14:37:44'),
(33, 'Neeraj', 'Sharma@aslfjlf', 'asjlfjsldkafjsl', 'aslfjlsdfjldskfjlsdfjsd', '2021-08-23 14:38:25'),
(35, 'need infor name22222222', 'need@infogmai.coansndfljdl222222222', 'object lsjdflsdafjlasdfj;22', 'aslfjjsdlakjsl;adkfjsl;aj2222222222', '2021-08-31 18:58:57'),
(36, 'Servono', 'Indrizolo', 'oggetto', 'messagio', '2021-08-31 19:11:57'),
(37, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:21'),
(38, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:22'),
(39, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:22'),
(40, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:23'),
(41, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:37'),
(42, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:38'),
(43, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:39'),
(44, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:40'),
(45, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:40'),
(46, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:40'),
(47, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:40'),
(48, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:40'),
(49, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:41'),
(50, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:41'),
(51, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:41'),
(52, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:41'),
(53, 'admin', 'puhupwasbind301@gmail.com', 'asdas', 'sffssdfsfds', '2021-09-12 10:58:41'),
(54, 'admin', 'puhupwasbind301@gmail.com', 'sfsdf', 'dsfsdfsd', '2021-09-12 11:01:13'),
(55, 'admin', 'puhupwasbind301@gmail.com', 'sfsdf', 'dsfsdfsd', '2021-09-12 11:01:14'),
(56, 'admin', 'puhupwasbind301@gmail.com', 'sfsdf', 'dsfsdfsd', '2021-09-12 11:01:50'),
(57, 'Z', 'ravi@gmail.com', 'zxc', 'zxc', '2021-09-12 11:03:06'),
(58, 'admin', 'puhupwasbind301@gmail.com', 'ssf', 'sdf', '2021-09-12 11:06:52'),
(59, 'sdf', 'ravi@gmail.com', 'asdasd', 'asd', '2021-09-12 11:07:15'),
(60, 'puhias', 'sfjs@gsj', 'sdjfl', 'sdfjljfsf', '2021-09-12 11:07:44'),
(61, 'sdf', 'puhupwasbind301@gmail.com', 'sdf', 'sd', '2021-09-12 11:08:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `admin_name` (`admin_name`);

--
-- Indexes for table `contact_detail`
--
ALTER TABLE `contact_detail`
  ADD PRIMARY KEY (`cd_id`);

--
-- Indexes for table `discover_other_room`
--
ALTER TABLE `discover_other_room`
  ADD PRIMARY KEY (`dor_id`);

--
-- Indexes for table `discover_other_room_facilities`
--
ALTER TABLE `discover_other_room_facilities`
  ADD PRIMARY KEY (`dorf_id`);

--
-- Indexes for table `double_room`
--
ALTER TABLE `double_room`
  ADD PRIMARY KEY (`dr_id`);

--
-- Indexes for table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `feature_and_service`
--
ALTER TABLE `feature_and_service`
  ADD PRIMARY KEY (`fas_id`);

--
-- Indexes for table `feature_and_service_detail`
--
ALTER TABLE `feature_and_service_detail`
  ADD PRIMARY KEY (`fasd_id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`home_id`);

--
-- Indexes for table `home_images_videos`
--
ALTER TABLE `home_images_videos`
  ADD PRIMARY KEY (`hiv_id`);

--
-- Indexes for table `home_page`
--
ALTER TABLE `home_page`
  ADD PRIMARY KEY (`hp_id`);

--
-- Indexes for table `home_page_def`
--
ALTER TABLE `home_page_def`
  ADD PRIMARY KEY (`hpd_id`);

--
-- Indexes for table `hotel_benaco_video`
--
ALTER TABLE `hotel_benaco_video`
  ADD PRIMARY KEY (`hbv_id`);

--
-- Indexes for table `hotel_benaco_video_image_section`
--
ALTER TABLE `hotel_benaco_video_image_section`
  ADD PRIMARY KEY (`hbvis_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `living_hotel_banaco_facilities`
--
ALTER TABLE `living_hotel_banaco_facilities`
  ADD PRIMARY KEY (`lhb_id`);

--
-- Indexes for table `living_hotel_banaco_room`
--
ALTER TABLE `living_hotel_banaco_room`
  ADD PRIMARY KEY (`lhbr_id`);

--
-- Indexes for table `room_feature_and_service`
--
ALTER TABLE `room_feature_and_service`
  ADD PRIMARY KEY (`fasd_id`);

--
-- Indexes for table `room_images_videos`
--
ALTER TABLE `room_images_videos`
  ADD PRIMARY KEY (`riv_id`);

--
-- Indexes for table `room_page`
--
ALTER TABLE `room_page`
  ADD PRIMARY KEY (`rp_id`);

--
-- Indexes for table `slider_images`
--
ALTER TABLE `slider_images`
  ADD PRIMARY KEY (`si_id`);

--
-- Indexes for table `slider_images_page`
--
ALTER TABLE `slider_images_page`
  ADD PRIMARY KEY (`sip_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `admin_name` (`user_name`);

--
-- Indexes for table `user_contact_details`
--
ALTER TABLE `user_contact_details`
  ADD PRIMARY KEY (`ucd_id`);

--
-- Indexes for table `user_need_information`
--
ALTER TABLE `user_need_information`
  ADD PRIMARY KEY (`uni_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_detail`
--
ALTER TABLE `contact_detail`
  MODIFY `cd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `discover_other_room`
--
ALTER TABLE `discover_other_room`
  MODIFY `dor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `discover_other_room_facilities`
--
ALTER TABLE `discover_other_room_facilities`
  MODIFY `dorf_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `double_room`
--
ALTER TABLE `double_room`
  MODIFY `dr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `experience`
--
ALTER TABLE `experience`
  MODIFY `e_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `feature_and_service`
--
ALTER TABLE `feature_and_service`
  MODIFY `fas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `feature_and_service_detail`
--
ALTER TABLE `feature_and_service_detail`
  MODIFY `fasd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `home_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `home_images_videos`
--
ALTER TABLE `home_images_videos`
  MODIFY `hiv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `home_page`
--
ALTER TABLE `home_page`
  MODIFY `hp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_page_def`
--
ALTER TABLE `home_page_def`
  MODIFY `hpd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `hotel_benaco_video`
--
ALTER TABLE `hotel_benaco_video`
  MODIFY `hbv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `hotel_benaco_video_image_section`
--
ALTER TABLE `hotel_benaco_video_image_section`
  MODIFY `hbvis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `lang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT for table `living_hotel_banaco_facilities`
--
ALTER TABLE `living_hotel_banaco_facilities`
  MODIFY `lhb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `living_hotel_banaco_room`
--
ALTER TABLE `living_hotel_banaco_room`
  MODIFY `lhbr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `room_feature_and_service`
--
ALTER TABLE `room_feature_and_service`
  MODIFY `fasd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;

--
-- AUTO_INCREMENT for table `room_images_videos`
--
ALTER TABLE `room_images_videos`
  MODIFY `riv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `room_page`
--
ALTER TABLE `room_page`
  MODIFY `rp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `slider_images`
--
ALTER TABLE `slider_images`
  MODIFY `si_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `slider_images_page`
--
ALTER TABLE `slider_images_page`
  MODIFY `sip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user_contact_details`
--
ALTER TABLE `user_contact_details`
  MODIFY `ucd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user_need_information`
--
ALTER TABLE `user_need_information`
  MODIFY `uni_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
